# CUT Virtual Reality Prototype

Unreal Engine 4 based Virtual Reality Prototype of a Digital Twin demonstrating several show cases.

## License

CUT Virtual Reality Prototype is published under MIT license.


## Requirements

### Hardware

PC with at least:

- 16 GB RAM
- Intel i7 or AMD Ryzen 7 CPU
- 10 GB free disc space


#### 3D Modus
- nVidia GTX 1070 with at least 4GB video memory (or similar)

#### VR Modus
- nVidia GTX 2080 with at least 6GB video memory (or similar)

### Software

- Windows 10/11 with latest graphics card drivers installed
- Unreal Engine 4.27.2
- Visual Studio 2019 (Community Edition)

## Steuerung

This prototype can be run in 3D mode (with monitor, mouse, keyboard) or in VR mode using a VR Headset.

### 3D Modus

Use your mouse to look around.

Key bindings:

| Key | Function |
| ----------- | ----------- |
| W | Move forward |
| S | Move backwards |
| A | Strafe left |
| D | Strafe right |
| Q | Decrease altitude |
| E | Increase altitude |
| Space | Toggle Menu |


### VR Modus

