/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


using System;
using System.IO;
using UnrealBuildTool;

public class CUT_VR_Prototype : ModuleRules
{
	public CUT_VR_Prototype(ReadOnlyTargetRules Target) : base(Target)
	{
        PrivatePCHHeaderFile = "CUT_VR_Prototype.h";

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "Http", "Json", "JsonUtilities", "AbsoluteDT", "ProceduralMeshComponent" });

		PrivateDependencyModuleNames.AddRange(new string[] { "AbsoluteDT", "ZipUtility" });

		RuntimeDependencies.Add( "$(ProjectDir)/Binaries/Win64/assimp-vc141-mt.dll", "$(ProjectDir)/Plugins/AbsoluteDT/ThirdParty/assimp/bin/x64/assimp-vc141-mt.dll" );


    }

}
