
#include "VRGripInterface.h"
#include "UObject/ObjectMacros.h"
#include "UObject/Interface.h"
 
UVRGripInterface::UVRGripInterface(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 
}


void IVRGripInterface::Native_NotifyThrowGripDelegates(UGripMotionControllerComponent* Controller, bool bGripped, const FBPActorGripInformation& GripInformation, bool bWasSocketed)
{

}