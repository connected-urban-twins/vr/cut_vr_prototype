/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "DataTypes.h"

#include "TaskInformation.generated.h"

UENUM(BlueprintType)
enum EDownloadType
{
	ProjectPreviewImage,
	ProjectItemPreviewImage,
	ProjectItem
};


/**
 * describe a task's information
 */
USTRUCT(BlueprintType)
struct FTaskInformation
{
	GENERATED_BODY()
	
public:
	FTaskInformation()
	{
		TotalSize = 0;
		CurrentSize = 0;
		ETag = FString();
//		GUID = 0;
		SourceUrl = FString();
		FileName = FString();
		Project = nullptr;
		ProjectItem = nullptr;
	}

	bool SerializeToJsonString(FString& OutJsonString) const;

	bool DeserializeFromJsonString(const FString& InJsonString);

	FGuid GetGuid() const
	{
		return GUID;
	}

	bool operator==(const FTaskInformation& InTaskInfo)
	{
		return GetGuid() == InTaskInfo.GetGuid();
	}

	bool operator==(const FGuid InGuid)
	{
		return GetGuid() == InGuid;
	}

	EDownloadType DownloadType;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		UUserProjectViewModel* Project;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		UUserProjectItemViewModel* ProjectItem;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		FString FileName;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		FString SourceUrl;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		FString ETag;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		int32 CurrentSize;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		int32 TotalSize;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		FGuid GUID;
};
