/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "CoreMinimal.h"

#include "TaskInformation.h"
#include "DownloadTask.h"
#include "DownloadEvent.h"

#include "FileDownloadManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FDLManagerDelegate, ETaskEvent, InEvent, const FTaskInformation&, InInfo, FString, Message);

/**
 * FileDownloadManager, this class is the interface of the plugin, use this class download file as far as possible (both c++ & blueprint)
 */
UCLASS(BlueprintType)
class ABSOLUTEDT_API UFileDownloadManager : public UObject
{
	GENERATED_BODY()
public:

	UFileDownloadManager();

	UFUNCTION(BlueprintCallable)
		void StartAll();
	bool StartNext();

	UFUNCTION(BlueprintCallable)
		void StartLast();

	UFUNCTION(BlueprintCallable)
		void StartTask(const FGuid& InGuid);

	UFUNCTION(BlueprintCallable)
		void StopAll();

	UFUNCTION(BlueprintCallable)
		void StopTask(const FGuid& InGuid);

	/*save task information to a Json file, so you can load the task later.
	 @Param InGuid can not be invalid, identify a task
	 @Param InFileName figure out the target json file name, you can ignore this param
	*/
	UFUNCTION(BlueprintCallable)
		bool SaveTaskToJsonFile(const FGuid& InGuid, const FString& InFileName);

	/*
	*read a task describe file and assign it to current task
	@Param InFileName can not be empty. (example : "C:/FileDir/task_0.task")
	*/
	UFUNCTION(BlueprintCallable)
		bool ReadTaskFromJsonFile(const FGuid& InGuid, const FString& InFileName);

	UFUNCTION(BlueprintCallable)
		TArray<FTaskInformation> GetAllTaskInformation() const;

	
	/*Add a new task(exist task will be ignored, detected via Guid), first cannot be empty!!!
	 @ param : InUrl cannot be empty!
	 @ param : InDirectory ignore this param(Default directory will be used ../Content/FileDownload) 
   	 @ param : InFileName ignore this param(Default file name will be used, cutting & copy name from InUrl)
	 */
	//UFUNCTION(BlueprintCallable)
		FGuid AddTaskByUrl(EDownloadType downloadType, UUserProjectViewModel* project, UUserProjectItemViewModel* projectItem, const FString& userToken, const FString& InUrl, const FString& InDirectory = TEXT(""), const
		                   FString& InFileName = TEXT(""));
	void DeleteTask(FGuid taskIndex);
	bool GetAllTasksCompleted();
	void DeleteAllTasks();

	void OnTaskEvent(ETaskEvent InEvent, FTaskInformation InInfo, FString Message);

	UFUNCTION(BlueprintCallable)
		FString GetDownloadDirectory() const;
	int32 FindNextOpenToDo() const;

	UPROPERTY(BlueprintAssignable)
		FDLManagerDelegate OnDlManagerEvent;

	FGuid AddTask(DownloadTask* InTask);


	static FString DefaultDirectory;
	TArray<DownloadTask*> TaskList;

protected:

	int32 FindTaskToDo() const;

	int32 FindTaskByGuid(const FGuid& InGuid) const;


	int32 CurrentDoingWorks = 0;
};
