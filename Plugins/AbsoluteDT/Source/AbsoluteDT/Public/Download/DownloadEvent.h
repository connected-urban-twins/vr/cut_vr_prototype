/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class ETaskEvent : uint8
{
	//wait for download, no task information currently
	NONE,
	//start to get download task information
	GET_HEAD,
	//got task information
	GOT_HEAD,
	//start downloading task
	START_DOWNLOAD,
	//Update
	DOWNLOAD_UPDATE,
	
	//stop
	STOP,

	//download completed
	DOWNLOAD_COMPLETED,

	//target file exist
	EXIST,
	//meet error during downloading or get task information
	ERROR_OCCURED
};

UENUM(BlueprintType)
enum class ETaskState : uint8
{
	NONE,
	//wait for getting information
	WAIT,
	//waiting for starting
	READY,
	//is being downloading
	DOWNLOADING,

	//paused
	STOPPED,

	//task completed
	COMPLETED,
	//no need to download
	NO_NEED,
	//error state
	ERROR
};