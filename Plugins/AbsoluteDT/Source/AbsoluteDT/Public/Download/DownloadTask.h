/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "CoreMinimal.h"
#include "HttpModule.h"
#include "IHttpRequest.h"
#include "IHttpResponse.h"

#include "TaskInformation.h"
#include "DownloadEvent.h"
#include "DataTypes.h"

/**
 * a download task, normally operated by FileDownloadManager, extreamly advise you to use FileDownloadManager.
 */
class DownloadTask
{
public:
	DownloadTask();

	DownloadTask(EDownloadType downloadType, UUserProjectViewModel* project, UUserProjectItemViewModel* projectItem, const FString& userToken, const FString& InUrl, const FString& InDirectory, const FString& InFileName);

	virtual ~DownloadTask();
	void CleanUp();

	virtual FString SetFileName(const FString& InFileName);

	virtual FString GetFileName() const;

	virtual FString GetSourceUrl() const;

	virtual FString SetSourceUrl(const FString& InUrl);

	virtual FString SetDirectory(const FString& InDirectory);

	virtual FString GetDirectory() const;

	virtual int32 SetTotalSize(int32 InTotalSize);
	
	virtual int32 GetTotalSize() const;

	virtual int32 SetCurrentSize(int32 InCurrentSize);

	virtual int32 GetCurrentSize() const;

	virtual int32 GetPercentage() const;
	float GetProgress();

	virtual FString SetETag(const FString& ETag);

	virtual FString GetETag() const;

	virtual bool IsTargetFileExist() const;

	virtual bool Start();

	virtual bool Stop();

	bool ClearDirectory(const FString& InDirectory);

	FGuid GetGuid() const;

	virtual bool IsDownloading() const;

	FTaskInformation GetTaskInformation() const;

	ETaskState GetState() const;
	
	bool SaveTaskToJsonFile(const FString& InFileName) const;

	bool ReadTaskFromJsonFile(const FString& InFileName);
	void DeleteTask();

	//callback for notifying download events
	TFunction<void(ETaskEvent InEvent, const FTaskInformation& InInfo, FString Message)> ProcessTaskEvent = [this](ETaskEvent InEvent, const FTaskInformation& InInfo, FString Message) 
	{
	};


	FTaskInformation TaskInfo;

	bool bDeleteNow = false;

protected:

	virtual void GetHead();

	virtual void StartChunk();

	virtual FString GetFullFileName() const;

	virtual void OnGetHeadCompleted(FHttpRequestPtr InRequest, FHttpResponsePtr InResponse, bool bWasSuccessful);
	virtual void OnGetChunkCompleted(FHttpRequestPtr InRequest, FHttpResponsePtr InResponse, bool bWasSuccessful);

	virtual void OnTaskCompleted();
	bool ChangeTempFilename();

	virtual void OnWriteChunkEnd(int32 WroteSize);


	FString Directory;

	FString EncodedUrl;

	bool bShouldStop = false;

	ETaskState TaskState = ETaskState::NONE;

	static FString TEMP_FILE_EXTERN;
	static FString TASK_JSON;

	//4MB as one section to download
	const int32 ChunkSize = 4 * 1024 * 1024;

	TArray<uint8> DataBuffer;

	FString TmpFullName;

	FString UserToken;

	bool IsWritingToFile = false;
	IFileHandle* TargetFileHandle = nullptr;
	
	FHttpRequestPtr Request;
};
