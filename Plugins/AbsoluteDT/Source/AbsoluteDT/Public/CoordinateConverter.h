/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once


#include <math.h>   


#define M_PI 3.14159265358979323846

// UTM.h

// Original Javascript by Chuck Taylor
// Port to C++ by Alex Hajnal
//
// *** THIS CODE USES 32-BIT FLOATS BY DEFAULT ***
// *** For 64-bit double-precision edit this file: undefine FLOAT_32 and define FLOAT_64 (see below)
// 
// This is a simple port of the code on the Geographic/UTM Coordinate Converter (1) page from Javascript to C++.
// Using this you can easily convert between UTM and WGS84 (latitude and longitude).
// Accuracy seems to be around 50cm (I suspect rounding errors are limiting precision).
// This code is provided as-is and has been minimally tested; enjoy but use at your own risk!
// The license for UTM.cpp and UTM.h is the same as the original Javascript: 
// "The C++ source code in UTM.cpp and UTM.h may be copied and reused without restriction."
// 
// 1) http://home.hiwaay.net/~taylorc/toolbox/geography/geoutm.html
//
//#ifndef UTM_H
//#define UTM_H

// Choose floating point precision:

// 32-bit (for Teensy 3.5/3.6 ARM boards, etc.)
//#define FLOAT_32

// 64-bit (for desktop/server use)
#define FLOAT_64

#ifdef FLOAT_64
#define FLOAT double
#define SIN sin
#define COS cos
#define TAN tan
#define POW pow
#define SQRT sqrt
#define FLOOR floor

#else
#ifdef FLOAT_32
#define FLOAT float
#define SIN sinf
#define COS cosf
#define TAN tanf
#define POW powf
#define SQRT sqrtf
#define FLOOR floorf

#endif
#endif


#include <math.h>

#define pi 3.14159265358979

/* Ellipsoid model constants (actual values here are for WGS84) */
#define sm_a 6378137.0
#define sm_b 6356752.314
#define sm_EccSquared 6.69437999013e-03

#define UTMScaleFactor 0.9996

// DegToRad
// Converts degrees to radians.
FLOAT DegToRad(FLOAT deg);

// RadToDeg
// Converts radians to degrees.
FLOAT RadToDeg(FLOAT rad);

// ArcLengthOfMeridian
// Computes the ellipsoidal distance from the equator to a point at a
// given latitude.
// 
// Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
// GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
// 
// Inputs:
//     phi - Latitude of the point, in radians.
// 
// Globals:
//     sm_a - Ellipsoid model major axis.
//     sm_b - Ellipsoid model minor axis.
// 
// Returns:
//     The ellipsoidal distance of the point from the equator, in meters.
FLOAT ArcLengthOfMeridian(FLOAT phi);

// UTMCentralMeridian
// Determines the central meridian for the given UTM zone.
//
// Inputs:
//     zone - An integer value designating the UTM zone, range [1,60].
//
// Returns:
//   The central meridian for the given UTM zone, in radians
//   Range of the central meridian is the radian equivalent of [-177,+177].
FLOAT UTMCentralMeridian(int zone);

// FootpointLatitude
//
// Computes the footpoint latitude for use in converting transverse
// Mercator coordinates to ellipsoidal coordinates.
//
// Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
//   GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
//
// Inputs:
//   y - The UTM northing coordinate, in meters.
//
// Returns:
//   The footpoint latitude, in radians.
FLOAT FootpointLatitude(FLOAT y);

// MapLatLonToXY
// Converts a latitude/longitude pair to x and y coordinates in the
// Transverse Mercator projection.  Note that Transverse Mercator is not
// the same as UTM; a scale factor is required to convert between them.
//
// Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
// GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
//
// Inputs:
//    phi - Latitude of the point, in radians.
//    lambda - Longitude of the point, in radians.
//    lambda0 - Longitude of the central meridian to be used, in radians.
//
// Outputs:
//    x - The x coordinate of the computed point.
//    y - The y coordinate of the computed point.
//
// Returns:
//    The function does not return a value.
void MapLatLonToXY(FLOAT phi, FLOAT lambda, FLOAT lambda0, FLOAT &x, FLOAT &y);

// MapXYToLatLon
// Converts x and y coordinates in the Transverse Mercator projection to
// a latitude/longitude pair.  Note that Transverse Mercator is not
// the same as UTM; a scale factor is required to convert between them.
//
// Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
//   GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
//
// Inputs:
//   x - The easting of the point, in meters.
//   y - The northing of the point, in meters.
//   lambda0 - Longitude of the central meridian to be used, in radians.
//
// Outputs:
//   phi    - Latitude in radians.
//   lambda - Longitude in radians.
//
// Returns:
//   The function does not return a value.
//
// Remarks:
//   The local variables Nf, nuf2, tf, and tf2 serve the same purpose as
//   N, nu2, t, and t2 in MapLatLonToXY, but they are computed with respect
//   to the footpoint latitude phif.
//
//   x1frac, x2frac, x2poly, x3poly, etc. are to enhance readability and
//   to optimize computations.
void MapXYToLatLon(FLOAT x, FLOAT y, FLOAT lambda0, FLOAT& phi, FLOAT& lambda);

// LatLonToUTMXY
// Converts a latitude/longitude pair to x and y coordinates in the
// Universal Transverse Mercator projection.
//
// Inputs:
//   lat - Latitude of the point, in radians.
//   lon - Longitude of the point, in radians.
//   zone - UTM zone to be used for calculating values for x and y.
//          If zone is less than 1 or greater than 60, the routine
//          will determine the appropriate zone from the value of lon.
//
// Outputs:
//   x - The x coordinate (easting) of the computed point. (in meters)
//   y - The y coordinate (northing) of the computed point. (in meters)
//
// Returns:
//   The UTM zone used for calculating the values of x and y.
int LatLonToUTMXY(FLOAT lat, FLOAT lon, int zone, FLOAT& x, FLOAT& y);

// UTMXYToLatLon
//
// Converts x and y coordinates in the Universal Transverse Mercator//   The UTM zone parameter should be in the range [1,60].

// projection to a latitude/longitude pair.
//
// Inputs:
// x - The easting of the point, in meters.
// y - The northing of the point, in meters.
// zone - The UTM zone in which the point lies.
// southhemi - True if the point is in the southern hemisphere;
//               false otherwise.
//
// Outputs:
// lat - The latitude of the point, in radians.
// lon - The longitude of the point, in radians.
// 
// Returns:
// The function does not return a value.
void UTMXYToLatLon(FLOAT x, FLOAT y, int zone, bool southhemi, FLOAT& lat, FLOAT& lon);



inline void PotsdamToWGS84(double lp, double bp, double& LatOut, double& LonOut)
{
	//	// Geographische L�nge lp und Breite bp im Potsdam Datum
	//
	//	// Quellsystem Potsdam Datum
	//	//  Gro�e Halbachse a und Abplattung fq
	double a = 6378137.000 - 739.845;
	double fq = 3.35281066e-3 - 1.003748e-05;
	//
	//	// Zielsystem WGS84 Datum
	//	//  Abplattung f
	double f = 3.35281066e-3;
	//
	//	// Parameter f�r datum shift
	double dx = 587.0;
	double dy = 16.0;
	double dz = 393.0;
	//
	//	// Quadrat der ersten numerischen Exzentrizit�t in Quell- und Zielsystem
	double e2q = (2 * fq - fq * fq);
	double e2 = (2 * f - f * f);

	// Breite und L�nge in Radianten

	double b1 = bp * (M_PI / 180.0);
	double l1 = lp * (M_PI / 180.0);
	//
	//	// Querkr�mmungshalbmesser nd
	double nd = a / sqrt(1 - e2q * sin(b1)*sin(b1));
	//
	//	// Kartesische Koordinaten des Quellsystems Potsdam
	double xp = nd * cos(b1)*cos(l1);
	double yp = nd * cos(b1)*sin(l1);
	double zp = (1 - e2q)*nd*sin(b1);

	//	// Kartesische Koordinaten des Zielsystems (datum shift) WGS84
	double x = xp + dx;
	double y = yp + dy;
	double z = zp + dz;
	//
	//	// Berechnung von Breite und L�nge im Zielsystem
	double rb = sqrt(x*x + y * y);
	double b2 = (180 / pi) * atan((z / rb) / (1 - e2));

	double l2 = 0;

	if (x > 0)
		l2 = (180 / pi) * atan(y / x);
	if (x < 0 && y > 0)
		l2 = (180 / pi) * atan(y / x) + 180;
	if (x < 0 && y < 0)
		l2 = (180 / pi) * atan(y / x) - 180;

	LonOut = l2;
	LatOut = b2;
}


inline void GaussKrugerToLatLon(double rw, double hw, double& lat, double& lon) {
	// Rechtswert rw und Hochwert hw im Potsdam Datum

	//  Potsdam Datum / Bessel Ellipsoid
	// Gro�e Halbachse a und Abplattung f
	double a = 6377397.15508;
	double f = 3.34277321e-3; // = 1/299.1528128 

	// Polkr�mmungshalbmesser c
	double c = a / (1 - f);

	// Quadrat der zweiten numerischen Exzentrizit�t
	double ex2 = (2 * f - f * f) / ((1 - f)*(1 - f));
	double ex4 = ex2 * ex2;
	double ex6 = ex4 * ex2;
	double ex8 = ex4 * ex4;

	// Koeffizienten zur Berechnung der geographischen Breite aus gegebener
	// Meridianbogenl�nge
	double e0 = c * (M_PI / 180)*(1 - 3 * ex2 / 4 + 45 * ex4 / 64 - 175 * ex6 / 256 + 11025 * ex8 / 16384);
	double f2 = (180 / M_PI)*(3 * ex2 / 8 - 3 * ex4 / 16 + 213 * ex6 / 2048 - 255 * ex8 / 4096);
	double f4 = (180 / M_PI)*(21 * ex4 / 256 - 21 * ex6 / 256 + 533 * ex8 / 8192);
	double f6 = (180 / M_PI)*(151 * ex6 / 6144 - 453 * ex8 / 12288);

	// Geographische Breite bf zur Meridianbogenl�nge gf = hw
	double sigma = hw / e0;
	double sigmr = sigma * M_PI / 180;
	double bf = sigma + f2 * sin(2 * sigmr) + f4 * sin(4 * sigmr)
		+ f6 * sin(6 * sigmr);

	// Breite bf in Radianten
	double br = bf * M_PI / 180;
	double tan1 = tan(br);
	double tan2 = tan1 * tan1;
	double tan4 = tan2 * tan2;

	double cos1 = cos(br);
	double cos2 = cos1 * cos1;

	double etasq = ex2 * cos2;

	// Querkr�mmungshalbmesser nd
	double nd = c / sqrt(1 + etasq);
	double nd2 = nd * nd;
	double nd4 = nd2 * nd2;
	double nd6 = nd4 * nd2;
	double nd3 = nd2 * nd;
	double nd5 = nd4 * nd;

	//  L�ngendifferenz dl zum Bezugsmeridian lh
	//kz = parseInt(rw / 1e6);
	double kz = (int)(rw / 1e6);
	double lh = kz * 3;
	double dy = rw - (kz*1e6 + 500000);
	double dy2 = dy * dy;
	double dy4 = dy2 * dy2;
	double dy3 = dy2 * dy;
	double dy5 = dy4 * dy;
	double dy6 = dy3 * dy3;

	double b2 = -tan1 * (1 + etasq) / (2 * nd2);
	double b4 = tan1 * (5 + 3 * tan2 + 6 * etasq*(1 - tan2)) / (24 * nd4);
	double b6 = -tan1 * (61 + 90 * tan2 + 45 * tan4) / (720 * nd6);

	double l1 = 1 / (nd*cos1);
	double l3 = -(1 + 2 * tan2 + etasq) / (6 * nd3*cos1);
	double l5 = (5 + 28 * tan2 + 24 * tan4) / (120 * nd5*cos1);

	// Geographischer Breite bp und L�nge lp als Funktion von Rechts- und Hochwert
	//ll["lat"] = parseFloat(bf + (180 / M_PI) * (b2*dy2 + b4 * dy4 + b6 * dy6));
	//ll["lng"] = parseFloat(lh + (180 / M_PI) * (l1*dy + l3 * dy3 + l5 * dy5));
	double latIn = (bf + (180 / M_PI) * (b2*dy2 + b4 * dy4 + b6 * dy6));
	double lonIn = (lh + (180 / M_PI) * (l1*dy + l3 * dy3 + l5 * dy5));

	//if (ll["lng"] < 5 || ll["lng"] > 16 || ll["lat"] < 46 || ll["lat"] > 56) var ll = [];
	
	//var llw = pot2wgs(ll["lng"], ll["lat"]);
	PotsdamToWGS84(lonIn, latIn, lat, lon);
}

inline void GaussKrugerToUTM32(FVector& Vertex)
{
	double latOut, lonOut, nOut, eOut;

	GaussKrugerToLatLon(Vertex.X, Vertex.Y, latOut, lonOut);
	LatLonToUTMXY(latOut, lonOut, 32, eOut, nOut);

	Vertex.X = eOut;
	Vertex.Y = nOut;
}

inline void GaussKrugerToUTM32(TArray<double>& Vertex)
{
	double latOut, lonOut, nOut, eOut;

	GaussKrugerToLatLon(Vertex[0], Vertex[1], latOut, lonOut);
	LatLonToUTMXY(latOut, lonOut, 32, eOut, nOut);

	Vertex[0] = eOut;
	Vertex[1] = nOut;
}

inline FVector2D GetSceneCoordinatesFromLatLon(double Lat, double Lon)
{
	double XPositionFromLatLon;
	double YPositionFromLatLon;

	LatLonToUTMXY(Lat, Lon, 32, XPositionFromLatLon, YPositionFromLatLon);

	XPositionFromLatLon -= 564000.0;
	YPositionFromLatLon = -(YPositionFromLatLon - 5933000.0);

	// Convert to UE units, including the global scale factor
	XPositionFromLatLon *= 100.0 * GLOBALSCALEFACTOR;
	YPositionFromLatLon *= 100.0 * GLOBALSCALEFACTOR;

	return FVector2D(XPositionFromLatLon, YPositionFromLatLon);
}


//#endif
