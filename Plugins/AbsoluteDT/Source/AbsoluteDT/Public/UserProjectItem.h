/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once


#include "Engine.h"

#include "DataTypes.h"

#include "UserProjectItem.generated.h"

UCLASS(BlueprintType)
class ABSOLUTEDT_API UUserProjectItem : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int ProjectItemId;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString Name;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString Description;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString ItemImage;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TEnumAsByte<ItemType> Type;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int ProjectId;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float GpsLatitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float GpsLongitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool RightHand;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool RenderAlwaysOnTop;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString Val;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TEnumAsByte<ItemUnit> Unit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float ScalingFactorX;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float ScalingFactorY;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float ScalingFactorZ;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TEnumAsByte<ItemAxisOrder> AxisOrder;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float RotateX;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float RotateY;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float RotateZ;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float TranslateX;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float TranslateY;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float TranslateZ;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool IsProcessed;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool GlobalTimeline;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool AutomaticHeightPlacement;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FDateTime ValidFrom;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FDateTime ValidTo;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString BaseUri;
};
