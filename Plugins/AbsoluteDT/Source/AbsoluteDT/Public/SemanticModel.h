/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "SemanticModel.generated.h"

USTRUCT(Blueprintable)
struct FSemanticModelMetadataEntry
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString Name;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString Value;
};






UCLASS()
class ABSOLUTEDT_API ASemanticModel : public AActor
{
	GENERATED_BODY()

public:
	ASemanticModel();

	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable)
	bool RetrieveMetaData(FString Name);
	
	FString MyBytesToString(const uint8* In, int32 Count);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, meta = (DisplayName = "Metadata content subdirectory", RelativeToGameContentDir))
	FDirectoryPath MetadataPath;
	UPROPERTY(BlueprintReadOnly)
	TArray<FSemanticModelMetadataEntry> Metadata;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<UStaticMeshComponent*> MeshesWithMetadata;

protected:
	virtual void BeginPlay() override;
	
private:	
	void CollectMeshesWithMetadata();
};

inline FString ASemanticModel::MyBytesToString(const uint8* In, int32 Count)
{
	FString Result;
	Result.Empty(Count);

	while (Count)
	{
		const int16 Value = *In;

	    Result += static_cast<TCHAR>(Value);

	    ++In;
	    Count--;
	}
	return Result;
}