/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "ChargingStation.h"
#include "IHttpRequest.h"
#include "ChargingStationManager.generated.h"

UCLASS()
class ABSOLUTEDT_API AChargingStationManager : public AActor
{
	GENERATED_BODY()

public:
	AChargingStationManager();

	void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void EnableChargingStations(bool Enable);
	
	void GetData();
	void UpdateData();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<AChargingStation> ChargingStationClass;

protected:
	void BeginPlay() override;
	
private:	
	void OnHttpRequestCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void OnUpdateRequestCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	FString NextLink;
	FString CurrentLink;
	
	bool FirstRun;
	bool IsRetrievingData;
	bool ChargingStationActorsEnabled;
	TArray<AChargingStation*> ChargingStationActors;
	float TimeCounter;
	float RequestTimeDelay;
};