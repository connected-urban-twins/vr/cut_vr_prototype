/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "CoreMinimal.h"
#include "JsonObjectConverter.h"

/**
 * 
 */
class ABSOLUTEDT_API JsonHelper
{
public:
	JsonHelper();
	~JsonHelper();
	static bool UStructToJsonObjectString(const UStruct * StructDefinition, const void * Struct, FString & OutJsonString, int64 CheckFlags, int64 SkipFlags, int32 Indent, const FJsonObjectConverter::CustomExportCallback * ExportCb, bool bPrettyPrint);
	static bool UStructToJsonObject(const UStruct * StructDefinition, const void * Struct, TSharedRef<FJsonObject> OutJsonObject, int64 CheckFlags, int64 SkipFlags, const FJsonObjectConverter::CustomExportCallback * ExportCb);
	static bool UStructToJsonAttributes(const UStruct * StructDefinition, const void * Struct, TMap<FString, TSharedPtr<FJsonValue>>& OutJsonAttributes, int64 CheckFlags, int64 SkipFlags, const FJsonObjectConverter::CustomExportCallback * ExportCb);
	template<class CharType, class PrintPolicy>
	static bool UStructToJsonObjectStringInternal(const TSharedRef<FJsonObject>& JsonObject, FString & OutJsonString, int32 Indent);
};
