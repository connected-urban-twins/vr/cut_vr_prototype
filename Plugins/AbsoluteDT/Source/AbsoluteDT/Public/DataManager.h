/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "Engine.h"
#include "UnrealNetwork.h"
#include "GameFramework/Actor.h"
#include "HttpModule.h"

#include "AppSettings.h"
#include "FileDownloadManager.h"
#include "DataTypes.h"
#include "ProjectItemActor.h"
#include "MediaViewer.h"
#include "IfcImporter.h"

#include "DataManager.generated.h"



DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FDataManagerStatusEvent, EDataManagerStatus, responseStatus, FString, messageString);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDownloadProgressEvent, float, Progress);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FProjectItemAddedEvent, bool, Success);

UCLASS(Config = Game)
class ABSOLUTEDT_API ADataManager : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ADataManager();

protected:
	void PostInitializeComponents() override;
	// Called when the game starts or when spawned
	void BeginPlay() override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void ProcessProjectItem(const int ProjectID, const int ProjectItemID);

	void OnDownloadsUpdated(float Progress);
	void StartPreprocessing();

	UFUNCTION()
	void OnMeshPreProcessingFinished(ARuntimeMeshActor* MeshActor, bool Success);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void TeleportToProject(UUserProjectViewModel* Project);

	UFUNCTION(BlueprintCallable)
	void FilterByDate(FDateTime Date);
	
	UFUNCTION(BlueprintCallable)
	void SetItemsVisibility(bool TimeRangeMode);

	UFUNCTION(BlueprintCallable)
		bool SaveSettings();

	UFUNCTION(BlueprintCallable)
		bool LoadSettings();

	UFUNCTION(BlueprintCallable)
		bool SetActiveProject(int ProjectID);

	UFUNCTION(BlueprintCallable)
		UUserProjectViewModel* FindProject(int32 ProjectID);

	UFUNCTION(BlueprintCallable)
		UUserProjectItemViewModel* FindProjectItem(int32 ProjectID, int32 ProjectItemID);

	UFUNCTION(BlueprintCallable)
		FVector2D GetProjectCenter(UUserProjectViewModel* Project);

	void ClearScene();

	UFUNCTION(BlueprintCallable)
		void InitScene();
	
	/**
	 * Cleans up changed items after new project data has been downloaded
	 */
	void CleanupData(TArray<FUserProject>& OldProjectData, TArray<FUserProject>& NewProjectData);

	UFUNCTION(BlueprintCallable)
		AProjectItemActor* GetProjectItemInScene(UUserProjectItemViewModel* ProjectItem);

	FVector GetSceneLocationFromLatLon(double Lat, double Lon);

	void GetGpsCoordinatesFromSceneLocation(FVector Location, double& Lat, double& Lon);

	FVector GetProjectItemPosition(UUserProjectItemViewModel* projectItem, bool CheckXYCoordinates=false);
	FVector2D GetObjectPosition(double Lat, double Lon);

	void SpawnProjectItem(UUserProjectItemViewModel* ProjectItem);

	ARuntimeMeshActor* SpawnRuntimeMesh(FString& FileName, UUserProjectItemViewModel* ProjectItem);
	FString GetProjectsDirectory();
	FString GetProjectPath(UUserProjectViewModel* Project);
	FString GetProjectItemPath(UUserProjectItemViewModel* ProjectItem);
	void LoadMenuData();
	void ParseProjectsJsonFile(TArray<FUserProject>& ParsedProjects);

	UFUNCTION(BlueprintCallable)
		void LoadProjectsData();

	bool EnsureDirectoryExists(FString& Directory);
	bool RemoveDirectory(FString& Directory);
	float GetDownloadProgress();

	FUserContext& GetUserContext();

	void OnUserInfoReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);


	void OnProjectsResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	void SetAuthorizationHash(FString Hash, TSharedRef<IHttpRequest>& Request);
	void LoginResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	TSharedRef<IHttpRequest> RequestWithRoute(FString Subroute);
	void SetRequestHeaders(TSharedRef<IHttpRequest>& Request);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bAutoInitScene;

	UPROPERTY(BlueprintReadOnly)
		TArray<FUserProject> Projects;

	UPROPERTY(BlueprintReadOnly)
		FUIDescription UIDescription;

	UPROPERTY(BlueprintReadOnly)
		TArray<UUserProjectViewModel*> UserProjects;

	UPROPERTY(BlueprintAssignable)
		FDataManagerStatusEvent OnStatusUpdated;

	UPROPERTY(BlueprintAssignable)
		FProjectItemAddedEvent OnProjectItemAdded;

	UPROPERTY(BlueprintAssignable)
		FDownloadProgressEvent OnDownloadProgressUpdated;

	UPROPERTY(BlueprintReadOnly)
		FUserInfo UserInfo;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UFileDownloadManager* DownloadManager;

	UPROPERTY(BlueprintReadOnly)
		int32 NumFinishedDownloads;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString CacheDirectory;

	UPROPERTY(BlueprintReadOnly)
		UUserProjectViewModel* CurrentActiveProject = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<AMediaViewer> MediaViewerClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ARuntimeMeshActor> BuildingClass;
	
	UPROPERTY(BlueprintReadWrite)
		FDateTime ValidDate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bStickersVisible;

	UPROPERTY(BlueprintReadOnly)
		TEnumAsByte<EDataManagerStatus> CurrentStatus;

private:

	// void LoadConfig();
	
	void UpdateStatus(EDataManagerStatus Status, FString Message);
	void OnDownloadCompleted(const FTaskInformation& InInfo);

	template <typename StructType>
	void GetJsonStringFromStruct(StructType FilledStruct, FString& StringOutput);
	template <typename StructType>
	void GetStructFromJsonString(FHttpResponsePtr Response, StructType& StructOutput);
	
	FHttpModule* Http;
	
	UPROPERTY()
		TArray<AProjectItemActor*> ProjectItemsInScene;

	int32 NumMeshesToPreprocess, NumMeshesPreprocessed;

	FString MeshPreviewImageFilename;
	};
