/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma warning( disable : 4643 )
#pragma warning( disable : 4668 )

#pragma once

#include <ifcpp/model/BuildingModel.h>
#include <ifcpp/model/BuildingException.h>
#include <ifcpp/model/BuildingGuid.h>
#include <ifcpp/reader/ReaderSTEP.h>
#include <ifcpp/reader/ReaderUtil.h>
#include <ifcpp/writer/WriterSTEP.h>
#include <ifcpp/IFC4/include/IfcProduct.h>
#include <ifcpp/IFC4/include/IfcSite.h>
#include <ifcpp/IFC4/include/IfcLengthMeasure.h>
#include <ifcpp/IFC4/include/IfcOpeningElement.h>
#include <ifcpp/IFC4/include/IfcOwnerHistory.h>
#include <ifcpp/IFC4/include/IfcGloballyUniqueId.h>
#include <ifcpp/geometry/Carve/GeometryConverter.h>
#include <ifcpp/geometry/Carve/GeomUtils.h>

#include "CoreUObject.h"

#include "MeshImport.h"

class ABSOLUTEDT_API IfcImporter
{
public:
	IfcImporter();
	~IfcImporter();

	void ImportFile(FString FilePath, double GaussKrugerGeoLocationOffsetX, double GaussKrugerGeoLocationOffsetY, TSharedPtr<FImportedMeshData>& Result, UBuildingData* BuildingData);

private:

	void ApplyAppearances(const std::vector<shared_ptr<AppearanceData>>& vec_product_appearances, FImportMaterial& Material);
	void ApplyAppearanceDiffuseColor(const std::vector<shared_ptr<AppearanceData>>& vec_product_appearances,
	                                 FColor& Color);
	void convertToImportMaterial(const shared_ptr<AppearanceData>& Appearance, FImportMaterial& Material);
	void drawMeshSet(const shared_ptr<carve::mesh::MeshSet<3>>& meshset, FColor& VertexColor, FImportedMeshSectionInfo& MeshInfo, double crease_angle, bool add_color_array);
	void convertProductShapeToUnrealMesh(double GaussKrugerGeoLocationOffsetX, double GaussKrugerGeoLocationOffsetY, TArray<int32>& MeshSectionsIndices, TSharedPtr<FImportedMeshData> Result, shared_ptr<ProductShapeData>& product_shape, FImportMaterial& Material);
	void resolveProjectStructure(TMap<int32, TArray<int32>>& MapMeshSections, const shared_ptr<ProductShapeData>& product_data, FImportedBuildingNode& ParentNode);

	shared_ptr<GeometryConverter> m_geometry_converter;
	shared_ptr<GeometrySettings> m_geom_settings;
	std::wstring wCharFilePath;
};
