/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/SkeletalMesh.h"
#include "Containers/Map.h"
#include "AvateeringComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBlendshapeAnimationCompletedEvent, bool, Success);

// The struct that holds one frame of morph targets
USTRUCT(BlueprintType)
struct FBlendshapeStruct
{
public:
	GENERATED_BODY()
	
	// An array with the names of the morph targets
	UPROPERTY(BlueprintReadWrite, Category = "Avateering")
	TArray<FString> ListOfBlendshapeKeys;

	// An array with the values of the morph target values
	UPROPERTY(BlueprintReadWrite, Category = "Avateering")
	TArray<float> ListOfBlendshapeValues;
};

USTRUCT(BlueprintType)
struct FBlendshapeRecordingStruct
{
public:
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "Avateering")
	int32 ID;
	
	// Array of FBlendshapeStructs
	UPROPERTY(BlueprintReadWrite, Category = "Avateering")
	TArray<FBlendshapeStruct> ListOfBlendshapeStruct;
};

// The struct that holds a whole recording of morph targets
USTRUCT(BlueprintType)
struct FBlendshapeRecording
{
public:
	GENERATED_BODY()
	// The name of the recording
	UPROPERTY(BlueprintReadWrite, Category = "Avateering")
		FString Name;

	UPROPERTY(BlueprintReadWrite, Category = "Avateering")
	FBlendshapeRecordingStruct BlendshapeRecordingStruct;
};

// Component which animates morph targets
UCLASS(hidecategories=Object, config=Engine, editinlinenew, meta=(BlueprintSpawnableComponent))
class ABSOLUTEDT_API UAvateeringComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UAvateeringComponent();

protected:
	void BeginPlay() override;

public:	
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FBlendshapeRecording LoadRecording(FString jsonString);
	
	UFUNCTION(BlueprintCallable)
	void PlayAnimation(int32 Index);
	
	UFUNCTION(BlueprintCallable)
	void StopAnimation();
	
	//UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* MeshReference;

	// List of json files that contain blendshape animations
	UPROPERTY(EditAnywhere)
	TArray<FFilePath> AnimationRecordingFiles;

	UPROPERTY(BlueprintReadOnly)
	bool bIsPlaying;

		UPROPERTY(BlueprintAssignable)
		FBlendshapeAnimationCompletedEvent OnBlendshapeAnimationCompleted;

private:
	float CurrentAnimationKeyIndex;
	int32 CurrentAnimationIndex;

	TArray<FBlendshapeRecording> Recordings;
};

