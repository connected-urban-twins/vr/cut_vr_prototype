/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "ProceduralMeshComponent.h"
#include "BufferArchive.h"

#include "MeshImport.generated.h"


#define MESHIMPORT_VERSION 4

class ARuntimeMeshActor;

class FAsyncImportMesh : public FNonAbandonableTask
{
	friend class FAsyncTask<FAsyncImportMesh>;

public:

	FAsyncImportMesh(ARuntimeMeshActor* MeshActor, FString file) :
		MeshActor(MeshActor),
		File(file)
	{}

	~FAsyncImportMesh()
	{
		MeshActor = nullptr;
	}

	ARuntimeMeshActor* MeshActor;
	bool CancelTask;

protected:

	FString FindFile(FString BasePath, FString FileName);
	void DoWork();

	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FAsyncImportMesh, STATGROUP_ThreadPoolAsyncTasks);
	}
	
	FString File;

private:

	FCriticalSection Mutex;
};

class FAsyncLoadMesh : public FNonAbandonableTask
{
	friend class FAsyncTask<FAsyncLoadMesh>;

public:

	FAsyncLoadMesh(ARuntimeMeshActor* MeshActor, FString file) :
		MeshActor(MeshActor),
		File(file)
	{}

	~FAsyncLoadMesh()
	{
		MeshActor = nullptr;
	}

	ARuntimeMeshActor* MeshActor;
	bool CancelTask;

protected:

	void DoWork();

	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FAsyncLoadMesh, STATGROUP_ThreadPoolAsyncTasks);
	}
	
	FString File;

private:

	FCriticalSection Mutex;
};

FORCEINLINE FArchive& operator<<(FArchive &Ar, FProcMeshTangent& Value)
{
	Ar.ByteOrderSerialize(&Value, sizeof(Value));

	return Ar;
}

struct FImportMaterial
{
public:
	FImportMaterial()
	{
		DiffuseColor = FLinearColor(0.6, 0.6, 0.6);
		DiffuseTexture = FString();
		Metallic = 0.0;
		Roughness = 1.0;
		Opacity = 1.0;
	}

	FImportMaterial(FLinearColor diffuseColor, FString diffuseTexture, float specular, float roughness, float opacity)
	{
		DiffuseColor = diffuseColor;
		DiffuseTexture = diffuseTexture;
		Metallic = specular;
		Roughness = roughness;
		Opacity = opacity;
	}

	FLinearColor DiffuseColor;
	FString DiffuseTexture;
	float Metallic;
	float Roughness;
	float Opacity;

	friend FArchive& operator<<( FArchive& Ar, FImportMaterial& Material )
	{
		Ar << Material.DiffuseColor;
		Ar << Material.DiffuseTexture;
		Ar << Material.Metallic;
		Ar << Material.Roughness;
		Ar << Material.Opacity;
		return Ar;
	}
};


struct FImportedMeshSectionInfo
{
public:

	FImportedMeshSectionInfo()
	{
		Vertices.Empty();
		Triangles.Empty();
		Normals.Empty();
		UV0.Empty();
		VertexColors.Empty();
		Tangents.Empty();
		SectionIndex = 0;
	}

	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FVector2D> UV0;
	TArray<FColor> VertexColors;
	TArray<FProcMeshTangent> Tangents;	
	int32 SectionIndex;

	friend FArchive& operator<<( FArchive& Ar, FImportedMeshSectionInfo& MeshSection )
	{
		Ar << MeshSection.Vertices;
		Ar << MeshSection.Triangles;
		Ar << MeshSection.Normals;
		Ar << MeshSection.UV0;
		Ar << MeshSection.VertexColors;
		Ar << MeshSection.Tangents;
		Ar << MeshSection.SectionIndex;

		return Ar;
	}
};


struct FImportedBuildingNode
{
	FImportedBuildingNode()
	{
		Level = 0;
		ID = 0;
		Name = FString();
	}

	int32 Level;
	int32 ID;
	FString Name;
	TArray<int32> MeshSectionIndices;
	TArray<FImportedBuildingNode> Children;

	friend FArchive& operator<<( FArchive& Ar, FImportedBuildingNode& Node )
	{
		Ar << Node.Level;
		Ar << Node.ID;
		Ar << Node.Name;
		Ar << Node.MeshSectionIndices;
		Ar << Node.Children;

		return Ar;
	}
};

struct FImportedMeshDataHeader
{
	FImportedMeshDataHeader()
	{
		Filename = FString();
		Version = 0;
	}

	FString Filename;
	int32 Version;

	friend FArchive& operator<<( FArchive& Ar, FImportedMeshDataHeader& Header )
	{
		Ar << Header.Filename;
		Ar << Header.Version;

		return Ar;
	}
};

struct FImportedMeshData
{
	void SaveLoadData(FArchive& Ar);
	void SaveLoadHeader(FArchive& Ar);
	bool SaveToDisk(const FString& FullFilePath);
	bool LoadFromDisk(const FString& FullFilePath, bool OnlyHeader = false);
	void Clear();
	bool bSuccess;

	FImportedMeshDataHeader Header;
	
	TArray<FImportedMeshSectionInfo> ImportedMeshSectionInfos;

	TArray<FImportMaterial> Materials;

	TArray<FImportedBuildingNode> ImportedBuildingNodes;
};


UCLASS(BlueprintType)
class ABSOLUTEDT_API UBuildingNode : public UObject
{
	GENERATED_BODY()

public:
	UBuildingNode()
	{
		Name = FString();
		MeshSectionIndices.Empty();
		Level = 0;
		Children.Empty();
		bIsVisible = true;	
		bIsEnabled = true;
		bHasParent = true;
		bHasChildren = true;
		bIsLastElement = true;
		MeshActor = nullptr;
	}

	~UBuildingNode()
	{
		MeshSectionIndices.Empty();
		Children.Empty();
	}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Name;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<int32> MeshSectionIndices;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<UBuildingNode*> Children;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 Level;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bIsVisible;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bIsEnabled;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bHasParent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bHasChildren;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bIsLastElement;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ARuntimeMeshActor* MeshActor;
};

UCLASS(BlueprintType)
class ABSOLUTEDT_API UBuildingData : public UObject
{
	GENERATED_BODY()

public:
	UBuildingData()
	{
		BuildingNodes.Empty();
		IfcProjectLongName = FString();
		IfcProjectPhase = FString();
		FileName = FString();
	}

	~UBuildingData()
	{
		BuildingNodes.Empty();
	}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<UBuildingNode*> BuildingNodes;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString IfcProjectLongName;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString IfcProjectPhase;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString FileName;
};