/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "AbsoluteDTPrivatePCH.h"

#include "Engine.h"
#include "CoreUObject.h"
#include "Runtime/Core/Public/Templates/UnrealTemplate.h"
#include "DataTypes.generated.h"


UENUM(BlueprintType)
enum class EPanoramaMediaTypeEnum : uint8
{
	PMT_Video 	UMETA(DisplayName = "Video"),
	PMT_Image 	UMETA(DisplayName = "Image")
};


USTRUCT()
struct FResponse_Login
{
	GENERATED_BODY()

	UPROPERTY() int id;
	UPROPERTY() FString name;
	UPROPERTY() FString hash;

	FResponse_Login()
	{
		id = 0;
		name = FString();
		hash = FString();
	}
};

UENUM(BlueprintType)
enum EViewFeatureType
{
	Buildings,
	CutOutTerrain,
	DepthData,
	Water,
	ProjectIndicators,
	ChargingStations,
	BuildingMetadataHints,
	Clouds,
	Rain
};

UENUM()
enum ItemUnit
{
	inch = 0,
	meter = 1
};

UENUM()
enum ItemAxisOrder
{
	xyz = 0,
	xzy = 1,
	zyx = 2,
	yxz = 3,
	yzx = 4,
	zxy = 5
};

UENUM(Blueprintable)
enum ItemType
{
	notype = 0,
	cad = 1,
	photo = 2,
	movie = 3,
	photo360 = 4,
	movie360 = 5,
	heightfield = 6,
	pointcloud = 7,
	obj_ls310 = 8,
	obj_ls320 = 9,
	ifc = 10,
	ifc_ls310 = 11,
	ifc_ls320 = 12,
	OutdoorSensorParking = 13,
	Environment_station = 14
};

UENUM(Blueprintable)
namespace EConstructionType
{
	enum Type
	{
		None = 0,
		Stahlvorsetze = 1,
		Schuettsteindeckwerk = 2
	};
}


UENUM(Blueprintable)
namespace EDamagesurvey
{
	enum Type
	{
		None = 0,
		Korrosion = 1,
		Bruch = 2,
		Dell = 3,
		Versackung = 4,
		Kolk = 5
	};
}

USTRUCT()
struct FLoginViewModel
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY()
		FString Username;
	UPROPERTY()
		FString Password;
};

USTRUCT(Blueprintable)
struct FUserContext
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FDateTime expires;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FDateTime issued;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString token_type;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int expires_in;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString userName;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString access_token;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Error;
};

USTRUCT(Blueprintable)
struct FUserInfo
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Id;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString FirstName;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString LastName;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Email;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString PhoneNumber;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString UserName;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<int32> ProjectIds;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsAdmin;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<FString> Roles;
};

USTRUCT(Blueprintable)
struct FProjectItem
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ProjectItemId;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Name;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Description;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString ItemImage;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TEnumAsByte<ItemType> Type;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ProjectId;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float GpsLatitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float GpsLongitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool RightHand;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool RenderAlwaysOnTop;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Val;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TEnumAsByte<ItemUnit> Unit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ScalingFactorX;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ScalingFactorY;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ScalingFactorZ;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TEnumAsByte<ItemAxisOrder> AxisOrder;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float RotateX;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float RotateY;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float RotateZ;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float TranslateX;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float TranslateY;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float TranslateZ;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsProcessed;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool GlobalTimeline;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool AutomaticHeightPlacement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString DamagesurveyDate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TEnumAsByte<EConstructionType::Type> ConstructionType;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TEnumAsByte<EDamagesurvey::Type> Damagesurvey;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 Damageclass;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float MarkerX;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float MarkerY;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float MarkerZ;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString ValidFrom;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString ValidTo;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		int32 SelfIllumination;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool BackfaceCulling;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Code;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString AppId;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Secret;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString NetAtmoToken;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString NetAtmoRefreshToken;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString ClientId;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString RedirectUrl;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Scope;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString DeviceId;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 Building;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString BaseUri;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ParkingSpotNo;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString ParkingSpotParam;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ParkingSpotCountPkw;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ParkingSpotCountLkw;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ParkingSpotCountUpdateSeconds;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ParkingSpotOccupied;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ParkingSpotStatus;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ParkingSpotDuration;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ParkingSpotTimediffKfz;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 ParkingSpotTimediffLkw;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString DoorLabelNo;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString DoorLabelContent;

	FProjectItem()
	{
		ProjectItemId = -1;
		ProjectId = -1;
		Name.Empty();
		GpsLatitude = 0.0;
		GpsLongitude = 0.0;
		RightHand = false;
		RenderAlwaysOnTop = false;
		Unit = ItemUnit::meter;
		AxisOrder = ItemAxisOrder::xyz;
		Type = ItemType::notype;
		IsProcessed = false;
		GlobalTimeline = false;
		AutomaticHeightPlacement = false;
		ConstructionType = EConstructionType::None;
		Damagesurvey = EDamagesurvey::None;
		Damageclass = 0;
		MarkerX = 0.0;
		MarkerY = 0.0;
		MarkerZ = 0.0;
		ScalingFactorX = 1.0;
		ScalingFactorY = 1.0;
		ScalingFactorZ = 1.0;
		RotateX = 0.0;
		RotateY = 0.0;
		RotateZ = 0.0;
		TranslateX = 0.0;
		TranslateY = 0.0;
		TranslateZ = 0.0;
		SelfIllumination = 0;
		BackfaceCulling = true;
		Building = -1;
		ParkingSpotNo = -1;
		ParkingSpotParam = FString();
		ParkingSpotCountPkw = -1;
		ParkingSpotCountLkw = -1;
		ParkingSpotCountUpdateSeconds = -1;
		ParkingSpotOccupied = -1;
		ParkingSpotStatus = -1;
		ParkingSpotDuration = -1;
		ParkingSpotTimediffKfz = -1;
		ParkingSpotTimediffLkw = -1;
	}
};

USTRUCT(Blueprintable)
struct FAtmoSensor
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int ProjectItemId;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int BuildingsProjectItemId;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString UserId;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int time_utc;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Temperature;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		int CO2;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		int Humidity;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		int Noise;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float Pressure;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float AbsolutePressure;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float min_temp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float max_temp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int date_min_temp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int date_max_temp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString temp_trend;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FString pressure_trend;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString StationName;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float StationLon;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float StationLat;
	FAtmoSensor() {
		ProjectItemId = -1;
		BuildingsProjectItemId = -1;
		UserId.Empty();
		time_utc = 0;
		Temperature = 0.0;
		CO2 = 0;
		Humidity = 0;
		Noise = 0;
		Pressure = 0.0;
		AbsolutePressure = 0.0;
		min_temp = 0.0;
		max_temp = 0.0;
		date_min_temp = 0;
		date_max_temp = 0;
		temp_trend.Empty();
		pressure_trend.Empty();
		StationName.Empty();
		StationLon = 0.0;
		StationLat = 0.0;
	}
};

USTRUCT(Blueprintable)
struct FWeatherStationData
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int ID;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int HpaStationId;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int StationType;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FDateTime Timestamp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Lat;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Lon;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FString SerialNumber;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FString WindDirectionUnit;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		int WindDirection;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FString WindSpeedUnit;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float WindSpeed;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString TemperatureUnit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Temperature;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString PressureUnit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Pressure;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Rainunit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Rain;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString HumidityUnit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Humidity;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString GlobalRadiationUnit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float GlobalRadiation;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Pm10Unit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Pm10;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Pm25Unit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Pm25;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString So2Unit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float So2;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString No2Unit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float No2;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString No24Unit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float No24;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString No1Unit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float No1;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString No14Unit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float No14;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString CoUnit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Co;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString PbUnit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Pb;

	FWeatherStationData() {
		ID = -1;
		HpaStationId = -1;
		StationType = 0;
		Timestamp = FDateTime::Now();
		Lat = 0.0;
		Lon = 0.0;
		SerialNumber.Empty();
		WindDirectionUnit.Empty();
		WindDirection = 0;
		WindSpeedUnit.Empty();
		WindSpeed = 0.0;
		PressureUnit.Empty();
		Pressure = 0.0;
		TemperatureUnit.Empty();
		Temperature = 0.0;
		Rainunit.Empty();
		Rain = 0.0;
		HumidityUnit.Empty();
		Humidity = 0.0;
		GlobalRadiationUnit.Empty();
		GlobalRadiation = 0.0;
		Pm10Unit.Empty();
		Pm10 = 0.0;
		Pm25Unit.Empty();
		Pm25 = 0.0;
		So2Unit.Empty();
		So2 = 0.0;
		No2Unit.Empty();
		No2 = 0.0;
		No24Unit.Empty();
		No24 = 0.0;
		No1Unit.Empty();
		No1 = 0.0;
		No14Unit.Empty();
		No14 = 0.0;
		CoUnit.Empty();
		Co = 0.0;
		PbUnit.Empty();
		Pb = 0.0;
	}
};

USTRUCT(Blueprintable)
struct FUserProject
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int ProjectId;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Name;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Description;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float GpsLatitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float GpsLongitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ArealMinLatitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ArealMinLongitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ArealMaxLatitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ArealMaxLongitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Image;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<FProjectItem> ProjectItems;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString BaseUri;
};

UCLASS(BlueprintType)
class ABSOLUTEDT_API UUserProjectItemViewModel : public UObject
{
	GENERATED_BODY()

public:

	UUserProjectItemViewModel()
	{
		PreviewImageFullPath = FString();
		MeshPreviewImageFullPath = FString();
	}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FProjectItem ProjectItem;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsReady = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool IsEnabledInScene = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool CancelLoading = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString DirectoryPath;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString PreviewImageFullPath;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString MeshPreviewImageFullPath;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UTexture2DDynamic* PreviewTexture;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FDateTime ValidFrom;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FDateTime ValidTo;
};

UCLASS(BlueprintType)
class ABSOLUTEDT_API UUserProjectViewModel : public UObject
{
	GENERATED_BODY()


public:

	UUserProjectViewModel()
	{
		PreviewImageFullPath = FString();
		bAnimatable = false;
	}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int ProjectId;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Name;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Description;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float GpsLatitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float GpsLongitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ArealMinLatitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ArealMinLongitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ArealMaxLatitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float ArealMaxLongitude;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Image;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<UUserProjectItemViewModel*> ProjectItems;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString BaseUri;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString PreviewImageFullPath;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FVector ArealMinimum;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FVector ArealMaximum;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool bAnimatable;
};

UENUM(BlueprintType)
enum class EMenuItemEntry : uint8
{
	ShowProjectItem
};


USTRUCT(Blueprintable)
struct FMenuAction
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<FString> Action;
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	//	EMenuItemEntry Name;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	//	FString Value;
};

USTRUCT(Blueprintable)
struct FMenuItem
{
	GENERATED_USTRUCT_BODY()

public:
	FMenuItem()
	{
		Name = FString();
		OnClickGotoProjectID = -1;
	}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString OnClickGotoProject;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 OnClickGotoProjectID;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Name;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<FMenuAction> OnClick;
};

USTRUCT(Blueprintable)
struct FMenuEntry
{
	GENERATED_USTRUCT_BODY()

public:
	FMenuEntry()
	{
		Name = FString();
	}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString Name;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<FMenuItem> Items;
};

USTRUCT(Blueprintable)
struct FUIDescription
{
	GENERATED_USTRUCT_BODY()

public:
	FUIDescription()
	{
		Menu.Empty();
	}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<FMenuEntry> Menu;
};

inline FVector GetAxisOrderedVector(FVector vector, ItemAxisOrder order)
{
	FVector Result;

	switch (order)
	{
	case ItemAxisOrder::xyz:
		Result = FVector(vector.X, vector.Y, vector.Z);
		break;
	case ItemAxisOrder::xzy:
		Result = FVector(vector.X, vector.Z, vector.Y);
		break;
	case ItemAxisOrder::yxz:
		Result = FVector(vector.Y, vector.X, vector.Z);
		break;
	case ItemAxisOrder::yzx:
		Result = FVector(vector.Y, vector.Z, vector.X);
		break;
	case ItemAxisOrder::zxy:
		Result = FVector(vector.Z, vector.X, vector.Y);
		break;
	case ItemAxisOrder::zyx:
		Result = FVector(vector.Z, vector.Y, vector.X);
		break;
	}

	// Modify for Unreal Engine
	return FVector(Result.X, Result.Z, Result.Y);

	//return Result;
}

inline void SetAxisOrderForVector(TArray<double>& vector, ItemAxisOrder order)
{
	double TempValueX, TempValueY, TempValueZ;

	switch (order)
	{
	default:
	case ItemAxisOrder::xyz:
		TempValueX = vector[0];
		TempValueY = vector[1];
		TempValueZ = vector[2];
		break;
	case ItemAxisOrder::xzy:
		TempValueX = vector[0];
		TempValueY = vector[2];
		TempValueZ = vector[1];
		break;
	case ItemAxisOrder::yxz:
		TempValueX = vector[1];
		TempValueY = vector[0];
		TempValueZ = vector[2];
		break;
	case ItemAxisOrder::yzx:
		TempValueX = vector[1];
		TempValueY = vector[2];
		TempValueZ = vector[0];
		break;
	case ItemAxisOrder::zxy:
		TempValueX = vector[2];
		TempValueY = vector[0];
		TempValueZ = vector[1];
		break;
	case ItemAxisOrder::zyx:
		TempValueX = vector[2];
		TempValueY = vector[1];
		TempValueZ = vector[0];
		break;
	}

	// Modify for Unreal Engine	
	vector[0] = TempValueX;
	vector[1] = TempValueZ;
	vector[2] = TempValueY;
}

inline double GetScaleFactor(ItemUnit unit)
{
	switch (unit)
	{
	case inch:
		return 2.54 * GLOBALSCALEFACTOR;
		break;
	case meter:
		return 100.0 * GLOBALSCALEFACTOR;
		break;
	}
}

UENUM(Blueprintable)
enum EDataManagerStatus
{
	READY,
	SERVER_TIMEOUT,
	LOGIN_STARTED,
	LOGIN_FAILED,
	LOGIN_SUCCESSFUL,
	LOGOUT_SUCCESSFUL,
	GET_PROJECTS_FAILED,
	GET_PROJECTS_COMPLETED,
	DOWNLOADING,
	DOWNLOADING_COMPLETED,
	DOWNLOAD_ERROR,
	PREPROCESSING,
	PREPROCESSING_COMPLETED,
	GET_SENSOR_DATA_SUCCESFUL,
	GET_SENSOR_DATA_FAILED
};

