/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "Components/WidgetComponent.h"

#include "ChargingStation.generated.h"

UENUM(Blueprintable)
enum EChargingPointStatus
{
	Unknown = 0,
	Available = 1,
	Charging = 2
};

USTRUCT(BlueprintType)
struct FChargingPointData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	int32 ID;

	UPROPERTY(BlueprintReadOnly)
	FString Connector;

	UPROPERTY(BlueprintReadOnly)
	FString Type;
	
	UPROPERTY(BlueprintReadOnly)
	FString StatusString;

	UPROPERTY(BlueprintReadOnly)
	FString AuthMethod1;

	UPROPERTY(BlueprintReadOnly)
	FString AuthMethod2;
};

USTRUCT(BlueprintType)
struct FChargingStationData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	int32 ID;

	UPROPERTY(BlueprintReadOnly)
	FString Name;

	UPROPERTY(BlueprintReadOnly)
	FString Address;

	UPROPERTY(BlueprintReadOnly)
	int32 ChargingStationNumber;

	UPROPERTY(BlueprintReadOnly)
	int32 NumChargingPoints;

	UPROPERTY(BlueprintReadOnly)
	int32 NumFreeChargingPoints;

	UPROPERTY(BlueprintReadOnly)
	FString GeoLocationLatString;

	UPROPERTY(BlueprintReadOnly)
	FString GeoLocationLonString;
		
	UPROPERTY(BlueprintReadOnly)
	TArray<FChargingPointData> ChargingPointsData;
};

UCLASS()
class ABSOLUTEDT_API AChargingStation : public AActor
{
	GENERATED_BODY()

public:
	AChargingStation();
	
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateDisplay();

	UPROPERTY(BlueprintReadOnly)
	FChargingStationData StationData;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	USceneComponent* Scene;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UWidgetComponent* DisplayWidget;

protected:
	virtual void BeginPlay() override;
	
private:	
};