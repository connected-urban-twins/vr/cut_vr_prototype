/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "GameFramework/Actor.h"
#include "IImageWrapper.h"
#include "ProceduralMeshComponent.h"
#include "SharedPointer.h"

#include <assimp/Importer.hpp>  // C++ importer interface
#include <assimp/scene.h>       // Output data structure
#include <assimp/postprocess.h> // Post processing flags

#include "UserProjectItem.h"
#include "ProjectItemActor.h"
#include "IfcImporter.h"

#include "RuntimeMeshActor.generated.h"

UCLASS()
class ABSOLUTEDT_API ARuntimeMeshActor : public AProjectItemActor
{
	GENERATED_BODY()

public:
	ARuntimeMeshActor();
	~ARuntimeMeshActor();
	virtual void OnConstruction(const FTransform & Transform) override;
	virtual void PreInitializeComponents() override;

	UFUNCTION(BlueprintCallable)
	void SetScalarMaterialParameter(FName ParameterName, float Value);
	
	void SetChildNodesVisibility(bool Visible, UBuildingNode* Node);

	UFUNCTION(BlueprintCallable)
	void SetNodeVisibility(bool Visible, UBuildingNode* Node);

	void ImportMesh();
	void LoadMesh();
	void UnloadMesh();
	void ResolveBuildingStructure(FImportedBuildingNode& ImportedNode, UBuildingNode* GroupNode);
	void BuildMesh();
	UTexture2DDynamic* LoadTextureFromFile(const FString& FullFilePath, bool& IsValid, int32& Width, int32& Height) const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	virtual	void SetEnabledInScene(bool enable) override;

	static void WriteRawToTexture_RenderThread(FTexture2DDynamicResource* TextureResource, TArray64<uint8>* RawData, bool bUseSRGB = true);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString FilePath;

	UPROPERTY(Transient, EditDefaultsOnly, BlueprintReadWrite)
		UProceduralMeshComponent* Mesh;
	
	UPROPERTY(BlueprintAssignable)
	FMeshFullyLoadedEvent OnMeshFullyLoaded;

	FString RealFilePath;

	TSharedPtr<FImportedMeshData> ImportedMeshData;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBuildingData* BuildingData;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool IsLoaded;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool IsLoading;

private:

	FString FindObjFile(FString& FilePath);

	UPROPERTY()
		UMaterial* BaseMaterial;

	TArray<UMaterialInstanceDynamic*> Materials;
	TArray<UTexture2DDynamic*> Textures;

	FAsyncTask<FAsyncImportMesh>* AsyncImportTask = nullptr;
	FAsyncTask<FAsyncLoadMesh>* AsyncLoadTask = nullptr;
	FCriticalSection Mutex;
	

};