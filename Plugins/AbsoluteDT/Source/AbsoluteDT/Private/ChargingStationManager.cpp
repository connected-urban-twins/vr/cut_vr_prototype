/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "ChargingStationManager.h"
#include "DigitalTwinGameInstance.h"
#include "CoordinateConverter.h"
#include "GenericPlatformHttp.h"
#include "HttpModule.h"
#include "JsonObjectConverter.h"
#include "IHttpResponse.h"
#include "PhysicsEngine/PhysicsSettings.h"

AChargingStationManager::AChargingStationManager()
{
	PrimaryActorTick.bCanEverTick = true;
	bAllowTickBeforeBeginPlay = false;

	NextLink.Empty();
	CurrentLink.Empty();
	ChargingStationActors.Empty();
	FirstRun = true;
	IsRetrievingData = false;
	ChargingStationActorsEnabled = false;
	ChargingStationClass = AActor::StaticClass();
	TimeCounter = 0.0;
	RequestTimeDelay = 20.0;
}

void AChargingStationManager::BeginPlay()
{
	Super::BeginPlay();

	GetData();
}

void AChargingStationManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if(GEngine)
	{
		UWorld* World = GetWorld();
		if (!World) return;
		auto PlayerController = GEngine->GetFirstLocalPlayerController(GetWorld());

		if (!IsValid(PlayerController)) return;
		APlayerCameraManager* CameraManager = PlayerController->PlayerCameraManager;

		FVector CameraLocation = CameraManager->GetCameraLocation();

		for (AChargingStation* ChargingStation : ChargingStationActors)
		{
			FVector WorldLocation = ChargingStation->GetActorLocation();
			FRotator LookAtCameraRotation = (CameraLocation - WorldLocation).ToOrientationRotator();

			ChargingStation->DisplayWidget->SetWorldRotation(FRotator(0, LookAtCameraRotation.Yaw, 0));			
		}
	}

	TimeCounter += DeltaTime;

	if (TimeCounter > RequestTimeDelay)
	{
		TimeCounter = 0.0;
		if (FirstRun == false && ChargingStationActors.Num() != 0)
		{
			UpdateData();
		}
	}
}

void AChargingStationManager::EnableChargingStations(bool Enable)
{
	for (AChargingStation* ChargingStation : ChargingStationActors)
	{
		ChargingStation->SetActorHiddenInGame(!Enable);
		ChargingStation->SetActorEnableCollision(Enable);
	}
	
	ChargingStationActorsEnabled = Enable;
}

void AChargingStationManager::GetData()
{
	FString Url;

	if (!IsRetrievingData)
	{
		Url =TEXT("https://iot.hamburg.de/v1.0/Things?$filter=Datastreams/properties/serviceName%20eq%20%27HH_STA_E-Ladestationen%27&$count=true&$expand=Locations,Datastreams($expand=Observations($top=1))");
	}
	else
	{
		Url = NextLink;
	}

	const TSharedRef<IHttpRequest, ESPMode::ThreadSafe> Request = FHttpModule::Get().CreateRequest();

	IsRetrievingData = true;
	Request->OnProcessRequestComplete().BindUObject(this, &AChargingStationManager::OnHttpRequestCompleted);
	Request->SetURL(Url);
	Request->SetVerb("GET");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

void AChargingStationManager::UpdateData()
{
	FString Url;

	if (!IsRetrievingData)
	{
		Url =TEXT("https://iot.hamburg.de/v1.0/Things?$filter=Datastreams/properties/serviceName%20eq%20%27HH_STA_E-Ladestationen%27&$count=true&$expand=Datastreams($expand=Observations($top=1))");
	}
	else
	{
		Url = NextLink;
	}

	const TSharedRef<IHttpRequest, ESPMode::ThreadSafe> Request = FHttpModule::Get().CreateRequest();

	IsRetrievingData = true;
	Request->OnProcessRequestComplete().BindUObject(this, &AChargingStationManager::OnUpdateRequestCompleted);
	Request->SetURL(Url);
	Request->SetVerb("GET");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

void AChargingStationManager::OnHttpRequestCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	Request->OnProcessRequestComplete().Unbind();

	UWorld* World = GetWorld();

	if (!World || !World->IsValidLowLevel())
	{
		return;
	}

	if (bWasSuccessful)
	{
		if (Response->GetResponseCode() == 200)
		{			
			TSharedPtr<FJsonObject> JsonObject;
			FString ContentString = Response->GetContentAsString();
			TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(ContentString);
			if (FJsonSerializer::Deserialize(Reader, JsonObject))
			{
				// Get the array of stations
				TArray<TSharedPtr<FJsonValue>> StationsJson = JsonObject->GetArrayField("value");
								
				FCollisionQueryParams CollisionParams;
				CollisionParams.bTraceComplex = true;
				CollisionParams.bReturnPhysicalMaterial = false;
				CollisionParams.bReturnFaceIndex = false;

				FCollisionObjectQueryParams ObjectParams = FCollisionObjectQueryParams(ECC_WorldStatic);

				for (auto StationJson : StationsJson)
				{
					FChargingStationData NewStation;
					const TSharedPtr<FJsonObject>* StationData;
					StationJson->TryGetObject(StationData);

					const TArray<TSharedPtr<FJsonValue>> LocationsJson = StationData->Get()->GetArrayField("Locations");

					if (LocationsJson.Num() > 0)
					{
						TSharedPtr<FJsonValue, ESPMode::Fast> Location = LocationsJson[0];

						const TSharedPtr<FJsonObject>* LocationData;
						if (Location->TryGetObject(LocationData))
						{
							auto StationLocation = LocationData->Get()->GetObjectField("location");
							auto LocationGeometry = StationLocation->GetObjectField("geometry");
							auto LocationPoints = LocationGeometry->GetArrayField("coordinates");
							
							NewStation.GeoLocationLonString = LocationPoints[0]->AsString();
							NewStation.GeoLocationLatString = LocationPoints[1]->AsString();

							FVector2D StationCoordinatesOnPlane = GetSceneCoordinatesFromLatLon(FCString::Atod(*NewStation.GeoLocationLatString), FCString::Atod(*NewStation.GeoLocationLonString));

							FVector LineTraceStart(StationCoordinatesOnPlane.X, StationCoordinatesOnPlane.Y, 50000 * GLOBALSCALEFACTOR);
							FVector LineTraceEnd(StationCoordinatesOnPlane.X, StationCoordinatesOnPlane.Y, -50000 * GLOBALSCALEFACTOR);

							FHitResult HitResult;

							if (World->LineTraceSingleByObjectType(HitResult, LineTraceStart, LineTraceEnd, ObjectParams, CollisionParams))
							{
								FActorSpawnParameters SpawnParameters;
								SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
								FTransform Transform = FTransform(HitResult.ImpactPoint);
								Transform.SetScale3D(FVector(GLOBALSCALEFACTOR));
								AChargingStation* ChargingStationActor = World->SpawnActorDeferred<AChargingStation>(ChargingStationClass, Transform);

								ChargingStationActor->StationData.Name = LocationData->Get()->GetStringField("name");
								ChargingStationActor->StationData.Address = LocationData->Get()->GetStringField("description");

								int NumFreeChargingPoints = 0;

								const auto ChargingPointsJson = StationData->Get()->GetArrayField("Datastreams");

								for (auto ChargingPointJson : ChargingPointsJson)
								{
									FChargingPointData NewChargingPointData;
									const TSharedPtr<FJsonObject>* ChargingPointData;
									ChargingPointJson->TryGetObject(ChargingPointData);
									auto ChargingPointProperties = ChargingPointData->Get()->GetObjectField("properties");

									NewChargingPointData.ID = ChargingPointData->Get()->GetIntegerField("@iot.id");
									NewChargingPointData.Connector = ChargingPointProperties->GetStringField("connector");
									NewChargingPointData.Type = ChargingPointProperties->GetStringField("steckerTyp");
									NewChargingPointData.AuthMethod1 = ChargingPointProperties->GetStringField("authMeth1");
									NewChargingPointData.AuthMethod2 = ChargingPointProperties->GetStringField("authMeth2");

									auto ChargingPointObservations = ChargingPointData->Get()->GetArrayField("Observations");

									if (ChargingPointObservations.Num() > 0)
									{
										auto Observation = ChargingPointObservations[0];										
										const TSharedPtr<FJsonObject>* ObservationData;

										if (Observation->TryGetObject(ObservationData))
										{
											NewChargingPointData.StatusString = ObservationData->Get()->GetStringField("result");

											if (NewChargingPointData.StatusString.Equals("available"))
											{
												NumFreeChargingPoints++;
											}
										}
									}

									ChargingStationActor->StationData.ChargingPointsData.Add(NewChargingPointData);
								}

								ChargingStationActor->StationData.NumChargingPoints = ChargingPointsJson.Num();
								ChargingStationActor->StationData.NumFreeChargingPoints = NumFreeChargingPoints;
								ChargingStationActor->StationData.ID = StationData->Get()->GetIntegerField("@iot.id");

								UGameplayStatics::FinishSpawningActor(ChargingStationActor, Transform);
								ChargingStationActor->SetActorHiddenInGame(!ChargingStationActorsEnabled);
								ChargingStationActor->SetActorEnableCollision(ChargingStationActorsEnabled);
								ChargingStationActor->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
								ChargingStationActors.Add(ChargingStationActor);
							}
						}
					}
				}

				// Get next link entry
				if (!JsonObject->TryGetStringField("@iot.nextLink", NextLink))
				{
					IsRetrievingData = false;
					FirstRun = false;
				}
				else
				{
					GetData();
				}
			}
			else
			{
			}
		}
		else
		{
		}
	}
}

void AChargingStationManager::OnUpdateRequestCompleted(const FHttpRequestPtr Request, const FHttpResponsePtr Response, bool bWasSuccessful)
{
	Request->OnProcessRequestComplete().Unbind();

	const UWorld* World = GetWorld();

	if (!World || !World->IsValidLowLevel())
	{
		return;
	}

	if (bWasSuccessful)
	{
		if (Response->GetResponseCode() == 200)
		{			
			TSharedPtr<FJsonObject> JsonObject;
			const FString ContentString = Response->GetContentAsString();
			const TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(ContentString);

			if (FJsonSerializer::Deserialize(Reader, JsonObject))
			{
				// Get the array of stations
				TArray<TSharedPtr<FJsonValue>> StationsJson = JsonObject->GetArrayField("value");

				for (const auto StationJson : StationsJson)
				{
					const TSharedPtr<FJsonObject>* StationData;
					StationJson->TryGetObject(StationData);

					int32 StationID = StationData->Get()->GetIntegerField("@iot.id");

					AChargingStation* ChargingStationActor = nullptr;

					auto FindResult = ChargingStationActors.FindByPredicate([&](const AChargingStation* Station)
						{
							return Station->StationData.ID == StationID;
						});

					if (FindResult != nullptr)
					{
						ChargingStationActor = *FindResult;
					}

					if (ChargingStationActor != nullptr)
					{
						bool UpdateStationView = false;

						const auto ChargingPointsJson = StationData->Get()->GetArrayField("Datastreams");
	
						int NumFreeChargingPoints = 0;

						for (const auto ChargingPointJson : ChargingPointsJson)
						{
							const TSharedPtr<FJsonObject>* ChargingPointData;
							ChargingPointJson->TryGetObject(ChargingPointData);
			
							auto ChargingPointObservations = ChargingPointData->Get()->GetArrayField("Observations");
							int32 ChargingPointID = ChargingPointData->Get()->GetIntegerField("@iot.id"); 

							if (ChargingPointObservations.Num() > 0)
							{
								const auto Observation = ChargingPointObservations[0];										
								const TSharedPtr<FJsonObject>* ObservationData;

								if (Observation->TryGetObject(ObservationData))
								{
									auto ChargingPoints = ChargingStationActor->StationData.ChargingPointsData;
									const auto TargetChargingPoint = ChargingPoints.FindByPredicate([&](FChargingPointData ChargingPoint)
										{
											return ChargingPoint.ID == ChargingPointID;
										});

									if (TargetChargingPoint != nullptr)
									{
										FString NewStatusString = ObservationData->Get()->GetStringField("result");

										if (!TargetChargingPoint->StatusString.Equals(NewStatusString))
										{
											TargetChargingPoint->StatusString = NewStatusString;
											UpdateStationView = true;
										}
										
										if (NewStatusString.Equals("available"))
										{
											NumFreeChargingPoints++;
										}
									}
								}
							}								
						}

						if (UpdateStationView)
						{
							ChargingStationActor->StationData.NumChargingPoints = ChargingPointsJson.Num();
							ChargingStationActor->StationData.NumFreeChargingPoints = NumFreeChargingPoints;
							ChargingStationActor->UpdateDisplay();							
						}
					}
				}

				// Get next link entry
				if (!JsonObject->TryGetStringField("@iot.nextLink", NextLink))
				{
					IsRetrievingData = false;
				}
			}
			else
			{
			}
		}
		else
		{
		}
	}
}