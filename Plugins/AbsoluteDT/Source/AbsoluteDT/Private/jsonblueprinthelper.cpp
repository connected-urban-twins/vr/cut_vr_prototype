/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "AbsoluteDTPrivatePCH.h"

#include "JsonBlueprintHelper.h"
#include "Paths.h"
#include "FileHelper.h"
#include "JsonReader.h"
#include "JsonSerializer.h"

// Sets default values
UJsonBlueprintHelper::UJsonBlueprintHelper()
{
}

// Called when the game starts
void UJsonBlueprintHelper::BeginPlay()
{
	Super::BeginPlay();
}

//
//public string Id{ get; set; }
//public string Gattung{ get; set; }
//public string GattungLatein{ get; set; }
//public string GattungDeutsch{ get; set; }
//public string Art{ get; set; }
//public string ArtLatein{ get; set; }
//public string ArtDeutsch{ get; set; }
//public string Pflanzjahr{ get; set; }
//public string Kronendurchmesser{ get; set; }
//public string KronenDmAnzahl{ get; set; }
//public string Stammumfang{ get; set; }
//public string Stammumfangzahl{ get; set; }
//public string Strasse{ get; set; }
//public string Hausnummer{ get; set; }
//public string OrtsteilNr{ get; set; }
//public string StandBearbeitung{ get; set; }
//public string Bezirk{ get; set; }
//public float X{ get; set; }
//public float Y{ get; set; }
//public float Z{ get; std::set; }

//USTRUCT()
//struct FHpaTree
//{
//	GENERATED_USTRUCT_BODY()
//
//	UPROPERTY()
//		FString Id;
//	UPROPERTY()
//		FString Gattung;
//	UPROPERTY()
//		FString GattungLatein;
//	UPROPERTY()
//		FString GattungDeutsch;
//	UPROPERTY()
//		FString Art;
//	UPROPERTY()
//		FString ArtLatein;
//	UPROPERTY()
//		FString ArtDeutsch;
//	UPROPERTY()
//		FString Pflanzjahr;
//	UPROPERTY()
//		FString Kronendurchmesser;
//	UPROPERTY()
//		FString KronenDmAnzahl;
//	UPROPERTY()
//		FString Stammumfang;
//	UPROPERTY()
//		FString Stammumfangzahl;
//	UPROPERTY()
//		FString Strasse;
//	UPROPERTY()
//		FString Hausnummer;
//	UPROPERTY()
//		FString OrtsteilNr;
//	UPROPERTY()
//		FString StandBearbeitung;
//	UPROPERTY()
//		FString Bezirk;
//	UPROPERTY()
//		FString X;
//	UPROPERTY()
//		FString Y;
//	UPROPERTY()
//		FString Z;
//};

void UJsonBlueprintHelper::LoadJsonFile()
{
	FString JsonString;

	FString path = FPaths::Combine(*FPaths::ProjectDir(), *FString("Content/Base/JSON/trees.json"));
	FFileHelper::LoadFileToString(JsonString, *path);

	TSharedRef< TJsonReader<TCHAR> > Reader = TJsonReaderFactory<TCHAR>::Create(*JsonString);



	if (FJsonSerializer::Deserialize(Reader, JsonArray) )
	{
		for (int i = 0; i < JsonArray.Num(); i++)
		{
			TSharedPtr<FJsonValue> t = JsonArray[i];
			TSharedPtr<FJsonObject> st = t->AsObject();
		}
	}
}

int32 UJsonBlueprintHelper::GetJsonArrayLength()
{
	return JsonArray.Num();
}

FString UJsonBlueprintHelper::GetStringFieldAtIndex(int32 index, FString name)
{
	return JsonArray[index]->AsObject()->GetStringField(name);
}

float UJsonBlueprintHelper::GetFloatFieldAtIndex(int32 index, FString name)
{
	return JsonArray[index]->AsObject()->GetNumberField(name);
}