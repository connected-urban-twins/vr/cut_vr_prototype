/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "DataManager.h"

#include "FileManagerGeneric.h"
#include "GenericPlatformHttp.h"
#include "HttpManager.h"
#include "IHttpResponse.h"
#include "JsonObjectConverter.h"

#include "ZipFileFunctionLibrary.h"

#include "JsonHelper.h"
#include "CoordinateConverter.h"
#include "RuntimeMeshActor.h"
#include "VrPawn.h"
#include "IImageWrapperModule.h"
#include "ImageUtils.h"

#include "DigitalTwinGameInstance.h"

IPlatformFile* PlatformFile2 = nullptr;

ADataManager::ADataManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = false;

	if (PlatformFile2 == nullptr)
	{
		PlatformFile2 = &FPlatformFileManager::Get().GetPlatformFile();
		if (PlatformFile2->GetLowerLevel())
		{
			PlatformFile2 = PlatformFile2->GetLowerLevel();
		}
	}

	Http = &FHttpModule::Get();

	FString RootPath = FPaths::ConvertRelativePathToFull(FPaths::ProjectDir());

	CacheDirectory = FPaths::Combine(RootPath, FString("UserCache"));

	DownloadManager = CreateDefaultSubobject<UFileDownloadManager>(TEXT("DownloadManager"));

	LoadSettings();

	bStickersVisible = true;

	// UserLoggedIn = false;
	// IsUpdating = false;
	bAutoInitScene = false;

	MediaViewerClass = AMediaViewer::StaticClass();
	BuildingClass = ARuntimeMeshActor::StaticClass();

	CurrentStatus = EDataManagerStatus::READY;
	MeshPreviewImageFilename = FString("DT_PreviewImage.png");

	//	TArray<AActor*> FoundActors;
	//	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	//
	//	UGameplayStatics::GetAllActorsOfClass(World, ADataManager::StaticClass(), FoundActors);
}


void ADataManager::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (bAutoInitScene)
	{
		InitScene();
	}
}

// Called when the game starts or when spawned
void ADataManager::BeginPlay()
{
	Super::BeginPlay();

}

void ADataManager::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (DownloadManager && DownloadManager->IsValidLowLevel())
	{
		DownloadManager->DeleteAllTasks();
	}

	for (auto Project : UserProjects)
	{
		for (auto ProjectItem : Project->ProjectItems)
		{
			ProjectItem->CancelLoading = true;
		}
	}

	ClearScene();
}

void ADataManager::ProcessProjectItem(const int ProjectID, const int ProjectItemID)
{
}

void ADataManager::InitScene()
{
	ClearScene();
	LoadProjectsData();

	LoadMenuData();

	CurrentActiveProject = nullptr;

	for (auto Project : UserProjects)
	{
		for (auto ProjectItem : Project->ProjectItems)
		{
			SpawnProjectItem(ProjectItem);
		}
	}
}

void ADataManager::UpdateStatus(EDataManagerStatus Status, FString Message)
{
	CurrentStatus = Status;
	OnStatusUpdated.Broadcast(Status, Message);
}

void ADataManager::OnDownloadCompleted(const FTaskInformation& InInfo)
{
	if (DownloadManager->GetAllTasksCompleted())
	{
		UpdateStatus(EDataManagerStatus::DOWNLOADING_COMPLETED, "");

		DownloadManager->DeleteAllTasks();

		OnDownloadProgressUpdated.Broadcast(0.0);
		UpdateStatus(EDataManagerStatus::PREPROCESSING, "");
	
		StartPreprocessing();
	}
}

void ADataManager::OnDownloadsUpdated(float Progress)
{
	OnDownloadProgressUpdated.Broadcast(Progress);
}

void ADataManager::StartPreprocessing()
{
	NumMeshesToPreprocess = 0;
	NumMeshesPreprocessed = 0;

	TArray<UUserProjectItemViewModel*> ProjectItemsToUpdate;

	for (UUserProjectViewModel* Project : UserProjects)
	{
		for (UUserProjectItemViewModel* ProjectItem : Project->ProjectItems)
		{
			auto ItemType = ProjectItem->ProjectItem.Type;
			switch (ItemType)
			{
			case ItemType::cad:
			case ItemType::obj_ls310:
			case ItemType::obj_ls320:
			case ItemType::ifc:
			case ItemType::ifc_ls310:
			case ItemType::ifc_ls320:
			{		
				bool bShouldUpdate = true;

				FString FullPath = FPaths::Combine(ProjectItem->DirectoryPath, FString("DT_Mesh.bin"));
				bool BinFileExists = PlatformFile2->FileExists(*FullPath);

				// Check if there is already a mesh bin file and if it's valid
				if (BinFileExists)
				{
					FImportedMeshData MeshImportData;

					// We need to only deserialize the header, otherwise the client will raise an exception
					// when trying to deserialize the following TArrays,
					// if the read bin file structure from the current FImportedMeshData structure
					bool LoadedSuccessfully = MeshImportData.LoadFromDisk(FullPath, true);

					// If deserialization was successful, check if the version number
					// and the original projectitem filename match up
					if (LoadedSuccessfully && MeshImportData.Header.Version == MESHIMPORT_VERSION &&
						MeshImportData.Header.Filename == FPaths::GetCleanFilename(ProjectItem->ProjectItem.Val))
					{
						// The file is valid, no need to preprocess this file again
						bShouldUpdate = false;
					}
				}

				if (bShouldUpdate)
				{			
					ProjectItemsToUpdate.Add(ProjectItem);
				}

				break;
			}

			default:
				break;
			}
		}
	}

	NumMeshesToPreprocess = ProjectItemsToUpdate.Num();

	if (NumMeshesToPreprocess == 0)
	{
		// IsUpdating = false;
		UpdateStatus(EDataManagerStatus::PREPROCESSING_COMPLETED, "");
		OnDownloadProgressUpdated.Broadcast(1.0);
		return;
	}
    
	for (UUserProjectItemViewModel* ProjectItem : ProjectItemsToUpdate)
	{
		ARuntimeMeshActor* MeshActor = SpawnRuntimeMesh(ProjectItem->ProjectItem.Val, ProjectItem);

		if (MeshActor != nullptr)
		{
			MeshActor->OnMeshFullyLoaded.AddDynamic(this, &ADataManager::OnMeshPreProcessingFinished);
			MeshActor->ImportMesh();
		}
	}
}

void ADataManager::OnMeshPreProcessingFinished(ARuntimeMeshActor* MeshActor, bool Success)
{
	MeshActor->UnloadMesh();
	bool DestroySuccessful = MeshActor->Destroy();
	GEngine->ForceGarbageCollection(true);
	NumMeshesPreprocessed++;
	
	OnDownloadProgressUpdated.Broadcast((float)NumMeshesPreprocessed / (float)NumMeshesToPreprocess);

	if (NumMeshesPreprocessed == NumMeshesToPreprocess)
	{
		// IsUpdating = false;
		UpdateStatus(EDataManagerStatus::PREPROCESSING_COMPLETED, "");
	}
}

// Called every frame
void ADataManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

FVector2D ADataManager::GetProjectCenter(UUserProjectViewModel* project)
{
	double CenterLat = FMath::Lerp(project->ArealMinLatitude, project->ArealMaxLatitude, 0.5);
	double CenterLon = FMath::Lerp(project->ArealMinLongitude, project->ArealMaxLongitude, 0.5);

	FVector2D Position = GetObjectPosition(CenterLat, CenterLon);
	return Position;
}

void ADataManager::TeleportToProject(UUserProjectViewModel* project)
{
	AVrPawn* Pawn = Cast<AVrPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (Pawn != nullptr)
	{
		double CenterLat = FMath::Lerp(project->ArealMinLatitude, project->ArealMaxLatitude, 0.5);
		double CenterLon = FMath::Lerp(project->ArealMinLongitude, project->ArealMaxLongitude, 0.5);

		FVector2D Position = GetObjectPosition(CenterLat, CenterLon);

		Pawn->Teleport(FVector(Position, 5000.0f * GLOBALSCALEFACTOR));
	}
}

void ADataManager::FilterByDate(FDateTime Date)
{
	ValidDate = Date;
	SetItemsVisibility(true);
}

void ADataManager::SetItemsVisibility(bool TimeRangeMode)
{
	if (TimeRangeMode)
	{
		for (auto Actor : ProjectItemsInScene)
		{
			AProjectItemActor* ItemActor = Cast<AProjectItemActor>(Actor);

			if (ValidDate >= ItemActor->ProjectItem->ValidFrom &&
				ValidDate <= ItemActor->ProjectItem->ValidTo)
			{
				ItemActor->SetEnabledInScene(true);
			}
			else
			{
				ItemActor->SetEnabledInScene(false);
			}
		}
	}
	else
	{
		if (CurrentActiveProject == nullptr)
		{
			for (auto Actor : ProjectItemsInScene)
			{
				AProjectItemActor* ItemActor = Cast<AProjectItemActor>(Actor);
				ItemActor->SetEnabledInScene(false);
			}
		}
		else
		{
			for (auto Actor : ProjectItemsInScene)
			{
				AProjectItemActor* ItemActor = Cast<AProjectItemActor>(Actor);

				if (ItemActor->ProjectItem == nullptr)
				{
					break;
				}

				if (ItemActor->ProjectItem->ProjectItem.ProjectId != CurrentActiveProject->ProjectId)
				{
					ItemActor->SetEnabledInScene(false);
				}
				else
				{
					switch (ItemActor->ProjectItem->ProjectItem.Type)
					{
					case ItemType::cad:
					case ItemType::obj_ls310:
					case ItemType::obj_ls320:
					case ItemType::ifc:
					case ItemType::ifc_ls310:
					case ItemType::ifc_ls320:
						{
							ARuntimeMeshActor* MeshActor = Cast<ARuntimeMeshActor>(ItemActor);
							if (MeshActor != nullptr)
							{
								MeshActor->LoadMesh();
							}

							ItemActor->SetEnabledInScene(ItemActor->ProjectItem->IsEnabledInScene);
							break;
						}

					case ItemType::photo:
					case ItemType::movie360:
					case ItemType::photo360:
					case ItemType::movie:
						ItemActor->SetEnabledInScene(bStickersVisible);
						break;

					default:
						ItemActor->SetEnabledInScene(true);
						break;
					}
				}
			}

			bool ShowSpecialParkingLot = false;

			for (auto ProjectItem : CurrentActiveProject->ProjectItems)
			{
				if (ProjectItem->ProjectItem.Type == ItemType::OutdoorSensorParking)
				{
					ShowSpecialParkingLot = true;
					break;
				}
			}
		}
	}

}

bool ADataManager::SetActiveProject(int ProjectID)
{
	if (UserProjects.Num() == 0)
	{
		return false;
	}

	UUserProjectViewModel* Project = *UserProjects.FindByPredicate([&](UUserProjectViewModel* Project)
	{
		return Project->ProjectId == ProjectID;
	});

	if (!Project || !Project->IsValidLowLevel())
	{
		return false;
	}

	CurrentActiveProject = Project;

	//if (CurrentActiveProject != Project)
	//{
	SetItemsVisibility(false);
	//}

	return true;
}

UUserProjectViewModel* ADataManager::FindProject(int32 ProjectID)
{
	if(UserProjects.Num() > 0)
	{
		UUserProjectViewModel* Project = *UserProjects.FindByPredicate([&](UUserProjectViewModel* Project)
		{
			return Project->ProjectId == ProjectID;
		});
		return Project;
	}
	return nullptr;
}

UUserProjectItemViewModel* ADataManager::FindProjectItem(int32 ProjectID, int32 ProjectItemID)
{
	UUserProjectViewModel* Project = FindProject(ProjectID);

	if (Project == nullptr)
	{
		return nullptr;
	}

	UUserProjectItemViewModel** ProjectItem = Project->ProjectItems.FindByPredicate([&](UUserProjectItemViewModel* ProjectItem)
	{
		return ProjectItem->ProjectItem.ProjectItemId == ProjectItemID;
	});

	if (ProjectItem == nullptr)
	{
		return nullptr;
	}

	return *ProjectItem;
}

bool ADataManager::SaveSettings()
{
	SaveConfig();

	return true;
}

bool ADataManager::LoadSettings()
{
	// LoadConfig();
	
	return true;
}

void ADataManager::ClearScene()
{
	for (auto actor : ProjectItemsInScene)
	{
		if (actor && actor->IsValidLowLevel())
		{
			actor->Destroy();
		}
	}

	ProjectItemsInScene.Empty();
}


AProjectItemActor* ADataManager::GetProjectItemInScene(UUserProjectItemViewModel* ProjectItem)
{
	AProjectItemActor* ProjectItemActor = *ProjectItemsInScene.FindByPredicate([&](AProjectItemActor* ProjectItemActor)
	{
		return ProjectItemActor->ProjectItem == ProjectItem;
	});

	return ProjectItemActor;
}

FVector ADataManager::GetSceneLocationFromLatLon(double lat, double lon)
{
	FVector2D XyPosition = GetObjectPosition(lat, lon);

	return FVector(XyPosition, 0.0f);
}

void ADataManager::GetGpsCoordinatesFromSceneLocation(FVector Location, double& Lat, double& Lon)
{
	FVector ScaledLocation = Location * 0.01 / GLOBALSCALEFACTOR;
	ScaledLocation.X += 564000.0;
	ScaledLocation.Y = 5933000.0 - ScaledLocation.Y;

	UTMXYToLatLon(ScaledLocation.X, ScaledLocation.Y, 32, false, Lat, Lon);
}

FVector ADataManager::GetProjectItemPosition(UUserProjectItemViewModel* projectItem, bool CheckXYCoordinates)
{
	const double PositionX = projectItem->ProjectItem.TranslateX * 100.0 * GLOBALSCALEFACTOR;
	const double PositionY = -projectItem->ProjectItem.TranslateZ * 100.0 * GLOBALSCALEFACTOR;
	const double PositionZ = projectItem->ProjectItem.TranslateY * 100.0 * GLOBALSCALEFACTOR;

	if (CheckXYCoordinates && (PositionX != 0.0 || PositionY != 0.0))
	{		
		return FVector(PositionX, PositionY, PositionZ);
	}
	else
	{
		const double Lat = projectItem->ProjectItem.GpsLatitude;
		const double Lon = projectItem->ProjectItem.GpsLongitude;
		const FVector2D PositionXY = GetObjectPosition(Lat, Lon);

		if (PositionZ != 0.0)
		{
			return FVector(PositionXY, PositionZ);
		}
		else
		{
			return FVector(PositionXY, 100.0f);
		}
	}
}

FVector2D ADataManager::GetObjectPosition(double Lat, double Lon)
{
	double XPositionFromLatLon;
	double YPositionFromLatLon;

	LatLonToUTMXY(Lat, Lon, 32, XPositionFromLatLon, YPositionFromLatLon);

	XPositionFromLatLon -= 564000.0;
	YPositionFromLatLon = -(YPositionFromLatLon - 5933000.0);

	// Convert to UE units, including the global scale factor
	XPositionFromLatLon *= 100.0 * GLOBALSCALEFACTOR;
	YPositionFromLatLon *= 100.0 * GLOBALSCALEFACTOR;

	return FVector2D(XPositionFromLatLon, YPositionFromLatLon);
}

void ADataManager::SpawnProjectItem(UUserProjectItemViewModel* ProjectItem)
{
	UWorld* World = GetWorld();

	if (!World || !World->IsValidLowLevel())
	{
		return;
	}

	FActorSpawnParameters SpawnParameters;

	switch (ProjectItem->ProjectItem.Type)
	{
	case ItemType::photo:
	case ItemType::photo360:
	case ItemType::movie:
	case ItemType::movie360:
	{
		if (!MediaViewerClass || !MediaViewerClass->IsValidLowLevel())
		{
			break;
		}

		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		FVector Position = GetProjectItemPosition(ProjectItem, true);
		FTransform Transform = FTransform(FVector(Position.X, Position.Y, Position.Z));

		AMediaViewer* MediaViewer = World->SpawnActorDeferred<AMediaViewer>(MediaViewerClass, Transform);

		if (MediaViewer && MediaViewer->IsValidLowLevel())
		{
			bool Enable = false;

			if (CurrentActiveProject != nullptr)
			{
				if (ProjectItem->ProjectItem.ProjectId == CurrentActiveProject->ProjectId)
				{
					Enable = true;
				}
			}

			MediaViewer->ProjectItem = ProjectItem;
			MediaViewer->MediaType = ProjectItem->ProjectItem.Type;
			MediaViewer->SourceURL = FPaths::Combine(GetProjectItemPath(ProjectItem), ProjectItem->ProjectItem.Val);
			UGameplayStatics::FinishSpawningActor(MediaViewer, Transform);
			MediaViewer->SetEnabledInScene(Enable && bStickersVisible);
			ProjectItemsInScene.Add(MediaViewer);
		}

		break;
	}


	case ItemType::cad:
	case ItemType::obj_ls310:
	case ItemType::obj_ls320:
	case ItemType::ifc:
	case ItemType::ifc_ls310:
	case ItemType::ifc_ls320:
	{
		SpawnRuntimeMesh(ProjectItem->ProjectItem.Val, ProjectItem);
		break;
	}
	}
}

ARuntimeMeshActor* ADataManager::SpawnRuntimeMesh(FString& FileName, UUserProjectItemViewModel* ProjectItem)
{
	UWorld* World = GetWorld();

	if (!World || !World->IsValidLowLevel())
	{
		return nullptr;
	}

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	FTransform Transform = FTransform();
	Transform.SetLocation(FVector(0.0f, 0.0f, 0.0f));

	float Scalefactor = 1.0f;

	ARuntimeMeshActor* RuntimeMesh = World->SpawnActorDeferred<ARuntimeMeshActor>(BuildingClass, Transform);

	if (RuntimeMesh && RuntimeMesh->IsValidLowLevel())
	{
		RuntimeMesh->ProjectItem = ProjectItem;
		RuntimeMesh->FilePath = FPaths::Combine(GetProjectItemPath(ProjectItem), FileName);
		RuntimeMesh->SetActorRelativeScale3D(FVector(Scalefactor, Scalefactor, Scalefactor));
		UGameplayStatics::FinishSpawningActor(Cast<AActor>(RuntimeMesh), Transform);
		RuntimeMesh->SetEnabledInScene(false);

		ProjectItemsInScene.Add(RuntimeMesh);

		return RuntimeMesh;
	}

	return nullptr;
}

FString ADataManager::GetProjectsDirectory()
{
	return FPaths::Combine(CacheDirectory, FString("Projects"));
}

FString ADataManager::GetProjectPath(UUserProjectViewModel* Project)
{
	FString ProjectDirectory = FPaths::Combine(GetProjectsDirectory(), FString::FromInt(Project->ProjectId)) + "/";
	return ProjectDirectory;
}

FString ADataManager::GetProjectItemPath(UUserProjectItemViewModel* ProjectItem)
{
	FString ProjectDirectory = FPaths::Combine(GetProjectsDirectory(), FString::FromInt(ProjectItem->ProjectItem.ProjectId));
	FString ProjectItemDirectory = FPaths::Combine(ProjectDirectory, FString::FromInt(ProjectItem->ProjectItem.ProjectItemId)) + "/";
	return ProjectItemDirectory;
}

void ADataManager::LoadMenuData()
{
	FString MenusJsonString;
	FString MenusJsonPath = FPaths::Combine(FPaths::ConvertRelativePathToFull(FPaths::ProjectDir()), FString("PresentationUI.json"));

	if (!FFileHelper::LoadFileToString(MenusJsonString, *MenusJsonPath))
	{
		//OnStatusUpdated.Broadcast(EDataManagerStatus::GET_PROJECTS_FAILED, "Die Men�informationen konnten nicht geladen werden.");
		return;
	}

	FUIDescription ParsedUIDescription;

	FJsonObjectConverter::JsonObjectStringToUStruct(MenusJsonString, &UIDescription, 0, 0);
}

void ADataManager::ParseProjectsJsonFile(TArray<FUserProject>& ParsedProjects)
{
	FString ProjectsJsonString;
	FString ProjectsJsonPath = FPaths::Combine(GetProjectsDirectory(), FString("Projects.json"));

	if (!FFileHelper::LoadFileToString(ProjectsJsonString, *ProjectsJsonPath))
	{
		UpdateStatus(EDataManagerStatus::GET_PROJECTS_FAILED, "Die Projektinformationen konnten nicht geladen werden.");
		return;
	}

	FJsonObjectConverter::JsonArrayStringToUStruct(ProjectsJsonString, &ParsedProjects, 0, 0);
}

void ADataManager::LoadProjectsData()
{
	//FString ProjectsJsonString;
	//FString ProjectsJsonPath = FPaths::Combine(GetProjectsDirectory(), FString("Projects.json"));

	Projects.Empty();
	UserProjects.Empty();

	//if (!FFileHelper::LoadFileToString(ProjectsJsonString, *ProjectsJsonPath))
	//{
	//	UpdateStatus(EDataManagerStatus::GET_PROJECTS_FAILED, "Die Projektinformationen konnten nicht geladen werden.");
	//	return;
	//}

	//FJsonObjectConverter::JsonArrayStringToUStruct(ProjectsJsonString, &Projects, 0, 0);

	ParseProjectsJsonFile(Projects);
	
	for (auto Project : Projects)
	{
		UUserProjectViewModel* NewProject = NewObject<UUserProjectViewModel>();

		NewProject->ProjectId = Project.ProjectId;
		NewProject->Name = Project.Name;
		NewProject->Description = Project.Description;
		NewProject->GpsLatitude = Project.GpsLatitude;
		NewProject->GpsLongitude = Project.GpsLongitude;
		NewProject->ArealMinLatitude = Project.ArealMinLatitude;
		NewProject->ArealMinLongitude = Project.ArealMinLongitude;
		NewProject->ArealMaxLatitude = Project.ArealMaxLatitude;
		NewProject->ArealMaxLongitude = Project.ArealMaxLongitude;
		NewProject->Image = Project.Image;
		NewProject->BaseUri = Project.BaseUri;

		NewProject->ArealMinimum = GetSceneLocationFromLatLon(NewProject->ArealMinLatitude, NewProject->ArealMinLongitude);
		NewProject->ArealMaximum = GetSceneLocationFromLatLon(NewProject->ArealMaxLatitude, NewProject->ArealMaxLongitude);

		if (!NewProject->Image.IsEmpty())
		{
			NewProject->PreviewImageFullPath = FPaths::Combine(GetProjectPath(NewProject), Project.Image);
		}

		bool bProjectIsAynimatable = true;

		for (auto ProjectItem : Project.ProjectItems)
		{
			UUserProjectItemViewModel* NewProjectItem = NewObject<UUserProjectItemViewModel>();
			NewProjectItem->ProjectItem = ProjectItem;
			FString ProjectItemPath = GetProjectItemPath(NewProjectItem);
			
			NewProjectItem->DirectoryPath = ProjectItemPath;

			if (!ProjectItem.ItemImage.IsEmpty())
			{
				NewProjectItem->PreviewImageFullPath = FPaths::Combine(ProjectItemPath, ProjectItem.ItemImage);
			}

			NewProjectItem->MeshPreviewImageFullPath = FPaths::Combine(ProjectItemPath, MeshPreviewImageFilename);

			// If any ValidFrom property is empty, animation for this project should be disabled
			// because animation follows a timeline which is only given if all project items contain a valid date time entry
			if (ProjectItem.ValidFrom.IsEmpty())
			{
				bProjectIsAynimatable = false;
			}

			FDateTime ItemValidFrom;
			FDateTime ItemValidTo;
			FDateTime::ParseIso8601(*ProjectItem.ValidFrom, ItemValidFrom);
			FDateTime::ParseIso8601(*ProjectItem.ValidTo, ItemValidTo);
			NewProjectItem->ValidFrom = ItemValidFrom;
			NewProjectItem->ValidTo = ItemValidTo;

			NewProject->ProjectItems.Add(NewProjectItem);
		}

		if (bProjectIsAynimatable)
		{
			NewProject->bAnimatable = true;

			NewProject->ProjectItems.Sort([](const UUserProjectItemViewModel& ProjectItem1, const UUserProjectItemViewModel& ProjectItem2)
			{
				return  ProjectItem1.ValidFrom < ProjectItem2.ValidFrom;
			});
		}

		UserProjects.Add(NewProject);
	}

	CurrentActiveProject = nullptr;

	UpdateStatus(EDataManagerStatus::GET_PROJECTS_COMPLETED, "");

	Projects.Empty();
}

bool ADataManager::EnsureDirectoryExists(FString& Directory)
{
	if (PlatformFile2->DirectoryExists(*Directory) == false)
	{
		if (PlatformFile2->CreateDirectoryTree(*Directory) == false)
		{
			UE_LOG(LogTemp, Warning, TEXT("Cannot create directory : %s"), *Directory);
			return false;
		}
	}

	return true;
}

bool ADataManager::RemoveDirectory(FString& Directory)
{
	if (PlatformFile2->DirectoryExists(*Directory))
	{
		return PlatformFile2->DeleteDirectory(*Directory);
	}

	return true;
}

float ADataManager::GetDownloadProgress()
{
	float AccumulatedProgress = 0.0f;
	int32 NumTasks = DownloadManager->TaskList.Num();

	if (NumTasks == 0)
	{
		return 1.0f;
	}

	for (DownloadTask* Task : DownloadManager->TaskList)
	{
		if (Task != nullptr)
		{
			AccumulatedProgress += Task->GetProgress();
		}
	}

	return AccumulatedProgress / static_cast<float>(NumTasks);
}

template <typename StructType>
void ADataManager::GetJsonStringFromStruct(StructType FilledStruct, FString& StringOutput) {
	FJsonObjectConverter::UStructToJsonObjectString(StructType::StaticStruct(), &FilledStruct, StringOutput, 0, 0);
}

template <typename StructType>
void ADataManager::GetStructFromJsonString(FHttpResponsePtr Response, StructType& StructOutput) {
	StructType StructData;
	FString JsonString = Response->GetContentAsString();
	FJsonObjectConverter::JsonObjectStringToUStruct<StructType>(JsonString, &StructOutput, 0, 0);
}

FUserContext& ADataManager::GetUserContext()
{
	UDigitalTwinGameInstance* GameInstance = Cast<UDigitalTwinGameInstance>(GetGameInstance());
	return GameInstance->UserContext;

}

void ADataManager::OnUserInfoReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	TSharedPtr<FJsonObject> JsonObject;

	if (bWasSuccessful)
	{
		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			if (FJsonObjectConverter::JsonObjectToUStruct(JsonObject.ToSharedRef(), &UserInfo, 0, 0))
			{
				// Ok, we have a user info

				// UserLoggedIn = true;
				UpdateStatus(EDataManagerStatus::LOGIN_SUCCESSFUL, "");
			}
		}
		else
		{
			// UserLoggedIn = false;
			UpdateStatus(EDataManagerStatus::LOGIN_FAILED, "");
		}
	}
	else
	{
		// UserLoggedIn = false;
		UpdateStatus(EDataManagerStatus::LOGIN_FAILED, Response->GetContentAsString());
	}
}

void ADataManager::CleanupData(TArray<FUserProject>& OldProjectData, TArray<FUserProject>& NewProjectData)
{
	FString ProjectsDirectory = GetProjectsDirectory();
	FPlatformFileManager& FileManager = FPlatformFileManager::Get();
	FileManager.GetPlatformFile().SetSandboxEnabled(false);
	
	for (auto Project : NewProjectData)
	{
		FString ProjectDirectory = FPaths::Combine(ProjectsDirectory, FString::FromInt(Project.ProjectId)) + "/";

		for (auto ProjectItem : Project.ProjectItems)
		{
			bool InvalidateProjectItem = false;
			
			FUserProject* FoundProject = OldProjectData.FindByPredicate([&](FUserProject OldProject)
			{
				return OldProject.ProjectId == Project.ProjectId;
			});

			if (FoundProject != nullptr)
			{
				FProjectItem* FoundProjectItem = FoundProject->ProjectItems.FindByPredicate([&](FProjectItem OldProjectitem)
				{
					return OldProjectitem.ProjectItemId == ProjectItem.ProjectItemId;
				});

				if (FoundProjectItem != nullptr)
				{					
					if ((FoundProjectItem->TranslateX != ProjectItem.TranslateX) ||
						(FoundProjectItem->TranslateY != ProjectItem.TranslateY) ||
						(FoundProjectItem->TranslateZ != ProjectItem.TranslateZ) ||
						(FoundProjectItem->RotateX != ProjectItem.RotateX) ||
						(FoundProjectItem->RotateY != ProjectItem.RotateY) ||
						(FoundProjectItem->RotateZ != ProjectItem.RotateZ) ||
						(FoundProjectItem->ScalingFactorX != ProjectItem.ScalingFactorX) ||
						(FoundProjectItem->ScalingFactorY != ProjectItem.ScalingFactorY) ||
						(FoundProjectItem->ScalingFactorZ != ProjectItem.ScalingFactorZ) ||
						(FoundProjectItem->AxisOrder != ProjectItem.AxisOrder) ||
						(FoundProjectItem->RightHand != ProjectItem.RightHand) ||
						(FoundProjectItem->Unit != ProjectItem.Unit) ||
						(FoundProjectItem->Type != ProjectItem.Type) ||
						(FoundProjectItem->Val != ProjectItem.Val)
						)
					{
						InvalidateProjectItem = true;
					}
				}
				else
				{
					InvalidateProjectItem = true;					
				}
			}
			else
			{
				InvalidateProjectItem = true;					
			}

			if(InvalidateProjectItem)
			{
				FString ProjectItemDirectory = FPaths::Combine(ProjectDirectory, FString::FromInt(ProjectItem.ProjectItemId)) + "/";
				FString MeshBinFilePath = FPaths::Combine(ProjectItemDirectory, FString("DT_Mesh.bin"));

				FileManager.GetPlatformFile().DeleteFile(*MeshBinFilePath);
			}
		}
	}
}

void ADataManager::OnProjectsResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	FString ContentString = *Response->GetContentAsString();

	TSharedRef<TJsonReader<TCHAR>> Reader = TJsonReaderFactory<TCHAR>::Create(ContentString);

	FString Error = Reader->GetErrorMessage();

	if (Error.IsEmpty())
	{
		TArray<FUserProject> OldProjectsData;		
		ParseProjectsJsonFile(OldProjectsData);

		FString ProjectsJsonPath = FPaths::Combine(GetProjectsDirectory(), FString("Projects.json"));
		
		if (FFileHelper::SaveStringToFile(ContentString, *ProjectsJsonPath))
		{
			TArray<FUserProject> NewProjectsData;
			
			FJsonObjectConverter::JsonArrayStringToUStruct(ContentString, &NewProjectsData, 0, 0);

			CleanupData(OldProjectsData, NewProjectsData);
			
			UpdateStatus(EDataManagerStatus::GET_PROJECTS_COMPLETED, Error);
		}
		else
		{
			UpdateStatus(EDataManagerStatus::GET_PROJECTS_FAILED, Error);
		}
	}
	else
	{
		UpdateStatus(EDataManagerStatus::GET_PROJECTS_FAILED, Error);
	}
}