/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "AvateeringComponent.h"
#include "FileHelper.h"
#include "Json.h"
#include "JsonObjectConverter.h"

UAvateeringComponent::UAvateeringComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	CurrentAnimationKeyIndex = 0;
	CurrentAnimationIndex = 0;
	bIsPlaying = false;
}

void UAvateeringComponent::BeginPlay()
{
	Super::BeginPlay();
	
	AActor* OwningActor = GetOwner();
	
	if (OwningActor)
	{
		UActorComponent* FoundComponent;
		FoundComponent = OwningActor->FindComponentByClass(USkeletalMeshComponent::StaticClass());
		if (FoundComponent)
		{
			MeshReference = Cast<USkeletalMeshComponent>(FoundComponent);
		}
	}

	for (FFilePath Path : AnimationRecordingFiles)
	{
		FString FullPath = FPaths::Combine(FPaths::ProjectDir(), Path.FilePath);
		FBlendshapeRecording Recording = LoadRecording(FullPath);
		Recordings.Add(Recording);
	}
}

void UAvateeringComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bIsPlaying && MeshReference && CurrentAnimationIndex < Recordings.Num())
	{
		TArray<FBlendshapeStruct> BlendShapesArray = Recordings[CurrentAnimationIndex].BlendshapeRecordingStruct.ListOfBlendshapeStruct;

		FBlendshapeStruct Blendshape = BlendShapesArray[CurrentAnimationKeyIndex];
		for (int i=0; i < Blendshape.ListOfBlendshapeKeys.Num() - 1; i++)
		{
			MeshReference->SetMorphTarget(FName(*Blendshape.ListOfBlendshapeKeys[i]), Blendshape.ListOfBlendshapeValues[i], false);
		}
		
		CurrentAnimationKeyIndex += DeltaTime * 60.0;
		
		if (static_cast<int32>(CurrentAnimationKeyIndex) >= BlendShapesArray.Num())
		{
			bIsPlaying = false;
			
		OnBlendshapeAnimationCompleted.Broadcast(true);
		}
	}
}

// Function to parse a given JSON string into a FBlendshapeRecordings struct and return this struct
FBlendshapeRecording UAvateeringComponent::LoadRecording(FString FullPath)
{
	FBlendshapeRecording Recording;

	FString JsonString;
	FFileHelper::LoadFileToString(JsonString, *FullPath);
	
	FJsonObjectConverter::JsonObjectStringToUStruct(JsonString, &Recording, 0, 0);

	return Recording;
}

void UAvateeringComponent::PlayAnimation(int32 Index)
{
	bIsPlaying = true;
	CurrentAnimationKeyIndex = 0.0;
	CurrentAnimationIndex = Index;
}

void UAvateeringComponent::StopAnimation()
{
	bIsPlaying = false;
	CurrentAnimationKeyIndex = 0.0;
	if (MeshReference)
	{
		MeshReference->ClearMorphTargets();
	}
}


