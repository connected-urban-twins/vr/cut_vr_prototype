/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "RuntimeMeshActor.h"

#include <memory>
#include <string>
#include "Paths.h"

#include "Materials/MaterialInstanceDynamic.h"
#include "ConstructorHelpers.h"
#include "IImageWrapper.h"
#include "IImageWrapperModule.h"
#include "ModuleManager.h"
#include "FileHelper.h"
#include "BulkData.h"
#include "Engine/Texture2D.h"

#include <assimp/Importer.hpp>  // C++ importer interface
#include <assimp/scene.h>       // Output data structure
#include <assimp/postprocess.h> // Post processing flags

#include "DataManager.h"
#include "ZipFileFunctionLibrary.h"
#include "FileManagerGeneric.h"
#include "CoordinateConverter.h"
#include "DigitalTwinGameInstance.h"
#include "MeshImport.h"

//----------------------------------------------------------------------//
// UAsyncTaskDownloadImage
//----------------------------------------------------------------------//

void ARuntimeMeshActor::WriteRawToTexture_RenderThread(FTexture2DDynamicResource* TextureResource, TArray64<uint8>* RawData, bool bUseSRGB)
{
	check(IsInRenderingThread());

	if (TextureResource)
	{
		FRHITexture2D* TextureRHI = TextureResource->GetTexture2DRHI();

		int32 Width = TextureRHI->GetSizeX();
		int32 Height = TextureRHI->GetSizeY();

		uint32 DestStride = 0;
		uint8* DestData = reinterpret_cast<uint8*>(RHILockTexture2D(TextureRHI, 0, RLM_WriteOnly, DestStride, false, false));

		for (int32 y = 0; y < Height; y++)
		{
			uint8* DestPtr = &DestData[((int64)Height - 1 - y) * DestStride];

			const FColor* SrcPtr = &((FColor*)(RawData->GetData()))[((int64)Height - 1 - y) * Width];
			for (int32 x = 0; x < Width; x++)
			{
				*DestPtr++ = SrcPtr->B;
				*DestPtr++ = SrcPtr->G;
				*DestPtr++ = SrcPtr->R;
				*DestPtr++ = SrcPtr->A;
				SrcPtr++;
			}
		}

		RHIUnlockTexture2D(TextureRHI, 0, false, false);
	}

	delete RawData;
}

// Sets default values
ARuntimeMeshActor::ARuntimeMeshActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = false;

	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = SceneComponent;

	Mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("ProceduralMeshComponent"));
	Mesh->bUseAsyncCooking = false;
	Mesh->bUseComplexAsSimpleCollision = false;
	Mesh->AttachToComponent(SceneComponent, FAttachmentTransformRules::KeepRelativeTransform);
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh->SetupAttachment(SceneComponent);
	Mesh->CastShadow = false;
	Mesh->bVisibleInRayTracing = false;

	static ConstructorHelpers::FObjectFinder<UMaterial> Material(TEXT("Material'/AbsoluteDT/CADImport/CADImportMaterial.CADImportMaterial'"));
	if (Material.Object != nullptr)
	{
		BaseMaterial = static_cast<UMaterial*>(Material.Object);
	}

	ImportedMeshData = MakeShareable(new FImportedMeshData());
	ImportedMeshData->bSuccess = false;
	ImportedMeshData->ImportedMeshSectionInfos.Empty();

	BuildingData = NewObject<UBuildingData>();

	IsLoading = false;
	IsLoaded = false;
}

ARuntimeMeshActor::~ARuntimeMeshActor()
{
	if (AsyncImportTask)
	{
		AsyncImportTask->Cancel();
		AsyncImportTask->EnsureCompletion(false);
		delete AsyncImportTask;
		AsyncImportTask = nullptr;
	}

	if (AsyncLoadTask)
	{
		AsyncLoadTask->EnsureCompletion(false);
		delete AsyncLoadTask;
		AsyncLoadTask = nullptr;
	}

	Materials.Empty();
	ProjectItem = nullptr;
}

void ARuntimeMeshActor::OnConstruction(const FTransform & Transform)
{
	Super::OnConstruction(Transform);

	if (GetWorld() && GetWorld()->IsValidLowLevel())
	{
		if (!FPaths::FileExists(FilePath))
		{
			return;
		}
	}
}

void ARuntimeMeshActor::PreInitializeComponents()
{
	Super::PreInitializeComponents();
}

void ARuntimeMeshActor::SetScalarMaterialParameter(FName ParameterName, float Value)
{
	TArray <class UMaterialInterface*> Materials = Mesh->GetMaterials();

	for (UMaterialInterface* Material : Materials)
	{
		UMaterialInstanceDynamic* MaterialInstance = Cast<UMaterialInstanceDynamic>(Material);

		if (MaterialInstance != nullptr)
		{
			MaterialInstance->SetScalarParameterValue(ParameterName, Value);
		}
	}
}

void ARuntimeMeshActor::SetChildNodesVisibility(bool Visible, UBuildingNode* Node)
{
	Node->bIsEnabled = Visible;
	bool IsMeshSectionVisible = Visible ? Node->bIsVisible : false;

	for (auto MeshSectionIndex : Node->MeshSectionIndices)
	{
		Mesh->SetMeshSectionVisible(MeshSectionIndex, IsMeshSectionVisible);
	}

	for (auto Child : Node->Children)
	{
		SetChildNodesVisibility(Visible, Child);
	}
}

void ARuntimeMeshActor::SetNodeVisibility(bool Visible, UBuildingNode* Node)
{
	if (Visible)
	{
		Node->bIsVisible = true;
	}
	else
	{
		Node->bIsVisible = false;
	}

	for (auto MeshSectionIndex : Node->MeshSectionIndices)
	{
		Mesh->SetMeshSectionVisible(MeshSectionIndex, Visible);
	}

	for (auto Child : Node->Children)
	{
		SetChildNodesVisibility(Visible, Child);
	}
}

void ARuntimeMeshActor::ImportMesh()
{
	if (IsFullyLoaded || IsLoading)
	{
		return;
	}
		
	IsLoading = true;

	Mesh->ClearAllMeshSections();

	if (RealFilePath.IsEmpty())
	{
		UE_LOG(LogTemp, Warning, TEXT("Runtime Mesh Loader: filepath is empty.\n"));

		if(OnMeshFullyLoaded.IsBound()) //<~~~~
		{
			OnMeshFullyLoaded.Broadcast(this, false);
		}

		return;
	}

	AsyncImportTask = new FAsyncTask<FAsyncImportMesh>(this, RealFilePath);
	AsyncImportTask->StartBackgroundTask(static_cast<UDigitalTwinGameInstance*>(GetGameInstance())->ThreadPool);
}

void ARuntimeMeshActor::LoadMesh()
{
	if (IsFullyLoaded || IsLoading)
	{
		return;
	}

	AsyncLoadTask = new FAsyncTask<FAsyncLoadMesh>(this, RealFilePath);
	AsyncLoadTask->StartBackgroundTask(static_cast<UDigitalTwinGameInstance*>(GetGameInstance())->ThreadPool);
}

void ARuntimeMeshActor::UnloadMesh()
{
	Mesh->ClearAllMeshSections();
	
	for  (auto Material : Materials)
	{
		Material->SetTextureParameterValue("DiffuseTexture", nullptr);
	}
	
	Materials.Empty();

	for  (auto Texture : Textures)
	{
		Texture->ReleaseResource();
		Texture = nullptr;
	}
	
	Textures.Empty();
	
	if (IsFullyLoaded)
	{
		IsFullyLoaded = false;
		IsLoaded = false;
	}
}

void ARuntimeMeshActor::ResolveBuildingStructure(FImportedBuildingNode& ImportedNode, UBuildingNode* GroupNode)
{
	auto Children = ImportedNode.Children;
	int NumChildren = Children.Num();
	int LastElementIndex = NumChildren - 1;

	GroupNode->bHasChildren = (NumChildren > 0);

	for (int Index = 0; Index < Children.Num(); Index++)
	{
		auto Child = Children[Index];
		UBuildingNode* NewNode = NewObject<UBuildingNode>();
		NewNode->Name = Child.Name;
		NewNode->Level = Child.Level;
		NewNode->MeshActor = this;

		if (NumChildren >> 1 && Index < LastElementIndex)
		{
			NewNode->bIsLastElement = false;
		}

		NewNode->MeshSectionIndices.Append(Child.MeshSectionIndices);

		GroupNode->Children.Add(NewNode);
		ResolveBuildingStructure(Child, NewNode);
	}
}

void ARuntimeMeshActor::BuildMesh()
{		
	if (AsyncImportTask)
	{
		AsyncImportTask->EnsureCompletion(false);
		delete AsyncImportTask;
		AsyncImportTask = nullptr;
	}

	if (AsyncLoadTask)
	{
		AsyncLoadTask->EnsureCompletion(false);
		delete AsyncLoadTask;
		AsyncLoadTask = nullptr;
	}
		
	FString ThePath = FPaths::ConvertRelativePathToFull(FPaths::ProjectDir(), RealFilePath);
	FString DirectoryPath = FPaths::GetPath(ThePath);
	
	Materials.Empty();

	for (uint32 i = 0; i < ImportedMeshData->Materials.Num(); i++)
	{
		UMaterialInstanceDynamic* DynMaterial = UMaterialInstanceDynamic::Create(BaseMaterial, this);

		DynMaterial->SetVectorParameterValue("DiffuseColor", ImportedMeshData->Materials[i].DiffuseColor);

		if (ImportedMeshData->Materials[i].DiffuseTexture.Len() > 0)
		{
			FString texturePath = FPaths::Combine(DirectoryPath, ImportedMeshData->Materials[i].DiffuseTexture);

			bool IsValid;
			int32 Width;
			int32 Height;
			UTexture2DDynamic* newTexture = LoadTextureFromFile(texturePath, IsValid, Width, Height);

			if (newTexture != nullptr)
			{
				DynMaterial->SetTextureParameterValue("DiffuseTexture", newTexture);
				Textures.Add(newTexture);
			}
		}

		DynMaterial->SetScalarParameterValue("Metallic", ImportedMeshData->Materials[i].Metallic);

		DynMaterial->SetScalarParameterValue("Roughness", ImportedMeshData->Materials[i].Roughness);

		DynMaterial->SetScalarParameterValue("Opacity", ImportedMeshData->Materials[i].Opacity);

		Materials.Add(DynMaterial);
	}

	int NumMeshes = ImportedMeshData->ImportedMeshSectionInfos.Num();

	for (uint32 i = 0; i < NumMeshes; ++i)
	{
		Mesh->CreateMeshSection(i, ImportedMeshData->ImportedMeshSectionInfos[i].Vertices, ImportedMeshData->ImportedMeshSectionInfos[i].Triangles, ImportedMeshData->ImportedMeshSectionInfos[i].Normals, ImportedMeshData->ImportedMeshSectionInfos[i].UV0, ImportedMeshData->ImportedMeshSectionInfos[i].VertexColors, ImportedMeshData->ImportedMeshSectionInfos[i].Tangents, false);
		Mesh->SetMaterial(i, Materials[ImportedMeshData->ImportedMeshSectionInfos[i].SectionIndex]);
	}

	for (auto ImportedNode : ImportedMeshData->ImportedBuildingNodes)
	{
		UBuildingNode* NewNode = NewObject<UBuildingNode>();
		NewNode->Name = ImportedNode.Name;
		NewNode->bHasParent = false;
		NewNode->Level = 0;
		NewNode->MeshActor = this;

		BuildingData->BuildingNodes.Add(NewNode);
		ResolveBuildingStructure(ImportedNode, NewNode);
	}

	ImportedMeshData->Clear();

	SetScalarMaterialParameter("TwoSided",ProjectItem->ProjectItem.BackfaceCulling == true ? 1.0 : 0.0);
	SetScalarMaterialParameter("SelfIllum", static_cast<float>(ProjectItem->ProjectItem.SelfIllumination) / 150.0);
	
	ImportedMeshData->bSuccess = true;
	ProjectItem->IsReady = true;
	
	IsLoading = false;
	IsFullyLoaded = true;
	
	if(OnMeshFullyLoaded.IsBound()) //<~~~~
	{
		OnMeshFullyLoaded.Broadcast(this, true);
	}
}

UTexture2DDynamic* ARuntimeMeshActor::LoadTextureFromFile(const FString& FullFilePath, bool& IsValid, int32& Width, int32& Height) const
{
	#if !UE_SERVER

	IImageWrapperModule& ImageWrapperMod = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));

	IsValid = false;
	UTexture2D* LoadedT2D = nullptr;

	//Load From File
	TArray<uint8> RawFileData;
	if (!FFileHelper::LoadFileToArray(RawFileData, *FullFilePath))
	{
		return nullptr;
	}

	EImageFormat ImageFormat = ImageWrapperMod.DetectImageFormat(RawFileData.GetData(), RawFileData.Num());

	if (ImageFormat == EImageFormat::Invalid)
	{
		return nullptr;
	}

	TSharedPtr<IImageWrapper> ImageWrapper = ImageWrapperMod.CreateImageWrapper(ImageFormat);

	//Create T2D!
	if (ImageWrapper.IsValid() && ImageWrapper->SetCompressed(RawFileData.GetData(), RawFileData.Num()))
	{
		TArray64<uint8>* RawData = new TArray64<uint8>();
		if (ImageWrapper->GetRaw(ERGBFormat::BGRA, 8, *RawData))
		{
			if ( UTexture2DDynamic* Texture = UTexture2DDynamic::Create(ImageWrapper->GetWidth(), ImageWrapper->GetHeight()) )
			{
				Texture->SRGB = true;
				Texture->UpdateResource();

				FTexture2DDynamicResource* TextureResource = static_cast<FTexture2DDynamicResource*>(Texture->Resource);
				if (TextureResource)
				{
					ENQUEUE_RENDER_COMMAND(FWriteRawDataToTexture)(
						[TextureResource, RawData](FRHICommandListImmediate& RHICmdList)
						{
							WriteRawToTexture_RenderThread(TextureResource, RawData);
						});
				}
				else
				{
					delete RawData;
				}					
				return Texture;
			}
		}
	}
	
#endif
	
	return nullptr;
}

// Called when the game starts or when spawned
void ARuntimeMeshActor::BeginPlay()
{
	Super::BeginPlay();
	
	RealFilePath = FindObjFile(FilePath);
}

void ARuntimeMeshActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

FString ARuntimeMeshActor::FindObjFile(FString& filePath)
{
	FString Extension;
	TEnumAsByte<ItemType> ItemType = ProjectItem->ProjectItem.Type;

	if (ItemType == cad || ItemType == obj_ls310 || ItemType == obj_ls320)
	{
		Extension = "obj";
	}

	if (ItemType == ifc || ItemType == ifc_ls310 || ItemType == ifc_ls320)
	{
		Extension = "ifc";
	}
	
	FString BasePath = FPaths::GetPath(filePath);

	TArray<FString> FileNames;
	FFileManagerGeneric FileMgr;
	FileMgr.SetSandboxEnabled(false);
	FString wildcard("*." + Extension);
	FString search_path(FPaths::Combine(BasePath, *wildcard));

	FileMgr.FindFilesRecursive(FileNames, *BasePath, *wildcard,	true, false);

	if (FileNames.Num() > 0)
	{
		return FileNames[0];
	}
	
	return FString();
}

// Called every frame
void ARuntimeMeshActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsFullyLoaded)
	{
		return;
	}

	if (IsLoaded)
	{
		BuildMesh();
	}
}

void ARuntimeMeshActor::SetEnabledInScene(bool enable)
{
	bEnabledInScene = enable;

	if (enable)
	{
		if (!IsFullyLoaded && !IsLoading)
		{			
			LoadMesh();
		}
	}
	//else
	//{
	//	UnloadMesh();
	//}

	//Mesh->SetVisibility(enable, true);

	//if (bHidden == enable)
	//{
		SetActorHiddenInGame(!enable);
	//}
}
