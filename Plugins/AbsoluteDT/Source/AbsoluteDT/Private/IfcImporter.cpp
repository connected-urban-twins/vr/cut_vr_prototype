/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "IfcImporter.h"


IfcImporter::IfcImporter()
{
	m_geom_settings = shared_ptr<GeometrySettings>(new GeometrySettings());
}

IfcImporter::~IfcImporter()
{
}

void IfcImporter::ApplyAppearances(const std::vector<shared_ptr<AppearanceData> >& vec_product_appearances, FImportMaterial& Material)
{
	if (vec_product_appearances.size() > 0)
	{
		for (size_t ii = 0; ii < vec_product_appearances.size(); ++ii)
		{
			const shared_ptr<AppearanceData>& appearance = vec_product_appearances[ii];
			if (!appearance)
			{
				continue;
			}

			if (appearance->m_apply_to_geometry_type == AppearanceData::GEOM_TYPE_SURFACE || appearance->m_apply_to_geometry_type == AppearanceData::GEOM_TYPE_ANY)
			{
				convertToImportMaterial(appearance, Material);
			}
			else if (appearance->m_apply_to_geometry_type == AppearanceData::GEOM_TYPE_CURVE)
			{

			}
		}
	}
}

void IfcImporter::ApplyAppearanceDiffuseColor(const std::vector<shared_ptr<AppearanceData> >& vec_product_appearances, FColor& Color)
{
	if (vec_product_appearances.size() > 0)
	{
		for (size_t ii = 0; ii < vec_product_appearances.size(); ++ii)
		{
			const shared_ptr<AppearanceData>& appearance = vec_product_appearances[ii];
			if (!appearance)
			{
				continue;
			}

			if (appearance->m_apply_to_geometry_type == AppearanceData::GEOM_TYPE_SURFACE || appearance->m_apply_to_geometry_type == AppearanceData::GEOM_TYPE_ANY)
			{
				const float color_diffuse_r = appearance->m_color_diffuse.r();
				const float color_diffuse_g = appearance->m_color_diffuse.g();
				const float color_diffuse_b = appearance->m_color_diffuse.b();
				const float color_diffuse_a = appearance->m_color_diffuse.a();

				Color = FColor(color_diffuse_r * 255, color_diffuse_g * 255, color_diffuse_b * 255, 255);
			}
			else if (appearance->m_apply_to_geometry_type == AppearanceData::GEOM_TYPE_CURVE)
			{

			}
		}
	}
}

void IfcImporter::convertToImportMaterial(const shared_ptr<AppearanceData>& Appearance, FImportMaterial& Material)
{
	if (!Appearance)
	{
		return;
	}

	const float shininess = Appearance->m_shininess;
	const float transparency = Appearance->m_transparency;
	const bool set_transparent = Appearance->m_set_transparent;

	const float color_ambient_r = Appearance->m_color_ambient.r();
	const float color_ambient_g = Appearance->m_color_ambient.g();
	const float color_ambient_b = Appearance->m_color_ambient.b();
	const float color_ambient_a = Appearance->m_color_ambient.a();

	const float color_diffuse_r = Appearance->m_color_diffuse.r();
	const float color_diffuse_g = Appearance->m_color_diffuse.g();
	const float color_diffuse_b = Appearance->m_color_diffuse.b();
	const float color_diffuse_a = Appearance->m_color_diffuse.a();

	const float color_specular_r = Appearance->m_color_specular.r();
	const float color_specular_g = Appearance->m_color_specular.g();
	const float color_specular_b = Appearance->m_color_specular.b();
	const float color_specular_a = Appearance->m_color_specular.a();

//	Material.DiffuseColor = FLinearColor(color_diffuse_r, color_diffuse_g, color_diffuse_b, color_diffuse_a);

	//mat->setAmbient(osg::Material::FRONT_AND_BACK, ambientColor);
	//mat->setDiffuse(osg::Material::FRONT_AND_BACK, diffuseColor);
	//mat->setSpecular(osg::Material::FRONT_AND_BACK, specularColor);
	//mat->setShininess(osg::Material::FRONT_AND_BACK, shininess);
	//mat->setColorMode(osg::Material::SPECULAR);

	if (Appearance->m_set_transparent)
	{
		//Material.Opacity = FMath::Clamp(1.0 - transparency, 0.0, 1.0);
		Material.Opacity = 1.0;
		//mat->setTransparency(osg::Material::FRONT_AND_BACK, transparency);
	}

	if (Appearance->m_specular_exponent != 0.0)
	{
		//osg::ref_ptr<osgFX::SpecularHighlights> spec_highlights = new osgFX::SpecularHighlights();
		//spec_highlights->setSpecularExponent( spec->m_value );
		// todo: add to scenegraph
	}
}

void IfcImporter::drawMeshSet(const shared_ptr<carve::mesh::MeshSet<3> >& meshset, FColor& VertexColor, FImportedMeshSectionInfo& MeshInfo, double crease_angle = M_PI * 0.05, bool add_color_array = false)
{
	if (!meshset)
	{
		return;
	}

  	int32 VertexIndex = MeshInfo.Vertices.Num();

	TArray<FVector> vertices_tri;
	TArray<FVector> normals_tri;

	TArray<FVector> vertices_quad;
	TArray<FVector> normals_quad;

	const size_t max_num_faces_per_vertex = 10000;
	std::map<carve::mesh::Face<3>*, double> map_face_area;
	std::map<carve::mesh::Face<3>*, double>::iterator it_face_area;

	if (crease_angle > 0)
	{
		for (size_t i_mesh = 0; i_mesh < meshset->meshes.size(); ++i_mesh)
		{
			const carve::mesh::Mesh<3>* mesh = meshset->meshes[i_mesh];
			const size_t num_faces = mesh->faces.size();
			for (size_t i_face = 0; i_face != num_faces; ++i_face)
			{
				carve::mesh::Face<3>* face = mesh->faces[i_face];
				// compute area of projected face:
				std::vector<vec2> projected;
				face->getProjectedVertices(projected);
				double face_area = carve::geom2d::signedArea(projected);
				map_face_area[face] = abs(face_area);
			}
		}
	}

	for (size_t i_mesh = 0; i_mesh < meshset->meshes.size(); ++i_mesh)
	{
		const carve::mesh::Mesh<3>* mesh = meshset->meshes[i_mesh];

		const size_t num_faces = mesh->faces.size();
		for (size_t i_face = 0; i_face != num_faces; ++i_face)
		{
			carve::mesh::Face<3>* face = mesh->faces[i_face];
			const size_t n_vertices = face->nVertices();

			if (n_vertices > 4)
			{
				//drawFace(face, geode);
				continue;
			}

			const vec3 face_normal = face->plane.N;

			if (crease_angle > 0)
			{
				carve::mesh::Edge<3>* e = face->edge;
				for (size_t jj = 0; jj < n_vertices; ++jj)
				{
					carve::mesh::Vertex<3>* vertex = e->vert;
					vec3 intermediate_normal;

//					// collect all faces at vertex
//					//              | ^
//					//              | |
//					//  f1   e->rev | | e    face
//					//              v |
//					// <---e1-------   <---------------
//					//------------->   --------------->
//					//              |  ^
//					//              |  |
//					//              v  |
					carve::mesh::Edge<3>* e1 = e;// ->rev->next;
					carve::mesh::Face<3>* f1 = e1->face;
//#ifdef _DEBUG
//					if (f1 != face)
//					{
//						std::cout << "f1 != face" << std::endl;
//					}
//#endif
					for (size_t i3 = 0; i3 < max_num_faces_per_vertex; ++i3)
					{
						if (!e1->rev)
						{
							break;
						}
						if (!e1->rev->next)
						{
							break;
						}

						vec3 f1_normal = f1->plane.N;
						const double cos_angle = dot(f1_normal, face_normal);
						if (cos_angle > 0)
						{
							const double deviation = std::abs(cos_angle - 1.0);
							if (deviation < crease_angle)
							{
								double weight = 0.0;
								it_face_area = map_face_area.find(f1);
								if (it_face_area != map_face_area.end())
								{
									weight = it_face_area->second;
								}
								intermediate_normal += weight * f1_normal;
							}
						}

						if (!e1->rev)
						{
							// it's an open mesh
							break;
						}

						e1 = e1->rev->next;
						if (!e1)
						{
							break;
						}
						f1 = e1->face;
//#ifdef _DEBUG
//						if (e1->vert != vertex)
//						{
//							std::cout << "e1->vert != vertex" << std::endl;
//						}
//#endif
						if (f1 == face)
						{
							break;
						}
					}

					const double intermediate_normal_length = intermediate_normal.length();

					if (intermediate_normal_length < 0.0000000001)
					{
						intermediate_normal = face_normal;
					}
					else
					{
						// normalize:
						intermediate_normal *= 1.0 / intermediate_normal_length;
					}

					const vec3& vertex_v = vertex->v;
					if (face->n_edges == 3)
					{						
						FVector4 Vertex = FVector(vertex_v.x, -vertex_v.y, vertex_v.z);
						FVector Normal = FVector(intermediate_normal.x, -intermediate_normal.y, intermediate_normal.z);
						
						int32 Index = MeshInfo.Vertices.Add(Vertex);
						MeshInfo.Normals.Add(Normal);
						MeshInfo.Triangles.Add(Index);
						MeshInfo.VertexColors.Add(FColor(VertexColor));
					}
					else if (face->n_edges == 4)
					{
						vertices_quad.Push(FVector(vertex_v.x, -vertex_v.y, vertex_v.z));
						normals_quad.Push(FVector(intermediate_normal.x, -intermediate_normal.y, intermediate_normal.z));
					}

					e = e->next;
				}
			}
			else
			{
				carve::mesh::Edge<3>* e = face->edge;
				for (size_t jj = 0; jj < n_vertices; ++jj)
				{
					carve::mesh::Vertex<3>* vertex = e->vert;
					const vec3& vertex_v = vertex->v;

					if (face->n_edges == 3)
					{
						FVector4 Vertex = FVector(vertex_v.x, vertex_v.y, vertex_v.z);
						FVector Normal = FVector(face_normal.x, face_normal.y, face_normal.z);

						int32 Index = MeshInfo.Vertices.Add(Vertex);
						MeshInfo.Normals.Add(Normal);
						MeshInfo.Triangles.Add(Index);
						MeshInfo.VertexColors.Add(FColor(VertexColor));
					}
					else if (face->n_edges == 4)
					{
						vertices_quad.Push(FVector(vertex_v.x, vertex_v.y, vertex_v.z));
						normals_quad.Push(FVector(face_normal.x, face_normal.y, face_normal.z));
					}
					e = e->next;
				}
			}
		}
	}
}

FMatrix convertMatrixToOSG(const carve::math::Matrix& mat_in)
{
	FMatrix newMatrix;

	newMatrix.M[0][0] = mat_in.m[0][0];
	newMatrix.M[1][0] = mat_in.m[1][0];
	newMatrix.M[2][0] = mat_in.m[2][0];
	newMatrix.M[3][0] = mat_in.m[3][0];

	newMatrix.M[0][1] = mat_in.m[0][1];
	newMatrix.M[1][1] = mat_in.m[1][1];
	newMatrix.M[2][1] = mat_in.m[2][1];
	newMatrix.M[3][1] = mat_in.m[3][1];

	newMatrix.M[0][2] = mat_in.m[0][2];
	newMatrix.M[1][2] = mat_in.m[1][2];
	newMatrix.M[2][2] = mat_in.m[2][2];
	newMatrix.M[3][2] = mat_in.m[3][2];

	newMatrix.M[0][3] = mat_in.m[0][3];
	newMatrix.M[1][3] = mat_in.m[1][3];
	newMatrix.M[2][3] = mat_in.m[2][3];
	newMatrix.M[3][3] = mat_in.m[3][3];

	return newMatrix;
}

void IfcImporter::convertProductShapeToUnrealMesh(double GaussKrugerGeoLocationOffsetX, double GaussKrugerGeoLocationOffsetY, TArray<int32>& MeshSectionsIndices, TSharedPtr<FImportedMeshData> Result, shared_ptr<ProductShapeData>& product_shape, FImportMaterial& Material)
{
	for (auto Transform : product_shape->m_vec_transforms)
	{
		if (Transform->m_matrix._42 > 300000)
		{
			//Transform->m_matrix = carve::math::Matrix::IDENT();
			Transform->m_matrix._41 += GaussKrugerGeoLocationOffsetX;
			Transform->m_matrix._42 += GaussKrugerGeoLocationOffsetY;
			//Transform->m_matrix._42 = -Transform->m_matrix._42;
			//Transform->m_matrix._41 = 00000.0;
			//Transform->m_matrix._42 = 0.0;

		}
	}

	carve::math::Matrix matrix = product_shape->getTransform();

	product_shape->applyTransformToProduct(matrix, true);

	if (product_shape->m_ifc_object_definition.expired())
	{
		return;
	}

	shared_ptr<IfcObjectDefinition> ifc_object_def(product_shape->m_ifc_object_definition);
	shared_ptr<IfcProduct> ifc_product = dynamic_pointer_cast<IfcProduct>(ifc_object_def);

	if (!ifc_product)
	{
		return;
	}

	const int product_id = ifc_product->m_entity_id;
	std::stringstream strs_product_switch_name;
	strs_product_switch_name << "#" << product_id << "=" << ifc_product->className() << " group";

	FImportedMeshSectionInfo MeshInfo;
	MeshInfo.SectionIndex = 0;

	FColor CurrentColor = FColor(255, 255, 255, 255);

	std::vector<shared_ptr<RepresentationData> >& vec_product_representations = product_shape->m_vec_representations;
	for (size_t ii_representation = 0; ii_representation < vec_product_representations.size(); ++ii_representation)
	{
		const shared_ptr<RepresentationData>& product_representation_data = vec_product_representations[ii_representation];
		if (product_representation_data->m_ifc_representation.expired())
		{
			continue;
		}
		shared_ptr<IfcRepresentation> ifc_representation(product_representation_data->m_ifc_representation);
		const int representation_id = ifc_representation->m_entity_id;

		const std::vector<shared_ptr<ItemShapeData> >& product_items = product_representation_data->m_vec_item_data;
		for (size_t i_item = 0; i_item < product_items.size(); ++i_item)
		{
			const shared_ptr<ItemShapeData>& item_shape = product_items[i_item];

			ApplyAppearanceDiffuseColor(item_shape->m_vec_item_appearances, CurrentColor);

			// create shape for open shells
			for (size_t ii = 0; ii < item_shape->m_meshsets_open.size(); ++ii)
			{
				shared_ptr<carve::mesh::MeshSet<3> >& item_meshset = item_shape->m_meshsets_open[ii];
				CSG_Adapter::retriangulateMeshSet(item_meshset);

				drawMeshSet(item_meshset, CurrentColor, MeshInfo, m_geom_settings->getCoplanarFacesMaxDeltaAngle());

//				//// disable back face culling for open meshes
			}

			// create shape for meshsets
			for (size_t ii = 0; ii < item_shape->m_meshsets.size(); ++ii)
			{
				shared_ptr<carve::mesh::MeshSet<3> >& item_meshset = item_shape->m_meshsets[ii];
				CSG_Adapter::retriangulateMeshSet(item_meshset);

				drawMeshSet(item_meshset, CurrentColor, MeshInfo, m_geom_settings->getCoplanarFacesMaxDeltaAngle());
			}

			// apply statesets if there are any
			if (item_shape->m_vec_item_appearances.size() > 0)
			{
				ApplyAppearances(item_shape->m_vec_item_appearances, Material);
			}
		}

		// apply statesets if there are any
		if (product_representation_data->m_vec_representation_appearances.size() > 0)
		{
			ApplyAppearances(product_representation_data->m_vec_representation_appearances, Material);
		}

		// If anything has been created, add it to the product group
//		if (representation_switch->getNumChildren() > 0)
//		{
////#ifdef _DEBUG
////			if (representation_switch->getNumParents() > 0)
////			{
////				std::cout << __FUNC__ << ": product_representation_switch->getNumParents() > 0" << std::endl;
////			}
////#endif
//			// enable transparency for certain objects
//			if (dynamic_pointer_cast<IfcSpace>(ifc_product))
//			{
//				representation_switch->setStateSet(m_glass_stateset);
//			}
//			else if (dynamic_pointer_cast<IfcCurtainWall>(ifc_product) || dynamic_pointer_cast<IfcWindow>(ifc_product))
//			{
//				representation_switch->setStateSet(m_glass_stateset);
//				SceneGraphUtils::setMaterialAlpha(representation_switch, 0.6f);
//			}
// 
//			map_representation_switches.insert(std::make_pair(representation_id, representation_switch));
//		}
	}

	int32 NewMeshSectionIndex = Result->ImportedMeshSectionInfos.Add(MeshInfo);
	MeshSectionsIndices.Add(NewMeshSectionIndex);
}

void IfcImporter::resolveProjectStructure(TMap<int32, TArray<int32>>& MapMeshSections, const shared_ptr<ProductShapeData>& product_data, FImportedBuildingNode& group)
{
	if (!product_data)
	{
		return;
	}

	if (product_data->m_ifc_object_definition.expired())
	{
		return;
	}

	shared_ptr<IfcObjectDefinition> object_def(product_data->m_ifc_object_definition);
	const int entity_id = object_def->m_entity_id;

	const std::vector<shared_ptr<ProductShapeData> >& vec_children = product_data->getChildren();
	for (size_t ii = 0; ii < vec_children.size(); ++ii)
	{
		const shared_ptr<ProductShapeData>& child_product_data = vec_children[ii];
		if (!child_product_data)
		{
			continue;
		}

		FImportedBuildingNode group_subparts = FImportedBuildingNode();

		if (!child_product_data->m_ifc_object_definition.expired())
		{
			shared_ptr<IfcObjectDefinition> child_obj_def(child_product_data->m_ifc_object_definition);

			if (child_obj_def && child_obj_def->m_Name)
			{
				group_subparts.Name = child_obj_def->m_Name->m_value.c_str();
			}
		}

		group_subparts.Level = group.Level + 1;

		TArray<int32>* FoundMeshSections = MapMeshSections.Find(child_product_data->m_entity_id);
		if (FoundMeshSections)
		{
			group_subparts.MeshSectionIndices.Append(*FoundMeshSections);
		}

		int32 NewChildIndex = group.Children.Add(group_subparts);
		resolveProjectStructure(MapMeshSections, child_product_data, group.Children[NewChildIndex]);
	}
}

void IfcImporter::ImportFile(FString FilePath, double GaussKrugerGeoLocationOffsetX, double GaussKrugerGeoLocationOffsetY, TSharedPtr<FImportedMeshData>& MeshData, UBuildingData* BuildingData)
{
	wCharFilePath = TCHAR_TO_WCHAR(*FilePath);

	std::shared_ptr<BuildingModel> Model;
	std::shared_ptr<ReaderSTEP> ModelReader;

	Model = shared_ptr<BuildingModel>(new BuildingModel());
	
	m_geometry_converter = shared_ptr<GeometryConverter>(new GeometryConverter(Model));
	m_geometry_converter->clearMessagesCallback();
	m_geometry_converter->resetModel();
	
	// load file to IFC model
	ModelReader = shared_ptr<ReaderSTEP>(new ReaderSTEP());
	ModelReader->loadModelFromFile(wCharFilePath, m_geometry_converter->getBuildingModel());



	shared_ptr<IfcProject> IfcProjectPtr = Model->getIfcProject();

	if (IfcProjectPtr)
	{
		if (IfcProjectPtr->m_LongName && IfcProjectPtr->m_LongName->m_value.size() > 0)
		{
			BuildingData->IfcProjectLongName = IfcProjectPtr->m_LongName->m_value.c_str();
		}

		if (IfcProjectPtr->m_Phase && IfcProjectPtr->m_Phase->m_value.size() > 0)
		{
			BuildingData->IfcProjectPhase = IfcProjectPtr->m_Phase->m_value.c_str();
		}

		if (Model->getFileName().size() > 0)
		{
			BuildingData->FileName = Model->getFileName().c_str();	
		}	
	}

	// convert IFC geometric representations into Carve geometry
	m_geometry_converter->convertGeometry();

	shared_ptr<ProductShapeData> ifc_project_data;
	shared_ptr<IfcRoot> IfcProjectRoot;

	std::vector<shared_ptr<ProductShapeData> > vec_products;

	std::map<int, shared_ptr<ProductShapeData>> map_shape_data = m_geometry_converter->getShapeInputData();

	for (auto it = map_shape_data.begin(); it != map_shape_data.end(); ++it)
	{
		shared_ptr<ProductShapeData> shape_data = it->second;
		if (shape_data)
		{
			vec_products.push_back(shape_data);

			weak_ptr<IfcObjectDefinition>& ifc_object_def_weak = shape_data->m_ifc_object_definition;
			if (ifc_object_def_weak.expired())
			{
				continue;
			}

			shared_ptr<IfcObjectDefinition> ifc_object_def(ifc_object_def_weak);

			std::stringstream thread_err;
			if (dynamic_pointer_cast<IfcFeatureElementSubtraction>(ifc_object_def))
			{
				// geometry will be created in method subtractOpenings
				continue;
			}
			else if (dynamic_pointer_cast<IfcProject>(ifc_object_def))
			{
				ifc_project_data = shape_data;
				IfcProjectRoot = dynamic_pointer_cast<IfcRoot>(ifc_object_def);
			}
		}
	}


	TMap<int32, TArray<int32>> MapMeshSections;

	const int num_products = (int)vec_products.size();

	if (num_products > 0)
	{
		shared_ptr<ProductShapeData>& shape_data = vec_products[0];
		shape_data->m_vec_transforms;
	}

	for (int i = 0; i < num_products; ++i)
	{
		shared_ptr<ProductShapeData>& shape_data = vec_products[i];

		weak_ptr<IfcObjectDefinition>& ifc_object_def_weak = shape_data->m_ifc_object_definition;
		if (ifc_object_def_weak.expired())
		{
			continue;
		}

		shared_ptr<IfcObjectDefinition> ifc_object_def(ifc_object_def_weak);

		std::stringstream thread_err;
		if (dynamic_pointer_cast<IfcFeatureElementSubtraction>(ifc_object_def))
		{
			// geometry will be created in method subtractOpenings
			continue;
		}
		else if (dynamic_pointer_cast<IfcProject>(ifc_object_def))
		{
			ifc_project_data = shape_data;
		}

		shared_ptr<IfcProduct> ifc_product = dynamic_pointer_cast<IfcProduct>(ifc_object_def);
		if (!ifc_product)
		{
			continue;
		}

		if (!ifc_product->m_Representation)
		{
			continue;
		}

		// Create a default material
		FImportMaterial Material(FLinearColor(0.6f, 0.6f, 0.6f), "", 1.0f, 0.8f, 1.0f);

		TArray<int32> MeshSectionsIndices;

		try
		{
			convertProductShapeToUnrealMesh(GaussKrugerGeoLocationOffsetX, GaussKrugerGeoLocationOffsetY, MeshSectionsIndices, MeshData, shape_data, Material);
			MapMeshSections.Add(shape_data->m_entity_id, MeshSectionsIndices);
		}
		catch (OutOfMemoryException& e)
		{
			throw e;
		}
		catch (BuildingException& e)
		{
			thread_err << e.what();
		}
		catch (carve::exception& e)
		{
			thread_err << e.str();
		}
		catch (std::exception& e)
		{
			thread_err << e.what();
		}
		catch (...)
		{
			thread_err << "undefined error, entity id " << shape_data->m_entity_id;
		}

		// apply statesets if there are any
		const std::vector<shared_ptr<AppearanceData> >& vec_product_appearances = shape_data->getAppearances();

		MeshData->Materials.Push(Material);
		
		if (thread_err.tellp() > 0)
		{
//			messageCallback(thread_err.str().c_str(), StatusCallback::MESSAGE_TYPE_ERROR, __FUNC__);
		}

	}

	try
	{
		// now resolve spatial structure
		if (ifc_project_data)
		{
			if (IfcProjectRoot)
			{
				FImportedBuildingNode NewNode = FImportedBuildingNode();

				if (IfcProjectRoot->m_Name)
				{
					NewNode.Name = IfcProjectRoot->m_Name->m_value.c_str();					
				}

				NewNode.Level = 0;

				int32 NewNodeIndex = MeshData->ImportedBuildingNodes.Add(NewNode);
				resolveProjectStructure(MapMeshSections, ifc_project_data, MeshData->ImportedBuildingNodes[NewNodeIndex]);
			}
		}
	}
	catch (OutOfMemoryException& e)
	{
		throw e;
	}
	catch (BuildingException& e)
	{
		//messageCallback(e.what(), StatusCallback::MESSAGE_TYPE_ERROR, "");
		throw e;
	}
	catch (std::exception& e)
	{
		//messageCallback(e.what(), StatusCallback::MESSAGE_TYPE_ERROR, "");
		throw e;
	}
	catch (...)
	{
		//messageCallback("undefined error", StatusCallback::MESSAGE_TYPE_ERROR, __FUNC__);
	}

	MapMeshSections.Empty();

	m_geometry_converter->clearInputCache();
}
