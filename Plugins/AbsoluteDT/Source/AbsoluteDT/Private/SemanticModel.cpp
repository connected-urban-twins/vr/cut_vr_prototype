/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "SemanticModel.h"

#include "JsonObjectConverter.h"

ASemanticModel::ASemanticModel()
{
	PrimaryActorTick.bCanEverTick = true;
	bAllowTickBeforeBeginPlay = false;

	MeshesWithMetadata.Empty();
}

void ASemanticModel::BeginPlay()
{
	CollectMeshesWithMetadata();

	Super::BeginPlay();	
}

void ASemanticModel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool ASemanticModel::RetrieveMetaData(FString Name)
{
	FString MetadataFileName = Name.RightChop(3); // remove the 'SM_'
	MetadataFileName.Append(".json"); // append the json file extension

	const FString FilePath = FPaths::Combine(FPaths::ProjectContentDir(), MetadataPath.Path, MetadataFileName);
	
	if (FPaths::FileExists(FilePath))
	{
		TArray<uint8> FileContent;

		if (FFileHelper::LoadFileToArray(FileContent, *FilePath ))
		{
			FString JsonString = MyBytesToString(FileContent.GetData(), FileContent.Num());
			
			TSharedPtr<FJsonObject> JsonObject;
			const TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<TCHAR>::Create(JsonString);

			if (FJsonSerializer::Deserialize(Reader, JsonObject))
			{
				Metadata.Empty();
				auto ValueArray = JsonObject->Values;
				for (auto Entry : ValueArray)
				{
					FString KeyString = static_cast<FString>(Entry.Key);

					FString ValueString = Entry.Value->AsString();
					FSemanticModelMetadataEntry NewMetadataEntry;
					NewMetadataEntry.Name = KeyString;
					NewMetadataEntry.Value = ValueString;

					Metadata.Add(NewMetadataEntry);
				}

				return true;
			}
		}
	}

	return false;
}


void ASemanticModel::CollectMeshesWithMetadata()
{
	auto FoundMeshComponents = GetComponentsByClass(UStaticMeshComponent::StaticClass());

	for (auto ActorComponent : FoundMeshComponents)
	{
		UStaticMeshComponent* MeshComponent = Cast<UStaticMeshComponent>(ActorComponent);

		FString Name = MeshComponent->GetName();
		FString MetadataFileName = Name.RightChop(3); // remove the 'SM_'
		MetadataFileName.Append(".json"); // append the json file extension

		const FString FilePath = FPaths::Combine(FPaths::ProjectContentDir(), MetadataPath.Path, MetadataFileName);
		
		if (FPaths::FileExists(FilePath))
		{
			MeshesWithMetadata.Add(MeshComponent);
		}
	}
}

