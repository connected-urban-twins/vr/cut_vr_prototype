/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "MeshImport.h"
#include "MemoryReader.h"
#include "FileHelper.h"

#include "CoordinateConverter.h"

#include "FileManagerGeneric.h"

#include "RuntimeMeshActor.h"


FString FAsyncImportMesh::FindFile(FString BasePath, FString FileName)
{
	FString Extension;
	
	TArray<FString> FileNames;
	FFileManagerGeneric FileMgr;
	FileMgr.SetSandboxEnabled(false);
	FString wildcard("*." + Extension);
	FString search_path(FPaths::Combine(BasePath, *FileName));

	FileMgr.FindFilesRecursive(FileNames, *BasePath, *FileName,	true, false);

	if (FileNames.Num() > 0)
	{
		FString RelativeFilePath = FileNames[0];
		FPaths::MakePathRelativeTo(RelativeFilePath, *BasePath);
		return RelativeFilePath;
	}
	
	return FString();
}

void FAsyncImportMesh::DoWork()
{
	{
		FScopeLock ScopeLock(&Mutex);

		if (!MeshActor || !MeshActor->IsValidLowLevel() ||
			!MeshActor->ProjectItem || !MeshActor->ProjectItem->IsValidLowLevel())
		{
			return;
		}

		MeshActor->IsLoading = true;
		
		FString ThePath = FPaths::ConvertRelativePathToFull(FPaths::ProjectContentDir(), File);
		std::string file = TCHAR_TO_UTF8(*ThePath);
		
		UUserProjectItemViewModel* ProjectItem = MeshActor->ProjectItem;
		TEnumAsByte<ItemType> ItemType = ProjectItem->ProjectItem.Type;

		FVector2D GeoLocationOffset = FVector2D(0.0, 0.0);
		double GeoLocationOffsetX = 0.0;
		double GeoLocationOffsetY = 0.0;
		double GaussKrugerGeoLocationOffsetX = 0.0;
		double GaussKrugerGeoLocationOffsetY = 0.0;

		bool IsUTM32 = true;


		ItemAxisOrder ItemAxisOrder = MeshActor->ProjectItem->ProjectItem.AxisOrder;
		FVector Translate = 100.0 * GLOBALSCALEFACTOR * FVector(MeshActor->ProjectItem->ProjectItem.TranslateX, -MeshActor->ProjectItem->ProjectItem.TranslateZ, MeshActor->ProjectItem->ProjectItem.TranslateY);
		FVector Scale = 100.0 * GLOBALSCALEFACTOR * FVector(MeshActor->ProjectItem->ProjectItem.ScalingFactorX, MeshActor->ProjectItem->ProjectItem.ScalingFactorZ, MeshActor->ProjectItem->ProjectItem.ScalingFactorY);
		FRotator Rotate = FRotator(MeshActor->ProjectItem->ProjectItem.RotateX, MeshActor->ProjectItem->ProjectItem.RotateY,-MeshActor->ProjectItem->ProjectItem.RotateZ);


		switch (ItemType)
		{
			case ifc_ls310:
			case obj_ls310:
				GeoLocationOffsetX = -564000.0;
				GeoLocationOffsetY = -5933000.0;
				GeoLocationOffset = FVector2D(-564000.0, -5933000.0);
				GaussKrugerGeoLocationOffsetX = -3564101.271;
				GaussKrugerGeoLocationOffsetY = -5934932.786;
				IsUTM32 = true;

				break;

			case ifc_ls320:
			case obj_ls320:
				GeoLocationOffsetX = -563924.37;
				GeoLocationOffsetY = -5933441.14;
				GeoLocationOffset = FVector2D(-563924.37, -5933441.14);
				GaussKrugerGeoLocationOffsetX = -3564025.610;
				GaussKrugerGeoLocationOffsetY = -5935374.113;
				IsUTM32 = true;
				break;

			default:
				break;
		}

                if (ItemType == obj_ls310 || ItemType == obj_ls320) 
		{
                  Scale = 100.0 * GLOBALSCALEFACTOR * FVector(1.0, -1.0, 1.0);
                  ItemAxisOrder = xzy;                 
                }
		
		TSharedPtr<FImportedMeshData> result = MeshActor->ImportedMeshData;

		result->Header.Version = MESHIMPORT_VERSION;
		result->Header.Filename = FPaths::GetCleanFilename(ProjectItem->ProjectItem.Val);


		if (ItemType == ifc || ItemType == ifc_ls310 || ItemType == ifc_ls320)
		{
			TSharedPtr<IfcImporter> IfcModelImporter = MakeShareable(new IfcImporter);

			IfcModelImporter->ImportFile(MeshActor->RealFilePath,
										 GaussKrugerGeoLocationOffsetX, GaussKrugerGeoLocationOffsetY,
										 result, MeshActor->BuildingData);
					
			IfcModelImporter = nullptr;
	 
			int32 NumMeshInfos = result->ImportedMeshSectionInfos.Num();

			FVector Vertex;
			FVector Normal;

			for (int m = 0; m < NumMeshInfos; m++)
			{
				int32 NumVertices = result->ImportedMeshSectionInfos[m].Vertices.Num();
				
				for (int i = 0; i < NumVertices; i++)
				{
					Vertex = result->ImportedMeshSectionInfos[m].Vertices[i];
					Normal = result->ImportedMeshSectionInfos[m].Normals[i];

					Vertex = Rotate.RotateVector(Vertex);
					Normal = Rotate.RotateVector(Normal);
					Vertex *= Scale;
					Vertex += Translate;
					
					result->ImportedMeshSectionInfos[m].Vertices[i] = Vertex;
					result->ImportedMeshSectionInfos[m].Normals[i] = Normal;
				}
			}		
		}

		if (ItemType == cad || ItemType == obj_ls310 || ItemType == obj_ls320)
		{
			TSharedPtr<Assimp::Importer> Importer;
			Importer = MakeShareable(new Assimp::Importer());

			if (Importer.IsValid())
			{
//				Importer->ReadFile(file, aiProcess_Triangulate | aiProcess_GenNormals | aiProcess_CalcTangentSpace | aiProcess_RemoveRedundantMaterials | aiProcess_OptimizeMeshes);
				Importer->ReadFile(file, aiProcess_Triangulate | aiProcess_RemoveRedundantMaterials);

				const aiScene* mScenePtr = Importer->GetScene();

				FString DirectoryPath = FPaths::GetPath(ThePath);

				if (mScenePtr == nullptr || !mScenePtr->HasMeshes())
				{
					UE_LOG(LogTemp, Warning, TEXT("Runtime Mesh Loader: Read mesh file failure.\n"));
					
					MeshActor->IsLoaded = true;
					return;
				}

				result->ImportedMeshSectionInfos.SetNum(mScenePtr->mNumMeshes, true);

				FVector Vertex;
				TArray<double> DVertex;
				DVertex.SetNum(3);

				for (uint32 i = 0; i < mScenePtr->mNumMeshes; ++i)
				{
					const auto Mesh = mScenePtr->mMeshes[i];

					result->ImportedMeshSectionInfos[i].SectionIndex = Mesh->mMaterialIndex;

					const int32 NumVertices = Mesh->mNumVertices;

					result->ImportedMeshSectionInfos[i].Vertices.Reserve(NumVertices);

					if (Mesh->HasNormals())
					{
						result->ImportedMeshSectionInfos[i].Normals.Reserve(Mesh->mNormals->Length());
					}

					if (Mesh->HasTextureCoords(0))
					{
						result->ImportedMeshSectionInfos[i].UV0.Reserve(NumVertices);
					}

					if (Mesh->HasTangentsAndBitangents())
					{
						result->ImportedMeshSectionInfos[i].Tangents.Reserve(NumVertices);
					}

					if (Mesh->HasVertexColors(0))
					{
						result->ImportedMeshSectionInfos[i].VertexColors.Reserve(NumVertices);
					}
					
					for (uint32 j = 0; j < Mesh->mNumVertices; ++j)
					{
						// Use double for more precision, needed for models with baked-in geo coordinates 
						DVertex[0] = Mesh->mVertices[j].x;
						DVertex[1] = Mesh->mVertices[j].y;
						DVertex[2] = Mesh->mVertices[j].z;						

						SetAxisOrderForVector(DVertex, ItemAxisOrder);

						if (!IsUTM32)
						{
							GaussKrugerToUTM32(DVertex);
						}

						DVertex[0] += GeoLocationOffsetX;
						DVertex[1] += GeoLocationOffsetY;

						// Convert back to float (FVector)
						Vertex.X = DVertex[0];
						Vertex.Y = DVertex[1];
						Vertex.Z = DVertex[2];
						
						Vertex = Rotate.RotateVector(Vertex);
						Vertex *= Scale;					
						Vertex += Translate;

						result->ImportedMeshSectionInfos[i].Vertices.Push(Vertex);

						if (Mesh->HasNormals())
						{
							FVector Normal = FVector(
								Mesh->mNormals[j].x,
								Mesh->mNormals[j].y,
								Mesh->mNormals[j].z);
												
							Normal = Rotate.RotateVector(Normal);

							result->ImportedMeshSectionInfos[i].Normals.Push(Normal);
						}

						if (Mesh->HasTextureCoords(0))
						{
							FVector2D uv = FVector2D(Mesh->mTextureCoords[0][j].x, -Mesh->mTextureCoords[0][j].y);
							result->ImportedMeshSectionInfos[i].UV0.Push(uv);
						}

						if (Mesh->HasTangentsAndBitangents())
						{
							FVector TangentVector = FVector(
								Mesh->mTangents[j].x,
								Mesh->mTangents[j].y,
								Mesh->mTangents[j].z);

							TangentVector = GetAxisOrderedVector(TangentVector, ItemAxisOrder);
							FProcMeshTangent meshTangent = FProcMeshTangent(TangentVector, false);

							result->ImportedMeshSectionInfos[i].Tangents.Push(meshTangent);
						}

						if (Mesh->HasVertexColors(0))
						{
							FLinearColor color = FLinearColor(
								Mesh->mColors[0][j].r,
								Mesh->mColors[0][j].g,
								Mesh->mColors[0][j].b,
								Mesh->mColors[0][j].a
							);

							result->ImportedMeshSectionInfos[i].VertexColors.Push(color.ToFColor(true));
						}
					}

					const int32 NumFaces = Mesh->mNumFaces;

					result->ImportedMeshSectionInfos[i].Triangles.Reserve(NumFaces * 3);

					for (uint32 FaceIndex = 0; FaceIndex < NumFaces; ++FaceIndex)
					{
						for (uint32 Index = 0; Index < 3; ++Index)
	//					for (uint32 Index = 0; Index < mScenePtr->mMeshes[i]->mFaces[FaceIndex].mNumIndices; ++Index)
						{
							result->ImportedMeshSectionInfos[i].Triangles.Push(Mesh->mFaces[FaceIndex].mIndices[Index]);
						}
					}
				}

				for (uint32 i = 0; i < mScenePtr->mNumMaterials; ++i)
				{
					FImportMaterial ImportMaterial;

					aiMaterial* assimpMaterial = mScenePtr->mMaterials[i];

					aiColor3D color(1.0f, 1.0f, 1.0f);
					assimpMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, color);
					ImportMaterial.DiffuseColor = FLinearColor::FromSRGBColor(FColor(color.r * 255, color.g * 255, color.b * 255, 255));

					aiString texName;
					assimpMaterial->Get(AI_MATKEY_TEXTURE_DIFFUSE(0), texName);

					if (texName.length > 0)
					{
						FString TexturePath = FString(texName.C_Str());
						FString CleanTextureFilename = FPaths::GetCleanFilename(TexturePath);

						if (ProjectItem && ProjectItem->IsValidLowLevel())
						{
							FString RelativeTexturePath = FindFile(ProjectItem->DirectoryPath, CleanTextureFilename);

							if (RelativeTexturePath.Len() > 0)
							{
								ImportMaterial.DiffuseTexture = RelativeTexturePath;
							}
						}
					}

					ai_real ShininessStrength = 0.0f;
					assimpMaterial->Get(AI_MATKEY_SHININESS_STRENGTH, ShininessStrength);
					ImportMaterial.Metallic = ShininessStrength;

					ai_real Shininess = 0.0f;
					assimpMaterial->Get(AI_MATKEY_SHININESS, Shininess);
					//ImportMaterial.Roughness = FMath::Clamp(1.0f - Shininess, 0.0f, 1.0f);
					ImportMaterial.Roughness = FMath::Clamp(1.0f - 0.5f * FMath::Sqrt(2 / (Shininess + 2)), 0.0f, 1.0f);

					ai_real MatOpacity = 1.0f;
					assimpMaterial->Get(AI_MATKEY_OPACITY, MatOpacity);
					ImportMaterial.Opacity = MatOpacity;
					
					result->Materials.Push(ImportMaterial);
				}
			
				Importer->FreeScene();
				Importer = nullptr;
			}
		}

		if (ProjectItem && ProjectItem->IsValidLowLevel())
		{
			FString FullPath = FPaths::Combine(ProjectItem->DirectoryPath, FString("DT_Mesh.bin"));
			result->SaveToDisk(FullPath);
		}
	}

	MeshActor->IsLoaded = true;
	MeshActor = nullptr;

	//AsyncTask(ENamedThreads::GameThread, [this]()
	//{
	//	MeshActor->BuildMesh();
	//});
}

void FAsyncLoadMesh::DoWork()
{
	FScopeLock ScopeLock(&Mutex);

	MeshActor->IsLoading = true;

	FString FullPath = FPaths::Combine(MeshActor->ProjectItem->DirectoryPath, FString("DT_Mesh.bin"));
	MeshActor->ImportedMeshData->LoadFromDisk(FullPath);

	MeshActor->IsLoaded = true;
	MeshActor = nullptr;
}

void FImportedMeshData::SaveLoadData(FArchive & Ar)
{	
	Ar << Header;
	Ar << ImportedMeshSectionInfos;
	Ar << Materials;
	Ar << ImportedBuildingNodes;
}

void FImportedMeshData::SaveLoadHeader(FArchive & Ar)
{	
	Ar << Header;
}

bool FImportedMeshData::SaveToDisk(const FString & FullFilePath)
{
	FBufferArchive ToBinary;

	SaveLoadData(ToBinary);

	//No Data
	if(ToBinary.Num() <= 0) return false;

	//Step 2: Binary to Hard Disk
	if (FFileHelper::SaveArrayToFile(ToBinary, *FullFilePath)) 
	{
		// Free Binary Array 	
		ToBinary.FlushCache();
		ToBinary.Empty();
		
		return true;
	}
	
	// Free Binary Array 	
	ToBinary.FlushCache();
	ToBinary.Empty();

	return false;
}

bool FImportedMeshData::LoadFromDisk(const FString & FullFilePath, bool OnlyHeader)
{
	TArray<uint8> TheBinaryArray;
	if (!FFileHelper::LoadFileToArray(TheBinaryArray, *FullFilePath))
	{
		return false;
	}

	//File Load Error
	if(TheBinaryArray.Num() <= 0) return false;
	
	// Read data
	FMemoryReader FromBinary = FMemoryReader(TheBinaryArray, true); //true, free data after done
	FromBinary.Seek(0);

	if (OnlyHeader)
	{
		SaveLoadHeader(FromBinary);
	}
	else
	{
		SaveLoadData(FromBinary);
	}

	FromBinary.FlushCache();
	TheBinaryArray.Empty();
	FromBinary.Close();
	
	return true;
}

void FImportedMeshData::Clear()
{
	ImportedMeshSectionInfos.Empty();
	Materials.Empty();
	ImportedBuildingNodes.Empty();
}
