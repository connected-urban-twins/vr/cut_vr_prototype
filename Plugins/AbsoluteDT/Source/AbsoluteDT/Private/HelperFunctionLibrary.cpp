
#include "HelperFunctionLibrary.h"

 int32 UHelperFunctionLibrary::GetTexture2DDynamicSizeX(UTexture2DDynamic* InTexture2DDynamic)
 {
     if (!InTexture2DDynamic)
     {
         return -1;
     }
 
     return InTexture2DDynamic->SizeX;
 }

 int32 UHelperFunctionLibrary::GetTexture2DDynamicSizeY(UTexture2DDynamic* InTexture2DDynamic)
 {
     if (!InTexture2DDynamic)
     {
         return -1;
     }
 
     return InTexture2DDynamic->SizeY;
 }