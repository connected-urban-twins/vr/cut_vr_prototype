/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "DownloadTask.h"

#include "Paths.h"
#include "FileHelper.h"
#include "PlatformFilemanager.h"
#include <TaskGraphInterfaces.h>

#include "ZipFileFunctionLibrary.h"
#include "GenericPlatformHttp.h"

FString DownloadTask::TEMP_FILE_EXTERN = TEXT(".dlFile");
FString DownloadTask::TASK_JSON = TEXT(".task");




IPlatformFile* PlatformFile = nullptr;

DownloadTask::DownloadTask()
{
	if (PlatformFile == nullptr)
	{
		PlatformFile = &FPlatformFileManager::Get().GetPlatformFile();
		if (PlatformFile->GetLowerLevel())
		{
			PlatformFile = PlatformFile->GetLowerLevel();
		}
	}

	PlatformFile->SetSandboxEnabled(false);
}

DownloadTask::DownloadTask(EDownloadType downloadType, UUserProjectViewModel* project, UUserProjectItemViewModel* projectItem, const FString& userToken, const FString& InUrl, const FString& InDirectory, const FString& InFileName)
			: DownloadTask()
{
	FString Dir = InDirectory;
	if (!IFileManager::Get().DirectoryExists(*Dir))
	{
		if (!IFileManager::Get().MakeDirectory(*Dir))
		{
			UE_LOG(LogTemp, Warning, TEXT("Cannot create directory : %s"), *Dir);
		}
	}
	SetDirectory(Dir);
	

	SetSourceUrl(InUrl);
	SetFileName(InFileName);
	TaskInfo.DownloadType = downloadType;
	TaskInfo.GUID = FGuid::NewGuid();	
	TaskInfo.Project = project;
	TaskInfo.ProjectItem = projectItem;
	UserToken = userToken;

	TmpFullName = GetFullFileName() + TEMP_FILE_EXTERN;
}

DownloadTask::~DownloadTask()
{
//	Stop();

	bShouldStop = true;

	if (Request.IsValid())
	{
		if (Request->OnProcessRequestComplete().IsBound())
		{
			Request->OnProcessRequestComplete().Unbind();
		}

		Request->CancelRequest();
}

	//if (HeaderRequestPtr.Get() != nullptr && HeaderRequestPtr->GetStatus() != EHttpRequestStatus::Succeeded)
	//{
	//	HeaderRequestPtr->OnProcessRequestComplete().Unbind();
	//	HeaderRequestPtr->CancelRequest();
	//}

	//if (DownloadRequestPtr.Get() != nullptr && DownloadRequestPtr->GetStatus() != EHttpRequestStatus::Succeeded)
	//{
	//	DownloadRequestPtr->OnProcessRequestComplete().Unbind();
	//	DownloadRequestPtr->CancelRequest();
	//}
	
	CleanUp();
}

void DownloadTask::CleanUp()
{
	if (TargetFileHandle != nullptr)
	{
		delete TargetFileHandle;
		TargetFileHandle = nullptr;
	}

	//TmpFullName = GetFullFileName() + TEMP_FILE_EXTERN;

	////Delete the temp file, appending doesn't work atm
	//if (IFileManager::Get().FileExists(*TmpFullName))
	//{
	//	IFileManager::Get().Delete(*TmpFullName, false, true, true);
	//}
}

FString DownloadTask::SetFileName(const FString& InFileName)
{
	TaskInfo.FileName = FPaths::GetCleanFilename(InFileName);
	return TaskInfo.FileName;
}

FString DownloadTask::GetFileName() const
{
	return TaskInfo.FileName;
}

FString DownloadTask::GetSourceUrl() const
{
	return TaskInfo.SourceUrl;
}

FString DownloadTask::SetSourceUrl(const FString& InUrl)
{
	TaskInfo.SourceUrl = InUrl;
	return GetSourceUrl();
}

FString DownloadTask::SetDirectory(const FString& InDirectory)
{
	Directory = InDirectory;
	return Directory;
}

FString DownloadTask::GetDirectory() const
{
	return Directory;
}

int32 DownloadTask::SetTotalSize(int32 InTotalSize)
{
	TaskInfo.TotalSize = InTotalSize;
	return TaskInfo.TotalSize;
}

int32 DownloadTask::GetTotalSize() const
{
	return TaskInfo.TotalSize;
}

int32 DownloadTask::SetCurrentSize(int32 InCurrentSize)
{
	TaskInfo.CurrentSize = InCurrentSize;
	return TaskInfo.CurrentSize;
}

int32 DownloadTask::GetCurrentSize() const
{
	return TaskInfo.CurrentSize;
}

int32 DownloadTask::GetPercentage() const
{
	int32 Total = TaskInfo.TotalSize;

	if (Total < 1)
	{
		return 0;
	}
	else
	{
		const float progress = static_cast<float>(GetCurrentSize()) / static_cast<float>(GetTotalSize());
		return (int)(progress * 100);
	}	
}

float DownloadTask::GetProgress()
{
	int32 Total = GetTotalSize();

	switch (TaskState)
	{
	case ETaskState::NONE:
	case ETaskState::READY:
		return 0.0f;
		break;

	case ETaskState::DOWNLOADING:
		// Multiply progress by 0.95 to leave a bit progress fopr unzipping after the download finished
		return static_cast<float>(GetCurrentSize()) / static_cast<float>(GetTotalSize()) * 0.95;
		break;

	default:
		return 1.0f;
		break;
	}
}

FString DownloadTask::SetETag(const FString& ETag)
{
	TaskInfo.ETag = ETag;
	return TaskInfo.ETag;
}

FString DownloadTask::GetETag() const
{
	return TaskInfo.ETag;
}

bool DownloadTask::IsTargetFileExist() const
{
	FString FullFileName = FPaths::Combine(GetDirectory(), GetFileName());
	return IFileManager::Get().FileExists(*FullFileName);

	//bool result = FPaths::FileExists(FullFileName);
	//return FPaths::FileExists(FullFileName);
}

bool DownloadTask::Start()
{
	//Set target file name
	if (GetFileName().IsEmpty())
	{
		SetFileName(FPaths::GetCleanFilename(GetSourceUrl()));
	}

	bShouldStop = false;

	//error occurs!!! URL and file cannot be empty
	if (TaskInfo.SourceUrl.IsEmpty() || TaskInfo.FileName.IsEmpty())
	{
		TaskState = ETaskState::ERROR;
		ProcessTaskEvent(ETaskEvent::ERROR_OCCURED, TaskInfo, FString("Keine g�ltige URL f�r diese Datei"));
		return false;
	}

	//if target file already exist, make this task complete. 
	if (IsTargetFileExist())
	{
		OnTaskCompleted();
		return true;
	}
	
	//ignore if already being downloading.
	if (IsDownloading())
	{
		return true;
	}


	


	/*every time we start download(include resume from pause), we should check task information,
	for the remote resource may be changed during pausing*/
	GetHead();

	return true;
}

bool DownloadTask::Stop()
{
	bShouldStop = true;

	//release file handle when task stopped.
	CleanUp();

	TaskState = ETaskState::STOPPED;
	ProcessTaskEvent(ETaskEvent::STOP, TaskInfo, FString());

	return true;
}

bool DownloadTask::ClearDirectory(const FString& InDirectory)
{
	if (PlatformFile->DirectoryExists(*InDirectory))
	{
		if (!PlatformFile->DeleteDirectoryRecursively(*InDirectory))
		{
			UE_LOG(LogTemp, Warning, TEXT("Cannot remove directory : %s"), *InDirectory);
		}
	}

	if (!PlatformFile->DirectoryExists(*InDirectory))
	{
		IFileManager::Get().SetSandboxEnabled(false);
		if (!PlatformFile->DirectoryExists(*InDirectory))
		{
			PlatformFile->CreateDirectoryTree(*InDirectory);
		}
	}

	return true;
}

FGuid DownloadTask::GetGuid() const
{
	return TaskInfo.GetGuid();
}

bool DownloadTask::IsDownloading() const
{
	return TaskState >= ETaskState::READY && TaskState < ETaskState::COMPLETED && TaskState != ETaskState::STOPPED;
}

FTaskInformation DownloadTask::GetTaskInformation() const
{
	return TaskInfo;
}

ETaskState DownloadTask::GetState() const
{
	return TaskState;
}

bool DownloadTask::SaveTaskToJsonFile(const FString& InFileName) const
{
	FString TmpName = InFileName;
	if (TmpName.IsEmpty())
	{
		TmpName = GetFullFileName() + TASK_JSON;
	}
	
	FString OutStr;
	TaskInfo.SerializeToJsonString(OutStr);
	return FFileHelper::SaveStringToFile(OutStr, *TmpName);
}

bool DownloadTask::ReadTaskFromJsonFile(const FString& InFileName)
{
	FString FileData;
	if (!FFileHelper::LoadFileToString(FileData, *InFileName))
	{
		return false;
	}

	return TaskInfo.DeserializeFromJsonString(FileData);
}

void DownloadTask::DeleteTask()
{
	bShouldStop = true;
	bDeleteNow = true;
	if (!IsWritingToFile)
	{
		delete this;
	}
}

void DownloadTask::GetHead()
{
	if (bShouldStop) return;

	if (Request.IsValid() == false)
	{
		Request = FHttpModule::Get().CreateRequest();
	}

	EncodedUrl = GetSourceUrl();

	//https://www.google.com/
	static int32 URLTag = 8;
	int32 StartSlash = GetSourceUrl().Find(FString("/"), ESearchCase::IgnoreCase, ESearchDir::FromStart, URLTag);
	if (StartSlash > INDEX_NONE)
	{
		FString UrlLeft = GetSourceUrl().Left(StartSlash);
		FString UrlRight = GetSourceUrl().Mid(StartSlash);
		TArray<FString> UrlDirectory;
		UrlRight.ParseIntoArray(UrlDirectory, *FString("/"));

		EncodedUrl = UrlLeft;

		for (int32 i = 0; i < UrlDirectory.Num(); ++i)
		{
			UrlDirectory[i] = FGenericPlatformHttp::UrlEncode(UrlDirectory[i]);
			EncodedUrl += FString("/");
			EncodedUrl += UrlDirectory[i];
		}
	}
	   	 
	Request->SetVerb("HEAD");
	//FString SourceUrl = FGenericPlatformHttp::UrlEncode(GetSourceUrl());
	FString SourceUrl = GetSourceUrl();
	//SourceUrl = SourceUrl.Replace(*FString(" "), *FString("%20"), ESearchCase::IgnoreCase);
	Request->SetURL(EncodedUrl);
	FString AuthCode = FString("Bearer ") + UserToken;
	Request->SetHeader(FString("Authorization"), AuthCode);
	//Request->SetHeader(TEXT("Content-Type"), TEXT("application/x-www-form-urlencoded"));
	Request->SetHeader(FString("User-Agent"), FString("X-UnrealEngine-Agent"));
	Request->OnProcessRequestComplete().BindRaw(this, &DownloadTask::OnGetHeadCompleted);
	Request->ProcessRequest();

	TaskState = ETaskState::READY;
	ProcessTaskEvent(ETaskEvent::GET_HEAD, TaskInfo, FString());
}

void DownloadTask::OnGetHeadCompleted(FHttpRequestPtr InRequest, FHttpResponsePtr InResponse, bool bWasSuccessful)
{
	//InRequest->OnProcessRequestComplete().Unbind();

	if (bShouldStop) return;
	
	//we should check return code first to ensure the URL & network is OK.
	if (!InResponse.IsValid() || !bWasSuccessful)
	{
		UE_LOG(LogTemp, Warning, TEXT("OnGetHeadCompleted Response error"));

		TaskState = ETaskState::ERROR;
		ProcessTaskEvent(ETaskEvent::ERROR_OCCURED, TaskInfo, FString("Das Anfordern des Dateiheaders war nicht efolgreich."));

		return;
	}

	int32 ReturnCode = InResponse->GetResponseCode();

	TArray<FString> Headers = InRequest.Get()->GetAllHeaders();
	FString URL = InRequest.Get()->GetURL();
	FString ResponseContent = InResponse->GetContentAsString();


	if (ReturnCode >= 400 || ReturnCode < 200)
	{
		UE_LOG(LogTemp, Warning, TEXT("Http return code error : %d"), ReturnCode);

		CleanUp();


		TaskState = ETaskState::ERROR;
		ProcessTaskEvent(ETaskEvent::ERROR_OCCURED, TaskInfo, FString("HTTP Return Code " + FString::FromInt(ReturnCode)));
		return;
	}

	//the remote file has updated,we need to re-download
	if (InResponse->GetHeader("ETag") != TaskInfo.ETag)
	{
		SetETag(InResponse->GetHeader(FString("ETag")));
		SetTotalSize(InResponse->GetContentLength());
		SetCurrentSize(0);
	}

	CleanUp();

	//if (TaskInfo.DownloadType == EDownloadType::ProjectItem)
	//{		
	//	// Remove the project item directory and recreate it
	//	ClearDirectory(Directory);
	//}

	FString Filename = GetFullFileName();
	if (PlatformFile->FileExists(*Filename))
	{		
		TaskState = ETaskState::NO_NEED;
		ProcessTaskEvent(ETaskEvent::DOWNLOAD_COMPLETED, TaskInfo, FString());
		return;
	}

	SetCurrentSize(0);

	TargetFileHandle = PlatformFile->OpenWrite(*TmpFullName, false);

	if (TargetFileHandle == nullptr)
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s, %d, create temp file error !"));
		//TaskState = ETaskState::ERROR;
		//ProcessTaskEvent(ETaskEvent::ERROR_OCCURED, TaskInfo, FString("Die Download-Datei konnte nicht erstellt werden."));

		// There seem to be files referenced by 2 (or more?) Tasks; in this case, 1 task locks the temp file, all others have to stay away
		TaskState = ETaskState::NO_NEED;
		ProcessTaskEvent(ETaskEvent::DOWNLOAD_COMPLETED, TaskInfo, FString());
		bShouldStop = true;
		return;
	}

	if (!bShouldStop)
	{
		TaskState = ETaskState::DOWNLOADING;
		StartChunk();
	}
}

void DownloadTask::StartChunk()
{
	if (bShouldStop) return;

	Request->SetVerb("GET");
	Request->SetURL(EncodedUrl);

	int32 StartPostion = GetCurrentSize();
	int32 EndPosition = StartPostion + ChunkSize - 1;
	//lastPosition = TotalSize-1 
	if (EndPosition >= GetTotalSize())
	{
		EndPosition = GetTotalSize() - 1;
	}

	if (StartPostion >= EndPosition)
	{
		UE_LOG(LogTemp, Warning, TEXT("Error! StartPostion >= EndPosition"));
		return;
	}

	FString RangeStr = FString("bytes=") + FString::FromInt(StartPostion) + FString(TEXT("-")) + FString::FromInt(EndPosition);
	Request->SetHeader(FString("Range"), RangeStr);

	Request->OnProcessRequestComplete().BindRaw(this, &DownloadTask::OnGetChunkCompleted);
	Request->ProcessRequest();
}

FString DownloadTask::GetFullFileName() const
{
	return FPaths::Combine(GetDirectory(), GetFileName());
}

void DownloadTask::OnGetChunkCompleted(FHttpRequestPtr InRequest, FHttpResponsePtr InResponse, bool bWasSuccessful)
{
	if (bShouldStop)
	{
		return;
	}

	if (!InResponse.IsValid() || !bWasSuccessful)
	{
		UE_LOG(LogTemp, Warning, TEXT("OnGetHeadCompleted Response error"));
		TaskState = ETaskState::ERROR;
		ProcessTaskEvent(ETaskEvent::ERROR_OCCURED, TaskInfo, FString("Das Anfordern des Dateiheaders war nicht erfolgreich."));
		return;
	}
	int32 RetCode = InResponse->GetResponseCode();

	if (RetCode >= 400 || RetCode < 200)
	{
		UE_LOG(LogTemp, Warning, TEXT("%d, Return code error !"), InResponse->GetResponseCode());
		if (TargetFileHandle != nullptr)
		{
			delete TargetFileHandle;
			TargetFileHandle = nullptr;
		}
		TaskState = ETaskState::ERROR;
 		ProcessTaskEvent(ETaskEvent::ERROR_OCCURED, TaskInfo, FString("HTTP Fehler ") + FString::FromInt(InResponse->GetResponseCode()));
		return;
	}

	DataBuffer = InResponse->GetContent();

	IsWritingToFile = true;

	//Async write chunk buffer to file 
	//FFunctionGraphTask::CreateAndDispatchWhenReady([this]()
	//{
		if (this == nullptr)
		{
			return;
		}

		if (this->TargetFileHandle != nullptr && !bShouldStop)
		{
			this->TargetFileHandle->Seek(this->GetCurrentSize());
			if (this->TargetFileHandle->Write(DataBuffer.GetData(), DataBuffer.Num()))
			{
				this->OnWriteChunkEnd(this->DataBuffer.Num());				
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("%s, %d, Async write file error !"), __FUNCTION__, __LINE__);
				TaskState = ETaskState::ERROR;
				this->ProcessTaskEvent(ETaskEvent::ERROR_OCCURED, TaskInfo, FString("Die Datei konnte nicht geschrieben werden."));
			}
		}

	//}
	//, TStatId(), nullptr, ENamedThreads::AnyBackgroundThreadNormalTask);
}

void DownloadTask::OnTaskCompleted()
{
	//release file handle, so we can change file name via IFileManager.
	if (TargetFileHandle != nullptr)
	{
		delete TargetFileHandle;
		TargetFileHandle = nullptr;
	}

	//Change file name if target file does not exist.
	if (!IsTargetFileExist())
	{
		if (FPaths::GetExtension(this->GetFileName()).Compare("zip", ESearchCase::IgnoreCase) == 0)
		{
			UZipFileFunctionLibrary::UnzipWithLambda(this->TmpFullName,
				[this]()
			{
				//Called when done
				ChangeTempFilename();
			},
				[](float Percent)
			{
				//called when progress updates with % done
			});
		}
		else
		{
			ChangeTempFilename();
		}
	}
	else
	{
		SetTotalSize(IFileManager::Get().FileSize(*GetFullFileName()));
		if (GetCurrentSize() != GetTotalSize())
		{
			SetCurrentSize(GetTotalSize());
		}

		TaskState = ETaskState::COMPLETED;
		ProcessTaskEvent(ETaskEvent::DOWNLOAD_COMPLETED, TaskInfo, FString());
	}
}

bool DownloadTask::ChangeTempFilename()
{
	//change temp file name to target file name.

	IFileManager::Get().SetSandboxEnabled(false);
	if (IFileManager::Get().Move(*GetFullFileName(), *TmpFullName, true, true))
	//IFileManager::Get().Move(*GetFullFileName(), *FString(GetFullFileName() + TEMP_FILE_EXTERN), true, true);

	//PlatformFile->MoveFile(*GetFullFileName(), *FString(GetFullFileName() + TEMP_FILE_EXTERN));

//	if (IFileManager::Get().Move(*GetFullFileName(), *this->TmpFullName))
	{
		TaskState = ETaskState::COMPLETED;
		ProcessTaskEvent(ETaskEvent::DOWNLOAD_COMPLETED, TaskInfo, FString());
		return true;
	}
	else
	{
		//error when changing file name.
		UE_LOG(LogTemp, Warning, TEXT("%s, %d, Change temp file name error !"), __FUNCTION__, __LINE__);
		TaskState = ETaskState::ERROR;
		ProcessTaskEvent(ETaskEvent::ERROR_OCCURED, TaskInfo, FString("Die tempor�re Datei konnte nicht umbenannt werden."));
	}

	return false;
}

void DownloadTask::OnWriteChunkEnd(int32 WroteSize)
{
	if (GetState() != ETaskState::DOWNLOADING)
	{
		return;
	}

	IsWritingToFile = false;

	if (bDeleteNow)
	{
		delete this;
	}

	if (bShouldStop)
	{
		TaskState = ETaskState::STOPPED;
		ProcessTaskEvent(ETaskEvent::STOP, TaskInfo, FString());
		return;
	}

	//update progress
	SetCurrentSize(GetCurrentSize() + WroteSize);

	if (GetCurrentSize() < GetTotalSize())
	{
		ProcessTaskEvent(ETaskEvent::DOWNLOAD_UPDATE, TaskInfo, FString());
		//download next chunk
		StartChunk();
	}
	else
	{
		//task have completed.
		OnTaskCompleted();
	}

}
