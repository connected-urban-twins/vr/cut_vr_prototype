/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "FileDownloadManager.h"

#include <Paths.h>


FString UFileDownloadManager::DefaultDirectory = TEXT("FileDownload");

UFileDownloadManager::UFileDownloadManager()
{

}

void UFileDownloadManager::StartAll()
{
	for (int32 i = 0; i < TaskList.Num(); ++i)
	{
		TaskList[i]->Start();
	}
}


bool UFileDownloadManager::StartNext()
{
	int32 NextTaskIndex = FindTaskToDo();

	if (NextTaskIndex != -1)
	{
		TaskList[NextTaskIndex]->Start();
		return true;
	}
	
	return false;
}

void UFileDownloadManager::StartLast()
{
	TaskList.Last()->Start();
}

void UFileDownloadManager::StartTask(const FGuid& InGuid)
{
	int32 ret = FindTaskByGuid(InGuid);
	if (ret >= 0)
	{
		TaskList[ret]->Start();
	}
}

void UFileDownloadManager::StopAll()
{
	for (auto task : TaskList)
	{
		if (task != nullptr)
		{
			task->Stop();
		}
	}
}

void UFileDownloadManager::StopTask(const FGuid& InGuid)
{
	int32 ret = FindTaskByGuid(InGuid);
	if (ret >= 0)
	{
		TaskList[ret]->Stop();
	}
}

bool UFileDownloadManager::SaveTaskToJsonFile(const FGuid& InGuid, const FString& InFileName /*= TEXT("")*/)
{
	int32 ret = FindTaskByGuid(InGuid);
	if (ret < 0)
	{
		return false;
	}

	return TaskList[ret]->SaveTaskToJsonFile(InFileName);
}

bool UFileDownloadManager::ReadTaskFromJsonFile(const FGuid& InGuid, const FString& InFileName)
{
	int32 ret = FindTaskByGuid(InGuid);
	if (ret < 0)
	{
		return false;
	}

	return TaskList[ret]->ReadTaskFromJsonFile(InFileName);
}

TArray<FTaskInformation> UFileDownloadManager::GetAllTaskInformation() const
{
	TArray<FTaskInformation> Ret;
	for (int32 i = 0; i < TaskList.Num(); ++i)
	{
		Ret.Add(TaskList[i]->GetTaskInformation());
	}

	return Ret;
}


FGuid UFileDownloadManager::AddTask(DownloadTask* InTask)
{
	if (InTask == nullptr)
	{
		FGuid ret;
		ret.Invalidate();
		return ret;
	}

	for (int32 i = 0; i < TaskList.Num(); ++i)
	{
		if (TaskList[i]->GetGuid() == InTask->GetGuid())
		{
			return TaskList[i]->GetGuid();
		}
	}

	InTask->ProcessTaskEvent = [this](ETaskEvent InEvent, FTaskInformation InInfo, FString Message)
	{
		if (this != nullptr)
		{
			this->OnTaskEvent(InEvent, InInfo, Message);
		}
	};

	TaskList.Add(InTask);
	return InTask->GetGuid();
}

FGuid UFileDownloadManager::AddTaskByUrl(EDownloadType downloadType, UUserProjectViewModel* project, UUserProjectItemViewModel* projectItem, const FString& userToken, const FString& InUrl, const FString& InDirectory, const FString&
                                         InFileName)
{
	FString TmpDir = InDirectory;
	if (TmpDir.IsEmpty())
	{
		TmpDir = FPaths::ProjectContentDir() + DefaultDirectory;
	}
	return AddTask(new DownloadTask(downloadType, project, projectItem, userToken, InUrl, TmpDir, InFileName));
}

void UFileDownloadManager::DeleteTask(FGuid task)
{
	int32 index = FindTaskByGuid(task);
	if (index >= 0)
	{
		TaskList[index]->CleanUp();
		TaskList.RemoveAt(index);
	}
}

bool UFileDownloadManager::GetAllTasksCompleted()
{
	for (int32 i = 0; i < TaskList.Num(); ++i)
	{
		if (TaskList[i]->GetState() <= ETaskState::STOPPED)
		{
			return false;
		}
	}

	return true;
}

void UFileDownloadManager::DeleteAllTasks()
{
	StopAll();

	int i = 0;
	while (i < TaskList.Num())
	{
		ETaskState CurrentState = TaskList[i]->GetState();
		if (CurrentState != ETaskState::READY && TaskList[i]->GetState() != ETaskState::DOWNLOADING)
		{
			//task->Stop();
			//TaskList[i]->DeleteTask();
			//delete TaskList[i];
			TaskList.RemoveAt(i);
		}
		else
		{
			i++;
		}
	}

//	TaskList.Empty();
}

void UFileDownloadManager::OnTaskEvent(ETaskEvent InEvent, FTaskInformation InInfo, FString Message)
{
	if (TaskList.Num() > 0)
	{
		OnDlManagerEvent.Broadcast(InEvent, InInfo, Message);
	}

	//if (InEvent == ETaskEvent::DOWNLOAD_COMPLETED)
	//{
	//	//int32 index = FindTaskByGuid(InInfo.GUID);
	//	//TaskList.RemoveAt(index);

	//	int32 n = FindTaskToDo();
	//	if (n >= 0)
	//	{
	//		TaskList[n]->Start();
	//	}
	//}

	return ;
}

FString UFileDownloadManager::GetDownloadDirectory() const
{
	return DefaultDirectory;
}

int32 UFileDownloadManager::FindNextOpenToDo() const
{
	int32 ret = -1;
	for (int32 i = 0; i < TaskList.Num(); ++i)
	{
		if (TaskList[i]->GetState() < ETaskState::READY)
		{
			ret = i;

		}
	}

	return ret;
}

int32 UFileDownloadManager::FindTaskToDo() const
{
	int32 ret = -1;
	for (int32 i = 0; i < TaskList.Num(); ++i)
	{
		if (TaskList[i]->GetState() < ETaskState::READY)
		{
			ret = i;
			
		}
	}

	return ret;
}

int32 UFileDownloadManager::FindTaskByGuid(const FGuid& InGuid) const
{
	int32 ret = -1;
	for (int32 i = 0; i < TaskList.Num(); ++i)
	{
		if (TaskList[i]->GetGuid() == InGuid)
		{
			ret = i;
		}
	}

	return ret;
}

