/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "DigitalTwinGameInstance.h" 	
#include "CoreMinimal.h"
#include "GenericPlatform/GenericPlatformMisc.h"

UDigitalTwinGameInstance::UDigitalTwinGameInstance(const FObjectInitializer& ObjectInitializer)
{
	ApplicationMode = EApplicationMode::NORMAL;
	
	if (FParse::Param(FCommandLine::Get(), TEXT("DemoMode")))
	{
		ApplicationMode = EApplicationMode::DEMO;
	}
	else if (FParse::Param(FCommandLine::Get(), TEXT("DroneMode")))
	{
		ApplicationMode = EApplicationMode::DRONE;
	}

	ThreadPool = FQueuedThreadPool::Allocate();
	const int32 NumQueuedThreads = FMath::Clamp(FGenericPlatformMisc::NumberOfWorkerThreadsToSpawn(), 1, 1);
	ThreadPool->Create(NumQueuedThreads, 256 * 1024, EThreadPriority::TPri_AboveNormal);
	
	DefaultMap = "Hamburg";
	PressThumbstickForMovement = true;
	UseLAN = false;

	FullAppFeatureSet = true;
	
	const FString ExePath = FPaths::GetPath(FString(FPlatformProcess::ExecutablePath()));
	const FString LoaderPath = FPaths::ConvertRelativePathToFull(FPaths::Combine(ExePath, FString(TEXT("../../../CUT.exe"))));

	if (FPaths::FileExists(LoaderPath))
	{
		FullAppFeatureSet = false;		
	}
}

void UDigitalTwinGameInstance::UDigitalTwinGameInstance::LoadSettings()
{
	if (!GConfig)
	{
		return;
	}

	GConfig->GetBool(
		TEXT("/Script/AbsoluteDT.DigitalTwinGameInstance"),
		TEXT("PressThumbstickForMovement"),
		PressThumbstickForMovement,
		GGameIni
	);
}

void UDigitalTwinGameInstance::UDigitalTwinGameInstance::SaveSettings()
{
	SaveConfig();
}
