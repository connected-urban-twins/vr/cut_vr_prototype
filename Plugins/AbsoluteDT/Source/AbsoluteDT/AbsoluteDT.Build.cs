/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/



using UnrealBuildTool;
using System.IO;

public class AbsoluteDT: ModuleRules
{
    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/")); }
    }

    public AbsoluteDT(ReadOnlyTargetRules Target) : base(Target)
    {
        PrivatePCHHeaderFile = "Private/AbsoluteDTPrivatePCH.h";
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        bUseRTTI = true;
        bEnableExceptions = true;
        PublicDefinitions.Add("BOOST_NO_NOEXCEPT");

        PublicIncludePaths.AddRange(
			new string[] {
                Path.Combine(ModuleDirectory, "Public"),
				// ... add public include paths required here ...
                Path.Combine(ThirdPartyPath, "assimp/include"),

                Path.Combine(ThirdPartyPath, "IfcPlusPlus/include"),
                Path.Combine(ThirdPartyPath, "IfcPlusPlus/include/ifcpp/geometry"),
                Path.Combine(ThirdPartyPath, "boost_1_68_0"),
                Path.Combine(ThirdPartyPath, "Carve/include")
            }
		);

        PrivateIncludePaths.AddRange(
			new string[] {
                "AbsoluteDT/Private"
				// ... add other private include paths required here ...
            }
            );

        PublicDependencyModuleNames.AddRange(
            new string[]
            {
                "Core",
                "CoreUObject",
                "Engine",
                "RHI",
                "RenderCore",
                "ProceduralMeshComponent",
                "Json",
                "JsonUtilities",
                "Http",
                "UMG",
                "ZipUtility",
                "VRExpansionPlugin",
                "ImageWrapper",
                // ... add other public dependencies that you statically link with here ...
			}
			);


        PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"Slate",
				"SlateCore",
				// ... add private dependencies that you statically link with here ...	
                "ImageWrapper",
            }
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
            }
            );

//        if ((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32))
        if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            string PlatformString = (Target.Platform == UnrealTargetPlatform.Win64) ? "x64" : "x86";
            PublicAdditionalLibraries.Add(Path.Combine(ThirdPartyPath, "assimp/lib",PlatformString, "assimp-vc141-mt.lib"));

            RuntimeDependencies.Add(Path.Combine(ThirdPartyPath, "assimp/bin", PlatformString, "assimp-vc141-mt.dll"));

            switch (Target.Configuration)
            {
                case UnrealTargetConfiguration.Debug:
                case UnrealTargetConfiguration.DebugGame:
                    PublicAdditionalLibraries.Add(Path.Combine(ThirdPartyPath, "IfcPlusPlus/lib", "IfcPlusPlusrd.lib"));
                    //RuntimeDependencies.Add(Path.Combine(ThirdPartyPath, "IfcPlusPlus/bin", "IfcPlusPlusd.dll"));
                    break;

                default:
                    //PublicAdditionalLibraries.Add(Path.Combine(ThirdPartyPath, "IfcPlusPlus/lib", "IfcPlusPlus.lib"));
                    //RuntimeDependencies.Add(Path.Combine(ThirdPartyPath, "IfcPlusPlus/bin", "IfcPlusPlus.dll"));
                    PublicAdditionalLibraries.Add(Path.Combine(ThirdPartyPath, "IfcPlusPlus/lib", "IfcPlusPlusrd.lib"));
                    break;
            }

            PublicAdditionalLibraries.Add(Path.Combine(ThirdPartyPath, "Carve/lib", "Carve.lib"));
        }


    }
}
