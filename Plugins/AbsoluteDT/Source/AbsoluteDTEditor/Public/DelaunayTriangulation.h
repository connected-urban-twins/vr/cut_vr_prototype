/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "Engine.h"
#include "RawMesh.h"
#include "AbsoluteDTEditorPrivatePCH.h"

#include <vector>
#include <algorithm>


#define EPSILON 0.000001

class Edge
{
public:
	Edge(): p1(), p2()	{}
	~Edge() {}
	Edge(const PointCloudEntry &p1, const PointCloudEntry &p2) : p1(p1), p2(p2)	{}
	Edge(const Edge &e) : p1(e.p1), p2(e.p2) {};

	PointCloudEntry p1;
	PointCloudEntry p2;
};

inline bool operator == (const Edge & e1, const Edge & e2)
{
	return 	(e1.p1 == e2.p1 && e1.p2 == e2.p2) || (e1.p1 == e2.p2 && e1.p2 == e2.p1);
}

class Triangle
{
public:

	Triangle(const PointCloudEntry &_p1, const PointCloudEntry &_p2, const PointCloudEntry &_p3)
		: p1(_p1), p2(_p2), p3(_p3),
		e1(_p1, _p2), e2(_p2, _p3), e3(_p3, _p1) {}

	bool containsVertex(const PointCloudEntry &v) const
	{
		return p1 == v || p2 == v || p3 == v;
	}

	inline bool CircumCircle(PointCloudEntry& point, double &centerX, double &centerY, double &radius) const
	{
		double m1, m2, mx1, mx2, my1, my2;

		/* Check for coincident points */
		if (abs(p1.Y - p2.Y) < EPSILON && abs(p2.Y - p3.Y) < EPSILON)
			return(false);

		if (abs(p2.Y - p1.Y) < EPSILON)
		{
			m2 = -(p3.X - p2.X) / (p3.Y - p2.Y);
			mx2 = (p2.X + p3.X) / 2.0;
			my2 = (p2.Y + p3.Y) / 2.0;
			centerX = (p2.X + p1.X) / 2.0;
			centerY = m2 * (centerX - mx2) + my2;
		}
		else if (abs(p3.Y - p2.Y) < EPSILON)
		{
			m1 = -(p2.X - p1.X) / (p2.Y - p1.Y);
			mx1 = (p1.X + p2.X) / 2.0;
			my1 = (p1.Y + p2.Y) / 2.0;
			centerX = (p3.X + p2.X) / 2.0;
			centerY = m1 * (centerX - mx1) + my1;
		}
		else
		{
			m1 = -(p2.X - p1.X) / (p2.Y - p1.Y);
			m2 = -(p3.X - p2.X) / (p3.Y - p2.Y);
			mx1 = (p1.X + p2.X) / 2.0;
			mx2 = (p2.X + p3.X) / 2.0;
			my1 = (p1.Y + p2.Y) / 2.0;
			my2 = (p2.Y + p3.Y) / 2.0;
			centerX = (m1 * mx1 - m2 * mx2 + my2 - my1) / (m1 - m2);
			centerY = m1 * (centerX - mx1) + my1;
		}

		double dx = p2.X - centerX;
		double dy = p2.Y - centerY;
		double radiusSquared = dx * dx + dy * dy;
		dx = point.X - centerX;
		dy = point.Y - centerY;
		double distanceSquared = dx * dx + dy * dy;

		return(distanceSquared <= radiusSquared) ? true : false;
	}

	PointCloudEntry p1;
	PointCloudEntry p2;
	PointCloudEntry p3;
	Edge e1;
	Edge e2;
	Edge e3;
};

inline bool operator == (const Triangle &t1, const Triangle &t2)
{
	return	(t1.p1 == t2.p1 || t1.p1 == t2.p2 || t1.p1 == t2.p3) &&
		(t1.p2 == t2.p1 || t1.p2 == t2.p2 || t1.p2 == t2.p3) &&
		(t1.p3 == t2.p1 || t1.p3 == t2.p2 || t1.p3 == t2.p3);
}

//template<typename T>
//inline typename std::vector<T>::iterator unorderedErase(std::vector<T>& p_container,
//	typename std::vector<T>::iterator p_it)
//{
//	if (p_it != p_container.end() - 1)
//	{
//		std::swap(*p_it, p_container.back());
//		p_container.pop_back();
//		return p_it;
//	}
//	// else
//	p_container.pop_back();
//	return p_container.end();
//}

class FDelaunayTriangulation
{
public:
	FDelaunayTriangulation() {}
	~FDelaunayTriangulation() {};

	void BuildRawMesh(FRawMesh* rawMesh);

	void BuildPointList(TArray<PointCloudEntry>& points)
	{
		uint32 NumPoints = points.Num();
		Vertices.resize(NumPoints);

		uint32 i = 0;

		for (auto point : points)
		{
			Vertices.at(i).X = point.X;
			Vertices.at(i).Y = point.Y;
			Vertices.at(i).Z = point.Z;
			Vertices.at(i).R = point.R;
			Vertices.at(i).G = point.G;
			Vertices.at(i).B = point.B;
			i++;
		}
	}

	const std::vector<Triangle>& triangulate(TArray<PointCloudEntry>& points)
	{
		BuildPointList(points);

		// Determine the super triangle
		float minX = Vertices[0].X;
		float minY = Vertices[0].Y;
		float maxX = minX;
		float maxY = minY;

		Triangles.reserve(static_cast<uint32>(Vertices.size()) * 6);

		for (size_t i = 0; i < Vertices.size(); ++i)
		{
			if (Vertices[i].X < minX) minX = Vertices[i].X;
			if (Vertices[i].Y < minY) minY = Vertices[i].Y;
			if (Vertices[i].X > maxX) maxX = Vertices[i].X;
			if (Vertices[i].Y > maxY) maxY = Vertices[i].Y;
		}

		float distanceMinMaxX = maxX - minX;
		float distanceMinMaxY = maxY - minY;
		float deltaMax = std::max(distanceMinMaxX, distanceMinMaxY);
		float midx = (minX + maxX) / 2.f;
		float midy = (minY + maxY) / 2.f;

		PointCloudEntry p1(midx - 20 * deltaMax, midy - deltaMax, 0.0, 0, 0, 0, 0);
		PointCloudEntry p2(midx, midy + 20 * deltaMax, 0.0, 0, 0, 0, 0);
		PointCloudEntry p3(midx + 20 * deltaMax, midy - deltaMax, 0.0, 0, 0, 0, 0);

		// Add the supertriangle
		Triangles.push_back(Triangle(p1, p2, p3));

		FString ProgressTitle = "Triangulating Point Cloud";
		FScopedSlowTask * ProgressTracker = new FScopedSlowTask(Vertices.size(), FText::FromString(ProgressTitle), true, *GWarn);
		ProgressTracker->MakeDialog();

		for (auto vertex = begin(Vertices); vertex != end(Vertices); vertex++)
		{
			ProgressTracker->EnterProgressFrame();

			std::vector<Triangle> badTriangles;
			badTriangles.reserve(static_cast<uint32>(Vertices.size()) * 6);
			std::vector<Edge> polygon;

			for (auto triangle = begin(Triangles); triangle != end(Triangles); triangle++)
			{
				double centerX, centerY, radius;

				if (triangle->CircumCircle(*vertex, centerX, centerY, radius))
				{
					badTriangles.push_back(*triangle);
					polygon.push_back(triangle->e1);
					polygon.push_back(triangle->e2);
					polygon.push_back(triangle->e3);
				}
			}

			Triangles.erase(std::remove_if(begin(Triangles), end(Triangles), [badTriangles](Triangle &triangle)
				{
					for (auto badTriangle = begin(badTriangles); badTriangle != end(badTriangles); badTriangle++)
					{
						if (*badTriangle == triangle)
						{
							return true;
						}
					}
					return false;
				}), end(Triangles));

			std::vector<Edge> badEdges;
			for (auto edge1 = begin(polygon); edge1 != end(polygon); edge1++)
			{
				for (auto edge2 = begin(polygon); edge2 != end(polygon); edge2++)
				{
					if (edge1 == edge2)
						continue;

					if (*edge1 == *edge2)
					{
						badEdges.push_back(*edge1);
						badEdges.push_back(*edge2);
					}
				}
			}

			polygon.erase(std::remove_if(begin(polygon), end(polygon), [badEdges](Edge &edge)
				{
					for (auto it = begin(badEdges); it != end(badEdges); it++)
					{
						if (*it == edge)
							return true;
					}
					return false;
				}), end(polygon));

			for (auto e = begin(polygon); e != end(polygon); e++)
			{
				Triangles.push_back(Triangle(e->p1, e->p2, *vertex));
			}
		}

		Triangles.erase(std::remove_if(begin(Triangles), end(Triangles), [p1, p2, p3](Triangle &triangle)
		{
			return triangle.containsVertex(p1) || triangle.containsVertex(p2) || triangle.containsVertex(p3);
		}), end(Triangles));

		ProgressTracker->Destroy();

		return Triangles;
	}

private:
	std::vector<Triangle> Triangles;
	std::vector<Edge> Edges;
	std::vector<PointCloudEntry> Vertices;
};

inline void FDelaunayTriangulation::BuildRawMesh(FRawMesh* rawMesh)
{
	rawMesh->VertexPositions.Empty();
	rawMesh->WedgeIndices.Empty();
	rawMesh->WedgeTexCoords[0].Empty();
	rawMesh->FaceMaterialIndices.Empty();
	rawMesh->FaceSmoothingMasks.Empty();

	uint32 NumVertices = Triangles.size() * 3;
	rawMesh->VertexPositions.Reserve(NumVertices);

	float EdgeLengthConstraint = 50000.0f * GLOBALSCALEFACTOR;

	for (auto t = begin(Triangles); t != end(Triangles); t++)
	{
		if (t->p1.Distance2DSquared(t->p2) < EdgeLengthConstraint &&
			t->p2.Distance2DSquared(t->p3) < EdgeLengthConstraint &&
			t->p1.Distance2DSquared(t->p3) < EdgeLengthConstraint)
		{
			FVector Vector1 = FVector(t->p1.X, t->p1.Y, t->p1.Z) * 100.0;
			FVector Vector2 = FVector(t->p2.X, t->p2.Y, t->p2.Z) * 100.0;
			FVector Vector3 = FVector(t->p3.X, t->p3.Y, t->p3.Z) * 100.0;

			uint32 index1 = rawMesh->VertexPositions.AddUnique(Vector1);
			uint32 index2 = rawMesh->VertexPositions.AddUnique(Vector2);
			uint32 index3 = rawMesh->VertexPositions.AddUnique(Vector3);

			rawMesh->WedgeIndices.Add(index1);
			rawMesh->WedgeIndices.Add(index2);
			rawMesh->WedgeIndices.Add(index3);

			rawMesh->WedgeColors.Add(FColor(t->p1.R, t->p1.G, t->p1.B));
			rawMesh->WedgeColors.Add(FColor(t->p2.R, t->p2.G, t->p2.B));
			rawMesh->WedgeColors.Add(FColor(t->p3.R, t->p3.G, t->p3.B));

			rawMesh->WedgeTexCoords[0].Add(0.01f * FVector2D(Vector1.X, Vector1.Y));
			rawMesh->WedgeTexCoords[0].Add(0.01f * FVector2D(Vector2.X, Vector2.Y));
			rawMesh->WedgeTexCoords[0].Add(0.01f * FVector2D(Vector3.X, Vector3.Y));

			rawMesh->FaceMaterialIndices.Add(0);
			rawMesh->FaceSmoothingMasks.Add(1);
		}
	}
}
