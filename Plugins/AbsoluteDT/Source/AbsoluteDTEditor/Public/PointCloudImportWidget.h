/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once
#include "SCompoundWidget.h"
#include "STextComboBox.h"

typedef TSharedPtr<FString> FComboItemType;

class SPointCloudImportWidget : public SCompoundWidget
{

public:
	SLATE_BEGIN_ARGS(SPointCloudImportWidget) {}
	SLATE_END_ARGS()

	void Construct(const FArguments& args);

	FReply ImportScanDataClicked();
	FReply ImportTerrainDataClicked();
	void OnCoordSystemSelectionChanged(FComboItemType NewValue, ESelectInfo::Type);
	void OnXYUnitSelectionChanged(FComboItemType NewValue, ESelectInfo::Type);
	void OnZUnitSelectionChanged(FComboItemType NewValue, ESelectInfo::Type);
	void OnInvertXAxisChanged(ECheckBoxState newState);
	void OnInvertYAxisChanged(ECheckBoxState newState);
	void OnInvertZAxisChanged(ECheckBoxState newState);
	void OnMaxNumPointsChanged(const FText& InText, ETextCommit::Type InCommitType);
	void GetDataFromUI();

	TSharedRef<SWidget> GenerateComboItem(TSharedPtr<FString> InItem);
	FText GetSelectedCoordinateSystemLabel() const;
	FText GetSelectedXYUnitItemLabel() const;
	FText GetSelectedZUnitItemLabel() const;
	FText GetMaxNumPoints() const;
	ECheckBoxState GetInvertXAxisCheck() const;
	ECheckBoxState GetInvertYAxisCheck() const;
	ECheckBoxState GetInvertZAxisCheck() const;

	TArray<FComboItemType> CoordinateSystemOptions;
	TArray<FComboItemType> XYUnitOptions;
	TArray<FComboItemType> ZUnitOptions;

	EPointCloudCoordinateSystem SelectedCoordinateSystem = XmZY;
	EPointCloudUnit SelectedXYUnit = inch;
	EPointCloudUnit SelectedZUnit = inch;

	FComboItemType SelectedCoordinateSystemItem;
	FComboItemType SelectedXYUnitItem;
	FComboItemType SelectedZUnitItem;
	bool InvertXAxisCheck = false;
	bool InvertYAxisCheck = false;
	bool InvertZAxisCheck = false;
	ImportCoordinateData CoordinateData;
	FString MaxNumPointsString;

private:
	FString CoordSystemTextXmZY = "XmZY";
	FString CoordSystemTextXYZ = "XYZ";
	FString CoordSystemXTextUnity = "Unity";

	FString UnitTextInch = "Inch";
	FString UnitTextMeter = "Meter";
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SPointCloudImportWidget::Construct(const FArguments& args)
{
	CoordinateSystemOptions.Empty();
	CoordinateSystemOptions.Add(MakeShareable(new FString(CoordSystemTextXYZ)));
	CoordinateSystemOptions.Add(MakeShareable(new FString(CoordSystemTextXmZY)));
	CoordinateSystemOptions.Add(MakeShareable(new FString(CoordSystemXTextUnity)));

	XYUnitOptions.Empty();
	XYUnitOptions.Add(MakeShareable(new FString(UnitTextInch)));
	XYUnitOptions.Add(MakeShareable(new FString(UnitTextMeter)));

	ZUnitOptions.Empty();
	ZUnitOptions.Add(MakeShareable(new FString(UnitTextInch)));
	ZUnitOptions.Add(MakeShareable(new FString(UnitTextMeter)));


	SelectedCoordinateSystemItem = CoordinateSystemOptions[0];
	SelectedXYUnitItem = XYUnitOptions[0];
	SelectedZUnitItem = ZUnitOptions[0];
	MaxNumPointsString = "20000";

	ChildSlot
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Top)
			[
				SNew(STextBlock)
				.ColorAndOpacity(FLinearColor::White)
				.ShadowColorAndOpacity(FLinearColor::Black)
				.ShadowOffset(FIntPoint(-1, 1))
				.Text(FText::FromString("Point CLoud Import"))
			]
			+ SOverlay::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(SBorder)
				.BorderBackgroundColor(FLinearColor(20,20,20))
				.ForegroundColor(FLinearColor::Gray)
				.ColorAndOpacity(FLinearColor::White)
				[
				SNew(SGridPanel)
				//.FillColumn(1, 0.5f)
				//.FillColumn(2, 0.5f)

				+ SGridPanel::Slot(0, 0)
					.Padding(20, 5, 20, 5)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text(FText::FromString("Coordinate System"))
					]
				+ SGridPanel::Slot(1, 0)
					.Padding(20, 5, 20, 5)
					.HAlign(HAlign_Fill)
					.VAlign(VAlign_Center)
					[
						SNew(SComboBox<FComboItemType>)
						.OptionsSource(&CoordinateSystemOptions)
						.OnGenerateWidget(this, &SPointCloudImportWidget::GenerateComboItem)
						.InitiallySelectedItem(SelectedCoordinateSystemItem)
						.OnSelectionChanged(this, &SPointCloudImportWidget::OnCoordSystemSelectionChanged)
						[
							SNew(STextBlock)
							.Text(this, &SPointCloudImportWidget::GetSelectedCoordinateSystemLabel)
						]
					]
				+ SGridPanel::Slot(0, 1)
					.Padding(20, 5, 20, 5)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text(FText::FromString("XY Unit"))
					]
				+ SGridPanel::Slot(1, 1)
					.Padding(20, 5, 20, 5)
					.HAlign(HAlign_Fill)
					.VAlign(VAlign_Center)
					[
						SNew(SComboBox<FComboItemType>)
						.OptionsSource(&XYUnitOptions)
						.OnGenerateWidget(this, &SPointCloudImportWidget::GenerateComboItem)
						.InitiallySelectedItem(SelectedXYUnitItem)
						.OnSelectionChanged(this, &SPointCloudImportWidget::OnXYUnitSelectionChanged)
						[
							SNew(STextBlock)
							.Text(this, &SPointCloudImportWidget::GetSelectedXYUnitItemLabel)
						]
					]
				+ SGridPanel::Slot(0, 2)
					.Padding(20, 5, 20, 5)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text(FText::FromString("Z Unit"))
					]
				+ SGridPanel::Slot(1, 2)
					.Padding(20, 5, 20, 5)
					.HAlign(HAlign_Fill)
					.VAlign(VAlign_Center)
					[
						SNew(SComboBox<FComboItemType>)
						.OptionsSource(&ZUnitOptions)
						.OnGenerateWidget(this, &SPointCloudImportWidget::GenerateComboItem)
						.InitiallySelectedItem(SelectedZUnitItem)
						.OnSelectionChanged(this, &SPointCloudImportWidget::OnZUnitSelectionChanged)
						[
							SNew(STextBlock)
							.Text(this, &SPointCloudImportWidget::GetSelectedZUnitItemLabel)
						]
					]
				+ SGridPanel::Slot(0, 3)
					.Padding(20, 5, 20, 5)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text(FText::FromString("Invert X-Axis"))
					]
				+ SGridPanel::Slot(1, 3)
					.Padding(20, 5, 20, 5)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					[
							SNew(SCheckBox)
							.IsChecked(this, &SPointCloudImportWidget::GetInvertXAxisCheck)
							.OnCheckStateChanged(this, &SPointCloudImportWidget::OnInvertXAxisChanged)
					]
				+ SGridPanel::Slot(0, 4)
					.Padding(20, 5, 20, 5)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text(FText::FromString("Invert Y-Axis"))
					]
				+ SGridPanel::Slot(1, 4)
					.Padding(20, 5, 20, 5)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					[
						SNew(SCheckBox)
						.IsChecked(this, &SPointCloudImportWidget::GetInvertYAxisCheck)
					.OnCheckStateChanged(this, &SPointCloudImportWidget::OnInvertYAxisChanged)
					]
				+ SGridPanel::Slot(0, 5)
					.Padding(20, 5, 20, 5)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text(FText::FromString("Invert Z-Axis"))
					]
				+ SGridPanel::Slot(1, 5)
					.Padding(20, 5, 20, 5)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					[
						SNew(SCheckBox)
						.IsChecked(this, &SPointCloudImportWidget::GetInvertZAxisCheck)
					.OnCheckStateChanged(this, &SPointCloudImportWidget::OnInvertZAxisChanged)
					]
				+ SGridPanel::Slot(0, 6)
					.Padding(20, 10, 20, 10)
					.HAlign(HAlign_Fill)
					.VAlign(VAlign_Center)
					[
						SNew(SButton)
						.Text(FText::FromString("Import Scan Data"))
						.OnClicked(this, &SPointCloudImportWidget::ImportScanDataClicked)
					]	
				+ SGridPanel::Slot(0, 7)
					.Padding(20, 10, 20, 10)
					.HAlign(HAlign_Fill)
					.VAlign(VAlign_Center)
					[
						SNew(SButton)
						.Text(FText::FromString("Import Terrain Point Cloud"))
						.OnClicked(this, &SPointCloudImportWidget::ImportTerrainDataClicked)
					]
				+ SGridPanel::Slot(1, 7)
					.Padding(20, 10, 20, 10)
					.HAlign(HAlign_Fill)
					.VAlign(VAlign_Center)
					[
						SNew(SEditableTextBox)						
						.OnTextChanged(this, &SPointCloudImportWidget::OnMaxNumPointsChanged, ETextCommit::Default)
						.Text(this, &SPointCloudImportWidget::GetMaxNumPoints)
					]
				+ SGridPanel::Slot(2, 7)
					.Padding(5, 5, 20, 5)
					.HAlign(HAlign_Left)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text(FText::FromString("Points max."))
					]
				]
			]
		];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TSharedRef<SWidget> SPointCloudImportWidget::GenerateComboItem(TSharedPtr<FString> InItem)
{
	return SNew(STextBlock)
		.Text(FText::FromString(*InItem));
}

FText SPointCloudImportWidget::GetSelectedCoordinateSystemLabel() const
{
	if (SelectedCoordinateSystemItem.IsValid())
	{
		return FText::FromString(*SelectedCoordinateSystemItem);
	}

	return FText::FromString("-");
}

FText SPointCloudImportWidget::GetSelectedXYUnitItemLabel() const
{
	if (SelectedXYUnitItem.IsValid())
	{
		return FText::FromString(*SelectedXYUnitItem);
	}

	return FText::FromString("-");
}

FText SPointCloudImportWidget::GetSelectedZUnitItemLabel() const
{
	if (SelectedZUnitItem.IsValid())
	{
		return FText::FromString(*SelectedZUnitItem);
	}

	return FText::FromString("-");
}

FText SPointCloudImportWidget::GetMaxNumPoints() const
{
	return FText::FromString(*MaxNumPointsString);
}

ECheckBoxState SPointCloudImportWidget::GetInvertXAxisCheck() const
{
	return (InvertXAxisCheck) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

ECheckBoxState SPointCloudImportWidget::GetInvertYAxisCheck() const
{
	return (InvertYAxisCheck) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

ECheckBoxState SPointCloudImportWidget::GetInvertZAxisCheck() const
{
	return (InvertZAxisCheck) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

void SPointCloudImportWidget::OnCoordSystemSelectionChanged(FComboItemType NewValue, ESelectInfo::Type)
{
	SelectedCoordinateSystemItem = NewValue;
}

void SPointCloudImportWidget::OnXYUnitSelectionChanged(FComboItemType NewValue, ESelectInfo::Type)
{
	SelectedXYUnitItem = NewValue;
}

void SPointCloudImportWidget::OnZUnitSelectionChanged(FComboItemType NewValue, ESelectInfo::Type)
{
	SelectedZUnitItem = NewValue;
}

inline void SPointCloudImportWidget::OnInvertXAxisChanged(ECheckBoxState newState)
{
	InvertXAxisCheck = (newState == ECheckBoxState::Checked);
}

inline void SPointCloudImportWidget::OnInvertYAxisChanged(ECheckBoxState newState)
{
	InvertYAxisCheck = (newState == ECheckBoxState::Checked);
}

inline void SPointCloudImportWidget::OnInvertZAxisChanged(ECheckBoxState newState)
{
	InvertZAxisCheck = (newState == ECheckBoxState::Checked);
}

void SPointCloudImportWidget::OnMaxNumPointsChanged(const FText& InText, ETextCommit::Type InCommitType)
{
	MaxNumPointsString = InText.ToString();
}

void SPointCloudImportWidget::GetDataFromUI()
{
	if ( SelectedCoordinateSystemItem->Equals(CoordSystemTextXmZY))
	{
		CoordinateData.CoordinateSystem = XmZY;
	}
	else if (SelectedCoordinateSystemItem->Equals(CoordSystemTextXYZ))
	{
		CoordinateData.CoordinateSystem = XYZ;
	}
	else if (SelectedCoordinateSystemItem->Equals(CoordSystemXTextUnity))
	{
		CoordinateData.CoordinateSystem = Unity;
	}

	if (SelectedXYUnitItem->Equals(UnitTextInch))
	{
		CoordinateData.XYUnit = inch;
	}
	else
	{
		CoordinateData.XYUnit = meter;
	}

	if (SelectedZUnitItem->Equals(UnitTextInch))
	{
		CoordinateData.ZUnit = inch;
	}
	else
	{
		CoordinateData.ZUnit = meter;
	}

	CoordinateData.InvertXAxis = InvertXAxisCheck;
	CoordinateData.InvertYAxis = InvertYAxisCheck;
	CoordinateData.InvertZAxis = InvertZAxisCheck;
}

FReply SPointCloudImportWidget::ImportScanDataClicked()
{
	GetDataFromUI();

	IDesktopPlatform* DesktopPlatform = FDesktopPlatformModule::Get();

	TArray<FString> outFilePath;
	if (DesktopPlatform->OpenFileDialog(nullptr, FString("Choose a Point Cloud txt file to import"), FString(""), FString("*.*"), FString("File|*.*"), EFileDialogFlags::None, outFilePath))
	{
		UPointCloudImporter* PointCloudImporter = NewObject<UPointCloudImporter>(GetTransientPackage(), FName("PointCloudImporter"), RF_Public);

		PointCloudImporter->ImportPointCLoud(&outFilePath[0], &CoordinateData);

		PointCloudImporter = nullptr;
		CollectGarbage(RF_NoFlags, true);
	}

	return FReply::Handled();
}

inline FReply SPointCloudImportWidget::ImportTerrainDataClicked()
{
	GetDataFromUI();

	IDesktopPlatform* DesktopPlatform = FDesktopPlatformModule::Get();

	TArray<FString> outFilePath;
	if (DesktopPlatform->OpenFileDialog(nullptr, FString("Choose a Point Cloud txt file to import"), FString(""), FString("*.*"), FString("File|*.*"), EFileDialogFlags::None, outFilePath))
	{
		UPointCloudImporter* PointCloudImporter = NewObject<UPointCloudImporter>(GetTransientPackage(), FName("PointCloudImporter"), RF_Public);

		PointCloudImporter->ImportTerrainPointCLoud(&outFilePath[0], &CoordinateData, FCString::Atoi(*MaxNumPointsString));

		PointCloudImporter = nullptr;
		CollectGarbage(RF_NoFlags, true);
	}

	return FReply::Handled();
}