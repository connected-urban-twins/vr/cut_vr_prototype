/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#define IMPORTPATH_POINTCLOUD "Import/PointCloud" 
#define IMPORTPATH_MESHES "Import/PointCloud/Meshes" 

#include "Engine.h"

#include "DelaunayTriangulation.h"

#include "PointCloudImporter.generated.h"


struct PointCloudTracker
{
	TArray<PointCloudEntry> PointCloud;
	TArray<PointCloudEntry> DecimatedPointCloud;
	ImportCoordinateData CoordinateData;

	double MinX;
	double MinY;
	double MinZ;

	double MaxX;
	double MaxY;
	double MaxZ;

	double ScaleFactorInTexture = 1.0;
	double ScaleFactorForActor = 1.0;

	FVector PointCloudActorLocation = FVector(0.0f);

	void CalculateBounds(TArray<PointCloudEntry>& pointCloud)
	{
		MinX = pointCloud[0].X;
		MinY = pointCloud[0].Y;
		MinZ = pointCloud[0].Z;

		MaxX = pointCloud[0].X;
		MaxY = pointCloud[0].Y;
		MaxZ = pointCloud[0].Z;

		const double maxUINT32 = static_cast<double>(UINT32_MAX);

		for (PointCloudEntry point : pointCloud)
		{
			if (point.X < MinX) { MinX = point.X; }
			if (point.Y < MinY) { MinY = point.Y; }
			if (point.Z < MinZ) { MinZ = point.Z; }

			if (point.X > MaxX) { MaxX = point.X; }
			if (point.Y > MaxY) { MaxY = point.Y; }
			if (point.Z > MaxZ) { MaxZ = point.Z; }
		}	
		
		double maxBoundingSideLength = FMath::Max3(MaxX - MinX, MaxY - MinY, MaxZ - MinZ);

		ScaleFactorInTexture = maxUINT32 / maxBoundingSideLength;
		
		// Values for the Unreal
		PointCloudActorLocation = FVector(MinX, MinY, MinZ) * 100.0 * GLOBALSCALEFACTOR;
		ScaleFactorForActor = maxBoundingSideLength * 1.5 * GLOBALSCALEFACTOR;
	}
};

UCLASS()
class UPointCloudImporter : public UObject
{
	 GENERATED_BODY()

public:
	UPointCloudImporter(): MeshPackage(nullptr), TargetTextureSizeX(4096), TargetTextureSizeY(4096)
	{
	}

	~UPointCloudImporter()
	{

	}

	void ImportPointCLoud(FString* filePath, ImportCoordinateData* coordinateData);
	void ImportTerrainPointCLoud(FString* filePath, ImportCoordinateData* coordinateData, int32 maxNumPoints);
	void CreateStaticMeshAsset(FString* inMeshName);
	void ConstructMeshFromPointCloud(FString* filePath);
	UTexture2D* GetTexture(UPackage** package, FString textureAssetName, bool isColorMap = false) const;
	void ParseTextFile(FString* filePath);
	FVector CalculateImportScale() const;
	FVector GetColorFromHeight(double normalizedHeight) const;
	void RecolorDepthPoints(int32 startIndex, int32 endIndex);
	void RecolorAllDepthPoints();
	void RecolorDepthPoints();
	void DecimatePointCloud(uint32 numPoints);

	UPROPERTY()
		UPackage* MeshPackage;

private:
	PointCloudTracker PointCloudTracker;
	FDelaunayTriangulation DelaunayTriangulator;
	int32 TargetTextureSizeX;
	int32 TargetTextureSizeY;
	FString CloudName;
};