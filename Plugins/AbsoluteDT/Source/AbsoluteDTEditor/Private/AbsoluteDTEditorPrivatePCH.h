/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#pragma once

#include "Engine.h"
#include <string>

// The world is at scale 0.1
#define GLOBALSCALEFACTOR 0.1

struct PointCloudEntry
{
	double X;
	double Y;
	double Z;
	uint8 R;
	uint8 G;
	uint8 B;
	uint8 A;

	PointCloudEntry()
	{
		X = 0;
		Y = 0;
		Z = 0;
		R = 0;
		G = 0;
		B = 0;
		A = 0;
	}

	PointCloudEntry(double x, double y, double z, uint8 r, uint8 g, uint8 b, uint8 a)
	{
		X = x;
		Y = y;
		Z = z;
		R = r;
		G = g;
		B = b;
		A = a;
	}

	PointCloudEntry(const PointCloudEntry& point)
	{
		X = point.X;
		Y = point.Y;
		Z = point.Z;
		R = point.R;
		G = point.G;
		B = point.B;
		A = point.A;
	}

	PointCloudEntry(const FVector& point)
	{
		X = point.X;
		Y = point.Y;
		Z = point.Z;
		R = 0;
		G = 0;
		B = 0;
		A = 0;
	}

	inline double Distance2DSquared(const PointCloudEntry &v) const
	{
		double dx = X - v.X;
		double dy = Y - v.Y;
		return dx * dx + dy * dy;
	}
	
	inline double Distance2D(const PointCloudEntry &v) const
	{
		return sqrtf(Distance2DSquared(v));
	}
};

inline bool operator == (PointCloudEntry v1, PointCloudEntry v2)
{
	return v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z;
}

inline PointCloudEntry operator * (float scale, PointCloudEntry v2)
{
	return PointCloudEntry(scale * v2.X, scale * v2.Y, scale * v2.Z, v2.R, v2.G, v2.B, v2.A);
}


enum EPointCloudCoordinateSystem
{
	XYZ,
	XmZY,
	Unity
};

enum EPointCloudUnit
{
	inch,
	meter
};

struct ImportCoordinateData
{
	EPointCloudCoordinateSystem CoordinateSystem;
	EPointCloudUnit XYUnit;
	EPointCloudUnit ZUnit;
	bool InvertXAxis = false;
	bool InvertYAxis = false;
	bool InvertZAxis = false;
};
