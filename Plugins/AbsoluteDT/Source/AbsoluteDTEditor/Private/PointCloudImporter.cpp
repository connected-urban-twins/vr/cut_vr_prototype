#pragma once

#include "PointCloudImporter.h"
#include "ImageUtils.h"
#include <array>
#include <fstream>
#include <stdio.h>

#include "PhysicsEngine/BodySetup.h"
#include "PackageTools.h"
#include "Editor.h"
#include "AssetRegistryModule.h"

void UPointCloudImporter::ImportPointCLoud(FString* filePath, ImportCoordinateData*  coordinateData)
{
	PointCloudTracker.CoordinateData = *coordinateData;
	ParseTextFile(filePath);

	DecimatePointCloud(TargetTextureSizeX * TargetTextureSizeY);

	RecolorDepthPoints();

	FString CoordHighTextureAssetName = FPaths::GetBaseFilename(*filePath) + "_High";
	FString CoordLowTextureAssetName = FPaths::GetBaseFilename(*filePath) + "_Low";
	FString ColorTextureAssetName = FPaths::GetBaseFilename(*filePath) + "_Col";

	UPackage* CoordHighPackage;
	UPackage* CoordLowPackage;
	UPackage* ColorPackage;


	UTexture2D * TextureCoordHigh = GetTexture(&CoordHighPackage, CoordHighTextureAssetName);
	UTexture2D * TextureCoordLow = GetTexture(&CoordLowPackage, CoordLowTextureAssetName);
	UTexture2D * TextureColor = GetTexture(&ColorPackage, ColorTextureAssetName, true);

	uint8* CoordHighMipData = TextureCoordHigh->Source.LockMip(0);
	uint8* CoordLowMipData = TextureCoordLow->Source.LockMip(0);
	uint8* ColorMipData = TextureColor->Source.LockMip(0);
	
	int32 iterator = 0;

	for (int32 y = 0; y < TargetTextureSizeY; y++)
	{
		uint16* DestCoordHighPtr = reinterpret_cast<uint16*>(&CoordHighMipData[(TargetTextureSizeY - 1 - y) * TargetTextureSizeX * sizeof(uint16) * 4]);
		uint16* DestCoordLowPtr = reinterpret_cast<uint16*>(&CoordLowMipData[(TargetTextureSizeY - 1 - y) * TargetTextureSizeX * sizeof(uint16) * 4]);
		uint8* DestColorPtr = static_cast<uint8*>(&ColorMipData[(TargetTextureSizeY - 1 - y) * TargetTextureSizeX * sizeof(uint8) * 4]);

		for (int32 x = 0; x < TargetTextureSizeX; x++)
		{
			uint32 xCoord;
			uint32 yCoord;
			uint32 zCoord;

			if (iterator >= PointCloudTracker.DecimatedPointCloud.Num())
			{
				xCoord = yCoord = zCoord = 0;
				*DestColorPtr++ = 0;
				*DestColorPtr++ = 0;
				*DestColorPtr++ = 0;
				*DestColorPtr++ = 0;
			}
			else
			{
				xCoord = PointCloudTracker.ScaleFactorInTexture * (PointCloudTracker.DecimatedPointCloud[iterator].X - PointCloudTracker.MinX);
				yCoord = PointCloudTracker.ScaleFactorInTexture * (PointCloudTracker.DecimatedPointCloud[iterator].Y - PointCloudTracker.MinY);
				zCoord = PointCloudTracker.ScaleFactorInTexture * (PointCloudTracker.DecimatedPointCloud[iterator].Z - PointCloudTracker.MinZ);
				*DestColorPtr++ = PointCloudTracker.DecimatedPointCloud[iterator].B;
				*DestColorPtr++ = PointCloudTracker.DecimatedPointCloud[iterator].G;
				*DestColorPtr++ = PointCloudTracker.DecimatedPointCloud[iterator].R;
				*DestColorPtr++ = PointCloudTracker.DecimatedPointCloud[iterator].A;
			}

			*DestCoordHighPtr++ = xCoord >> 16;
			*DestCoordHighPtr++ = yCoord >> 16;
			*DestCoordHighPtr++ = zCoord >> 16;
			*DestCoordHighPtr++ = 0xFFFF;

			*DestCoordLowPtr++ = xCoord & 0xFFFF;
			*DestCoordLowPtr++ = yCoord & 0xFFFF;
			*DestCoordLowPtr++ = zCoord & 0xFFFF;
			*DestCoordLowPtr++ = 0xFFFF;

			iterator++;
		}
	}

	TextureCoordHigh->Source.UnlockMip(0);
	TextureCoordLow->Source.UnlockMip(0);
	TextureColor->Source.UnlockMip(0);


	//int32 Width2 = Width / 2;
	//int32 Height2 = Height / 2;

	//uint8* MipData2 = Texture->Source.LockMip(1);
	//for (int32 y = 0; y < Height2; y++)
	//{
	//	uint16* DestPtr = (uint16*)&MipData2[(Height2 - 1 - y) * Width2 * sizeof(uint16) * 4];
	//	for (int32 x = 0; x<Width2; x++)
	//	{
	//		*DestPtr++ = 65535;
	//		*DestPtr++ = 0;
	//		*DestPtr++ = 0;
	//		*DestPtr++ = 0xFF;
	//	}
	//}
	//Texture->Source.UnlockMip(1);

	TextureCoordHigh->PostEditChange();
	TextureCoordLow->PostEditChange();
	TextureColor->PostEditChange();

	FAssetRegistryModule::AssetCreated(TextureCoordHigh);
	FAssetRegistryModule::AssetCreated(TextureCoordLow);
	FAssetRegistryModule::AssetCreated(TextureColor);

	CoordHighPackage->MarkPackageDirty();
	CoordLowPackage->MarkPackageDirty();
	ColorPackage->MarkPackageDirty();


	FString CoordHighPackageFilePath = FPaths::ProjectContentDir() + FString(FString(IMPORTPATH_POINTCLOUD) + "/") + CoordHighTextureAssetName + FPackageName::GetAssetPackageExtension();
	UPackage::Save(CoordHighPackage, TextureCoordHigh, EObjectFlags::RF_Public, *CoordHighPackageFilePath);

	FString CoordLowPackageFilePath = FPaths::ProjectContentDir() + FString(FString(IMPORTPATH_POINTCLOUD) + "/") + CoordLowTextureAssetName + FPackageName::GetAssetPackageExtension();
	UPackage::Save(CoordLowPackage, TextureCoordLow, EObjectFlags::RF_Public, *CoordLowPackageFilePath);

	FString ColorPackageFilePath = FPaths::ProjectContentDir() + FString(FString(IMPORTPATH_POINTCLOUD) + "/") + ColorTextureAssetName + FPackageName::GetAssetPackageExtension();
	UPackage::Save(ColorPackage, TextureColor, EObjectFlags::RF_Public, *ColorPackageFilePath);

	UE_LOG(LogTemp, Warning, TEXT("Imported Point Cloud: (Scale %f   Location (%f | %f | %f)\n"), PointCloudTracker.ScaleFactorForActor, PointCloudTracker.PointCloudActorLocation.X, PointCloudTracker.PointCloudActorLocation.Y, PointCloudTracker.PointCloudActorLocation.Z);

}

void UPointCloudImporter::ImportTerrainPointCLoud(FString* filePath, ImportCoordinateData* coordinateData, int32 maxNumPoints)
{
	PointCloudTracker.CoordinateData = *coordinateData;
	ParseTextFile(filePath);

	DecimatePointCloud(maxNumPoints);
	RecolorAllDepthPoints();

	ConstructMeshFromPointCloud(filePath);
}

void UPointCloudImporter::CreateStaticMeshAsset(FString* inMeshName)
{
	bool isNewAsset = false;
	const uint32 LightMapIndex = 0;

	FRawMesh AccumulatorMesh;
	DelaunayTriangulator.BuildRawMesh(&AccumulatorMesh);

	FString RawPackageName = FString(TEXT("/Game/" + FString(IMPORTPATH_MESHES) + "/" + *inMeshName));
	const FString PackageName = PackageTools::SanitizePackageName(RawPackageName);

	// Create the package
	MeshPackage = CreatePackage(*PackageName);
	check(MeshPackage);

	if (AccumulatorMesh.IsValidOrFixable())
	{
		FString MeshName = FPackageName::GetLongPackageAssetName(MeshPackage->GetFullName() + "/" + *inMeshName);

		UStaticMesh* StaticMesh = LoadObject<UStaticMesh>(MeshPackage, *MeshName);

		FStaticMeshSourceModel* SrcModel;

		if (!StaticMesh || !StaticMesh->IsValidLowLevel())
		{
			isNewAsset = true;
		}

		if (isNewAsset)
		{
			StaticMesh = NewObject<UStaticMesh>(MeshPackage, *MeshName, RF_Public | RF_Standalone);
			StaticMesh->InitResources();
			SrcModel = new (StaticMesh->GetSourceModels()) FStaticMeshSourceModel();
			StaticMesh->SetLightingGuid(FGuid::NewGuid());
		}
		else
		{
			StaticMesh->ReleaseResources();
			StaticMesh->InitResources();
			SrcModel = &StaticMesh->GetSourceModels()[0];
		}

		SrcModel->BuildSettings.bRecomputeNormals = true;
		SrcModel->BuildSettings.bRecomputeTangents = true;
		SrcModel->BuildSettings.bRemoveDegenerates = false;
		SrcModel->BuildSettings.bUseHighPrecisionTangentBasis = false;
		SrcModel->BuildSettings.bUseFullPrecisionUVs = true;
		SrcModel->BuildSettings.bGenerateLightmapUVs = false;
		SrcModel->BuildSettings.SrcLightmapIndex = 0;
		SrcModel->BuildSettings.DstLightmapIndex = LightMapIndex;
		SrcModel->RawMeshBulkData->SaveRawMesh(AccumulatorMesh);

		StaticMesh->CreateBodySetup();
		StaticMesh->GetBodySetup()->CollisionTraceFlag = ECollisionTraceFlag::CTF_UseComplexAsSimple;
		StaticMesh->GetBodySetup()->CollisionReponse = EBodyCollisionResponse::BodyCollision_Enabled;

		StaticMesh->GetStaticMaterials().Empty();
		StaticMesh->GetStaticMaterials().Add(FStaticMaterial());

		//Set the Imported version before calling the build
		StaticMesh->ImportVersion = LastVersion;

		// Set light map coordinate index to match DstLightmapIndex
		StaticMesh->SetLightMapCoordinateIndex(LightMapIndex);

		TArray<int32> UniqueMaterialIndices;
		for (int32 MaterialIndex : AccumulatorMesh.FaceMaterialIndices)
		{
			UniqueMaterialIndices.AddUnique(MaterialIndex);
		}

		int32 SectionIndex = 0;
		for (int32 UniqueMaterialIndex : UniqueMaterialIndices)
		{
			StaticMesh->GetSectionInfoMap().Set(0, SectionIndex, FMeshSectionInfo(UniqueMaterialIndex));
		}

		// Build mesh from source
		StaticMesh->Build(true);
		StaticMesh->PostEditChange();

		// Notify asset registry of new asset
		if (isNewAsset)
		{
			FAssetRegistryModule::AssetCreated(StaticMesh);
		}

		StaticMesh->MarkPackageDirty();

		//FActorSpawnParameters SpawnPar;
		//FString t = FString("SP3DMesh_" + *inMeshName);
		//SpawnPar.Name = FName(*t);

		////UE_LOG(LogTemp, Warning, TEXT("SPawned Actor Name:%s\n"), *t);

		////FTransform transform = FTransform(Matrix);
		//FTransform transform = FTransform(FMatrix::Identity);

		//transform.ScaleTranslation(FVector(100.0f, 100.0f, 100.0f));
		//AStaticMeshActor *actor = Cast<AStaticMeshActor, AActor>(GEditor->GetEditorWorldContext().World()->SpawnActor(AStaticMeshActor::StaticClass(), &transform, SpawnPar));
		//actor->GetStaticMeshComponent()->SetStaticMesh(meshToAdd);

		//switch (CurrentCollisionType)
		//{
		//case NoCollision:
		//	actor->GetStaticMeshComponent()->SetCollisionProfileName("NoCollision");
		//	break;

		//case Collide:
		//	actor->GetStaticMeshComponent()->SetCollisionProfileName("Custom");
		//	actor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

		//	// Collision Object Type 'WalkableFloor'		
		//	actor->GetStaticMeshComponent()->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);
		//	break;

		//default:
		//	actor->GetStaticMeshComponent()->SetCollisionProfileName("BlockAll");
		//	//actor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		//	//actor->GetStaticMeshComponent()->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
		//	break;
		//}


		//actor->SetActorLabel(t, false);

		//StaticMesh->ClearFlags(RF_Standalone);

		//const FString SanitizedBasePackageName = PackageTools::SanitizePackageName(StaticMesh->GetOutermost()->GetName());
		//const FString PackagePath = FPackageName::GetLongPackagePath(PackageName);

		UE_LOG(LogTemp, Warning, TEXT("Imported Terrain Point Cloud Pqackage :%s\n"), *PackageName);


		FString MeshFilePath = FPaths::ProjectContentDir() + FString(FString(IMPORTPATH_MESHES) + "/") + *inMeshName + FPackageName::GetAssetPackageExtension();
		UPackage::Save(MeshPackage, StaticMesh, EObjectFlags::RF_Public, *MeshFilePath);
	}
}

void UPointCloudImporter::ConstructMeshFromPointCloud(FString* filePath)
{
	DelaunayTriangulator.triangulate(PointCloudTracker.DecimatedPointCloud);
	FString AssetName = FPaths::GetBaseFilename(*filePath);
	CreateStaticMeshAsset(&AssetName);
}

UTexture2D* UPointCloudImporter::GetTexture(UPackage** package, FString textureAssetName, bool isColorMap) const
{
	FString packageName = FString(TEXT("/Game/" + FString(IMPORTPATH_POINTCLOUD) + "/")) + textureAssetName;

	*package = CreatePackage(*packageName);
	check(*package);

	UTexture2D* Texture = LoadObject<UTexture2D>(*package, *packageName);

	if (!Texture)
	{
		Texture = NewObject<UTexture2D>(*package, FName(*textureAssetName), RF_Public | RF_Standalone);
	}

	Texture->CompressionNoAlpha = true;
	Texture->DeferCompression = true;
	Texture->MipGenSettings = TMGS_NoMipmaps;

	if (isColorMap)
	{
		Texture->Source.Init(TargetTextureSizeX, TargetTextureSizeY, /*NumSlices=*/ 1, /*NumMips=*/ 1, TSF_BGRA8);
		Texture->SRGB = true;
		Texture->CompressionSettings = TC_HDR;
	}
	else
	{
		Texture->Source.Init(TargetTextureSizeX, TargetTextureSizeY, /*NumSlices=*/ 1, /*NumMips=*/ 1, TSF_RGBA16);
		Texture->SRGB = false;
		Texture->CompressionSettings = TC_HDR;
	}

	return Texture;
}

void UPointCloudImporter::ParseTextFile(FString* filePath)
{

	FString AnalyseProgressTitle = "Analysing Point Cloud File";
	FScopedSlowTask * AnalyseProgressTracker = new FScopedSlowTask(1.0f, FText::FromString(AnalyseProgressTitle), true, *GWarn);
	AnalyseProgressTracker->MakeDialog();

	std::ifstream myfile(**filePath);

	// new lines will be skipped unless we stop it from happening:    
	myfile.unsetf(std::ios_base::skipws);

	// count the newlines with an algorithm specialized for counting:
	uint32 line_count = std::count(
		std::istreambuf_iterator<char>(myfile),
		std::istreambuf_iterator<char>(),
		'\n');

	AnalyseProgressTracker->Destroy();
	
	float BLockSize = 100000;
	float WorkAmount = BLockSize * ((float)line_count / BLockSize + 1.0f);
	FString ParseProgressTitle = "Parsing Point Cloud File";
	FScopedSlowTask * ParseProgressTracker = new FScopedSlowTask(WorkAmount, FText::FromString(ParseProgressTitle), true, *GWarn);
	ParseProgressTracker->MakeDialog();

	FVector scaleFactor = CalculateImportScale();

	PointCloudTracker.PointCloud.Empty();
	PointCloudTracker.PointCloud.Reserve(line_count);

	std::string fPathChar = TCHAR_TO_UTF8(**filePath);

	FILE *stream;
	fopen_s(&stream, fPathChar.c_str(), "r");

	if (stream == nullptr) {
		return;
	}

	const int SIZE = 1024 * 1024 * 3;
	char line[SIZE];

	int32 LineIndex = 0;

	while (fgets(line, SIZE, stream) == line)
	{
		if (LineIndex % (uint32)BLockSize == 0)
		{
			ParseProgressTracker->EnterProgressFrame(BLockSize);
		}

		FString lineString = FString(line);

		if (!lineString.IsEmpty())
		{
			//TArray<FString> valueStrings;
			//lineString.ParseIntoArray(valueStrings, TEXT(" "), true);

			double x, y, z;

			uint8 r = 255;
			uint8 g = 255;
			uint8 b = 255;

			double xPos = 0.0, yPos = 0.0, zPos = 0.0;
			int red = 255, green = 255, blue = 255;

			int result = sscanf_s(line, "%lf %lf %lf %d %d %d", &xPos, &yPos, &zPos, &red, &green, &blue);

			if (result == EOF || result < 6)
			{
				int i = 0;
			}

			switch (PointCloudTracker.CoordinateData.CoordinateSystem)
			{
				case XYZ:
					x = xPos;
					y = yPos;
					z = zPos;
					break;
				case Unity:
					x = xPos;
					y = zPos;
					z = yPos;
					break;

				case XmZY:
				default:
					x = xPos;
					y = -zPos;
					z = yPos;
					break;
			}

			r = red;
			g = green;
			b = blue;


			PointCloudTracker.PointCloud.Add(PointCloudEntry(x * scaleFactor.X, y * scaleFactor.Y, z * scaleFactor.Z, r, g, b, 0));
		}

		LineIndex++;

	}

	ParseProgressTracker->Destroy();

	fclose(stream);
}

FVector UPointCloudImporter::CalculateImportScale() const
{
	FVector result = FVector(1.0);

	switch (PointCloudTracker.CoordinateData.XYUnit)
	{
		case inch:
			result.X = result.Y = 0.0254;
			break;
		case meter:
			result.X = result.Y = 1;
		default:
			break;
	}

	switch (PointCloudTracker.CoordinateData.ZUnit)
	{
		case inch:
			result.Z = 0.0254;
			break;
		case meter:
			result.Z = 1;
		default:
			break;
	}

	if (PointCloudTracker.CoordinateData.InvertXAxis)
	{
		result.X = -result.X;
	}

	if (PointCloudTracker.CoordinateData.InvertYAxis)
	{
		result.Y = -result.Y;
	}

	if (PointCloudTracker.CoordinateData.InvertZAxis)
	{
		result.Z = -result.Z;
	}

	result *= GLOBALSCALEFACTOR;

	return result;
}

FVector UPointCloudImporter::GetColorFromHeight(double normalizedHeight) const
{
	const FVector Color1 = FVector(0.0f, 0.0f, 0.5f);
	const FVector Color2 = FVector(0.0f, 0.5f, 1.0f);
	const FVector Color3 = FVector(0.5f, 1.0f, 0.5f);
	const FVector Color4 = FVector(1.0f, 0.5f, 0.0f);
	const FVector Color5 = FVector(0.5f, 0.0f, 0.0f);

	FVector Result = FVector(0.0f, 0.0f, 0.0f);

	if (normalizedHeight < 0.25)
	{
		Result = FMath::Lerp(Color1, Color2, normalizedHeight * 4);
	}
	else if (normalizedHeight < 0.5)
	{
		Result = FMath::Lerp(Color2, Color3, (normalizedHeight- 0.25) * 4);
	}
	else if (normalizedHeight < 0.75)
	{
		Result = FMath::Lerp(Color3, Color4, (normalizedHeight - 0.5) * 4);
	}
	else if (normalizedHeight >= 0.75)
	{
		Result = FMath::Lerp(Color4, Color5, (normalizedHeight - 0.75) * 4);
	}

	return Result;
}

void UPointCloudImporter::RecolorDepthPoints(int32 startIndex, int32 endIndex)
{
	double MinZ = DBL_MAX;
	double MaxZ = -DBL_MAX;

	for (int32 i = startIndex; i <=  endIndex; i++)
	{
		double zValue = PointCloudTracker.DecimatedPointCloud[i].Z;
		if (zValue < MinZ) { MinZ = zValue; }
		if (zValue > MaxZ) { MaxZ = zValue; }
	}

	for (int i = startIndex; i <= endIndex; i++)
	{
		float normalizedHeightValue = (PointCloudTracker.DecimatedPointCloud[i].Z - MinZ) / FMath::Abs(MaxZ - MinZ);

		FVector color = GetColorFromHeight(normalizedHeightValue);

		PointCloudTracker.DecimatedPointCloud[i].R = color.X * 255;
		PointCloudTracker.DecimatedPointCloud[i].G = color.Y * 255;
		PointCloudTracker.DecimatedPointCloud[i].B = color.Z * 255;
		PointCloudTracker.DecimatedPointCloud[i].A = 255;
	}
}

void UPointCloudImporter::RecolorAllDepthPoints()
{
	RecolorDepthPoints(0, PointCloudTracker.DecimatedPointCloud.Num() - 1);
}

void UPointCloudImporter::RecolorDepthPoints()
{
	bool currentItemHasDepthInformation = false;

	uint32 DepthIndexStart = 0;
	uint32 DepthIndexEnd = 0;
	uint32 index = 0;
	uint32 NumDepthPoints = 0;

	for (auto point : PointCloudTracker.DecimatedPointCloud)
	{
		if (point.R == 204 && point.G == 51 && point.B == 25)
		{
			if (currentItemHasDepthInformation == false)
			{
				currentItemHasDepthInformation = true;
				NumDepthPoints = 0;
			}
			else
			{
				NumDepthPoints++;
			}
		}
		else
		{
			if (currentItemHasDepthInformation == true)
			{
				if (NumDepthPoints > 1000)
				{
					DepthIndexStart = index - NumDepthPoints - 1;
					DepthIndexEnd = index - 1;
					break;
				}
			}
			else
			{
				
			}

			currentItemHasDepthInformation = false;
		}

		index++;
	}

	if (currentItemHasDepthInformation == true && NumDepthPoints > 1000)
	{
		DepthIndexStart = index - NumDepthPoints - 1;
		DepthIndexEnd = index - 1;
	}

	RecolorDepthPoints(DepthIndexStart, DepthIndexEnd);
}

void UPointCloudImporter::DecimatePointCloud(uint32 targetSize)
{
	double iterator = 0.0;
	double IteratorAdd = 1.0;

	uint32 SourceSize = PointCloudTracker.PointCloud.Num();

	targetSize = FMath::Min(SourceSize, targetSize);

	if (SourceSize > targetSize)
	{
		IteratorAdd = (SourceSize - 1.0) / (targetSize - 1.0);
	}

	PointCloudTracker.DecimatedPointCloud.Empty();

	for (uint32 index = 0; index < targetSize; index++)
	{
		if (iterator >= SourceSize)
		{
			break;
		}

		PointCloudTracker.DecimatedPointCloud.Add(PointCloudTracker.PointCloud[iterator]);

		iterator += IteratorAdd;
	}

	PointCloudTracker.PointCloud.Empty();
	PointCloudTracker.CalculateBounds(PointCloudTracker.DecimatedPointCloud);
}
