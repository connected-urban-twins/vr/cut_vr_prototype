/**

The MIT License (MIT)

Copyright (c) 2021 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "AbsoluteDTEditor.h"
#include "AbsoluteDTStyle.h"
#include "AbsoluteDTCommands.h"
#include "Misc/MessageDialog.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"

#include "LevelEditor.h"
#include "Developer/DesktopPlatform/Public/IDesktopPlatform.h"
#include "Developer/DesktopPlatform/Public/DesktopPlatformModule.h"
#include "Engine.h"
#include "PointCloudImporter.h"
#include "PointCloudImportWidget.h"
#include "SDockTab.h"

static const FName AbsoluteDTTabName("AbsoluteDT");

static const FName WIndowPluginTabName("WIndowPlugin");

#define LOCTEXT_NAMESPACE "FAbsoluteDTEditorModule"

void FAbsoluteDTEditorModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	FAbsoluteDTStyle::Initialize();
	FAbsoluteDTStyle::ReloadTextures();

	FAbsoluteDTCommands::Register();
	
	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FAbsoluteDTCommands::Get().ImportPointCloud,
		FExecuteAction::CreateRaw(this, &FAbsoluteDTEditorModule::ImportPointCLoud),
		FCanExecuteAction());

	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");
	
	{
		TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender());
		MenuExtender->AddMenuExtension("WindowLayout", EExtensionHook::After, PluginCommands, FMenuExtensionDelegate::CreateRaw(this, &FAbsoluteDTEditorModule::AddMenuExtension));

		LevelEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender);
	}
	
	{
		TSharedPtr<FExtender> ToolbarExtender = MakeShareable(new FExtender);
		ToolbarExtender->AddToolBarExtension("Settings", EExtensionHook::After, PluginCommands, FToolBarExtensionDelegate::CreateRaw(this, &FAbsoluteDTEditorModule::AddToolbarExtension));
		
		LevelEditorModule.GetToolBarExtensibilityManager()->AddExtender(ToolbarExtender);
	}

	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(WIndowPluginTabName, FOnSpawnTab::CreateRaw(this, &FAbsoluteDTEditorModule::OnSpawnPluginTab))
		.SetDisplayName(LOCTEXT("PointCloudImportTitle", "Point Cloud Import"))
		.SetMenuType(ETabSpawnerMenuType::Hidden);
}

void FAbsoluteDTEditorModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	FAbsoluteDTStyle::Shutdown();

	FAbsoluteDTCommands::Unregister();
}

TSharedRef<SDockTab> FAbsoluteDTEditorModule::OnSpawnPluginTab(const FSpawnTabArgs& SpawnTabArgs)
{
	return SNew(SDockTab)
		.TabRole(ETabRole::NomadTab)
		[
			SNew(SPointCloudImportWidget)
		];
}


void FAbsoluteDTEditorModule::ImportPointCLoud()
{
	FGlobalTabmanager::Get()->TryInvokeTab(WIndowPluginTabName);
}

void FAbsoluteDTEditorModule::AddMenuExtension(FMenuBuilder& Builder)
{
	Builder.AddMenuEntry(FAbsoluteDTCommands::Get().ImportPointCloud);
}

void FAbsoluteDTEditorModule::AddToolbarExtension(FToolBarBuilder& Builder)
{
	//Builder.AddToolBarButton(FAbsoluteDTCommands::Get().ImportPointCloud);

	Builder.AddComboButton(
		FUIAction(),
		FOnGetContent::CreateStatic(&FAbsoluteDTEditorModule::GenerateMenu, PluginCommands.ToSharedRef()),
		LOCTEXT("LindePluginModule", "AbsoluteDT"),
		LOCTEXT("LindePluginModule", "AbsoluteDT Importfunktionen"),
		FSlateIcon(FAbsoluteDTStyle::GetStyleSetName(), "AbsoluteDTToolbarButton")
	);
}

TSharedRef< SWidget > FAbsoluteDTEditorModule::GenerateMenu(TSharedRef<FUICommandList> InCommandList)
{
	FLevelEditorModule& LevelEditorModule = FModuleManager::GetModuleChecked<FLevelEditorModule>(TEXT("LevelEditor"));
	TArray<FLevelEditorModule::FLevelEditorMenuExtender> MenuExtenderDelegates = LevelEditorModule.GetAllLevelEditorToolbarBuildMenuExtenders();

	TArray<TSharedPtr<FExtender>> Extenders;
	for (int32 i = 0; i < MenuExtenderDelegates.Num(); ++i)
	{
		//if (i >= 2 && i < 8)
		//{
		//	LevelEditorModule.GetToolBarExtensibilityManager()->GetAllExtenders().Get()->MenuExtenderDelegates.RemoveAt(i);

		//}
		if (MenuExtenderDelegates[i].IsBound())
		{
			Extenders.Add(MenuExtenderDelegates[i].Execute(InCommandList));
		}
	}
	TSharedPtr<FExtender> MenuExtender = FExtender::Combine(Extenders);
	//	TSharedPtr<FExtender> MenuExtender;


	const bool bShouldCloseWindowAfterMenuSelection = true;
	FMenuBuilder MenuBuilder(bShouldCloseWindowAfterMenuSelection, InCommandList, MenuExtender);

	MenuBuilder.BeginSection("HPA Funktionen", LOCTEXT("HPAFunctions", "Aktionen"));

	MenuBuilder.AddMenuEntry(FAbsoluteDTCommands::Get().ImportPointCloud);
	MenuBuilder.EndSection();

	return MenuBuilder.MakeWidget();
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FAbsoluteDTEditorModule, AbsoluteDTEditor)