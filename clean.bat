echo Cleaning ...

RMDIR Binaries /S /Q
RMDIR Build /S /Q
RMDIR DerivedDataCache /S /Q
RMDIR Intermediate /S /Q
RMDIR Saved\Cooked /S /Q
RMDIR Saved\Logs /S /Q
RMDIR Saved\StagedBuilds /S /Q

RMDIR Plugins\AbsoluteDT\Binaries /S /Q
RMDIR Plugins\AbsoluteDT\Intermediate /S /Q

RMDIR Plugins\OpenXRExpansionPlugin\Binaries /S /Q
RMDIR Plugins\OpenXRExpansionPlugin\Intermediate /S /Q

RMDIR Plugins\VRExpansionPlugin\Binaries /S /Q
RMDIR Plugins\VRExpansionPlugin\Intermediate /S /Q

RMDIR Plugins\ZipUtility-ue4\Binaries /S /Q
RMDIR Plugins\ZipUtility-ue4\Intermediate /S /Q
